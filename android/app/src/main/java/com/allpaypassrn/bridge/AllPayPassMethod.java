package com.allpaypassrn.bridge;

import android.os.Build;
import android.util.Log;

import com.allpaypassrn.R;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.sun.org.apache.xml.internal.security.utils.Base64;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import co.intella.crypto.AesCryptoUtil;
import co.intella.crypto.KeyReader;
import co.intella.net.Constant;
import co.intella.net.HttpRequestUtil;
import static co.intella.net.HttpRequestUtil.getEncryptRequestJson;

/**
 * Created by lisa on 2017/6/2.
 */

public class AllPayPassMethod extends ReactContextBaseJavaModule{


    public AllPayPassMethod(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "AllPayPassMethod";
    }

    @ReactMethod
    public void bridgeTest(String str){
        System.out.println("This is bridge test :"+str);
    }

    @ReactMethod
    public void getPhoneOnlyNumber(Callback callback){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            String serial = Build.SERIAL;
            if(!serial.equals(Build.UNKNOWN)) {
                callback.invoke(serial);
                return;
            }
            callback.invoke("");
        }else{
            callback.invoke("");
        }
    }

    @ReactMethod
    public void encryptMap(String jsonString, String url, Callback successCallback, Callback errorCallback){
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            Map<String, String> requestMap = jsonToMap(jsonObject);
            InputStream _in;
            if(url.startsWith("https://s.intella.co/allpaypass"))
                _in = getReactApplicationContext().getResources().openRawResource(R.raw.stage);
            else
                _in = getReactApplicationContext().getResources().openRawResource(R.raw.pub);

            PublicKey publicKey = KeyReader.loadPublicKeyFromDER(_in);
            SecretKey secretKey = AesCryptoUtil.generateSecreteKey();
            String encodedKey = Base64.encode(secretKey.getEncoded());
            String requestJson = getEncryptRequestJson(requestMap, publicKey, secretKey);
            successCallback.invoke(requestJson, encodedKey);
        } catch (JSONException e) {
            errorCallback.invoke(e.getMessage());
        } catch (Exception e) {
            errorCallback.invoke(e.getMessage());
        }
    }
    private Map<String, String> jsonToMap(JSONObject json) throws JSONException {
        Iterator<String> keysItr = json.keys();
        Map<String, String> map = new HashMap<>();
        while(keysItr.hasNext()) {
            String key = keysItr.next();
            String value = (String) json.get(key);
            map.put(key, value);
        }
        return map;
    }

    @ReactMethod
    public void decryptResponse(String secretKeyStr, String response, Callback successCallback, Callback errorCallback){
        if(secretKeyStr == null)
            errorCallback.invoke("SecretKey is null.");

        String result;
        try {
            byte[] decodedKey = Base64.decode(secretKeyStr);
            SecretKey secretKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES/CBC/PKCS5PADDING");
            result = AesCryptoUtil.decryptResponse(secretKey, Constant.IV, response);
            successCallback.invoke(result);
        } catch (Exception e) {
            errorCallback.invoke(e.getMessage());
        }
    }

}
