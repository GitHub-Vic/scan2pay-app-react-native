//
//  AllPayPassMethod.m
//  AllPayPassRN
//
//  Created by Lisa on 2017/6/7.
//  Copyright © 2017年 Facebook. All rights reserved.
//
//#import <Foundation/Foundation.h>

#import "AllPayPassMethod.h"
#import <React/RCTLog.h>
#import "IntellaSDK/IntellaSDK.h"
#import "APNsTokenEmitter.h"

//@implementation 與 @end 行表示於之間的內容是AllPayPassMethod的類別實作。
@implementation AllPayPassMethod
// To export a module named AllPayPassMethod。叫用RN的巨集已讓此類別可被RN存取
RCT_EXPORT_MODULE();

//同樣的，bridgeTest :str的方法定義也已RCT_EXPORT_METHOD巨集前綴，他匯出此方法已顯露給我們的js程式。
//每個參數名稱包含在方法名稱中。
RCT_EXPORT_METHOD(bridgeTest:(NSString*) str){
   RCTLogInfo(@"bridgeTest, %@", str);
}

RCT_EXPORT_METHOD(getPhoneOnlyNumber: (RCTResponseSenderBlock)callback){
  NSString *UDID=[allpaylib getUDID];
//  NSLog(@"UDID=%@",UDID);
  callback(@[UDID]);
}

RCT_EXPORT_METHOD(encryptMap:(NSString*) json
              url:(NSString*) url
              successCallback:(RCTResponseSenderBlock)successCallback
              errorCallback:(RCTResponseSenderBlock)errorCallback){
  if(json == NULL){
    errorCallback(@[@"encrypMap - content from RN is Null"]);
    return;
  }
  
//  NSLog(@"encryptMap json=%@",json);
  NSString *allPayPackage, *allPayEncrypt;
  allPayPackage=[allpaylib allPayPackage:json]; // 將json 轉成 {header:{} , Data:{}}
//  NSLog(@"allPayPackage=%@",allPayPackage);
  allPayEncrypt=[allpaylib allPayEncrypt :allPayPackage :url];  //package 後才有 SecreKey
//  NSLog(@"allPayEncrypt=%@",allPayEncrypt); // {Request:"" , ApiKey:""}
  NSString *secretKey=[allpaylib getSecreKey];
//  RCTLogInfo(@"encryptMap allPayEncrypt=%@",allPayEncrypt);
//  NSLog(@"encryptMap secreKey=%@",secretKey);

//非常容易null，不知道為什麼，但好像不太影響結果，所以先助解掉
//  if(allPayPackage == NULL)
//    errorCallback(@[@"allPayPackage null"]);
//  if(allPayEncrypt == NULL)
//    errorCallback(@[@"allPayEncrypt null"]);
//  else if(secretKey == NULL)
//    errorCallback(@[@"secretKey null"]);
//  else
    successCallback(@[allPayEncrypt, secretKey]);
}

RCT_EXPORT_METHOD(decryptResponse:(NSString*) secretKeyStr
                  response:(NSString*) response
                  successCallback:(RCTResponseSenderBlock)successCallback
                  errorCallback:(RCTResponseSenderBlock)errorCallback){
  if(secretKeyStr == NULL || response == NULL){
    errorCallback(@[@"decryptResponse - content from RN is Null"]);
    return;
  }
//  NSLog(@"decryptResponse secretKeyStr=%@",secretKeyStr);
//  NSLog(@"decryptResponse response=%@",response);
  
//  NSString *rspEncryptMsg=[allpaylib allpayPost:allPayEncrypt :SANDBOX_URL];
//  NSLog(@"rspEncryptMsg=%@",rspEncryptMsg);
//  NSLog(@"secretKeyStr=%@",secretKeyStr);
  NSError *theError = NULL;
  //get Response from response
  NSDictionary *theDictionary = [NSDictionary dictionaryWithJSONString:response error:&theError];
  NSString *msg=[theDictionary objectForKey:@"Response"];
//  NSLog(@"response=%@",msg);
  NSString *rsp_decrypt=[allpaylib AESDecryptWithKey :msg:secretKeyStr];
//  NSLog(@"rsp_decrypt=%@",rsp_decrypt);
//  RCTLogInfo(@"decryptResponse rsp_decrypt=%@",rsp_decrypt);
  if(theError != NULL){
    errorCallback(@[@"decryptResponse - json to Dictionary fail."]);
  }else if(msg == NULL || rsp_decrypt == NULL){
    errorCallback(@[@"decryptResponse content is Null"]);
  }else{
    successCallback(@[rsp_decrypt]);
  }
}

//尚未測試
RCT_EXPORT_METHOD(needsUpdate:(RCTResponseSenderBlock)callback) {
  NSDictionary* infoDictionary = [[NSBundle mainBundle] infoDictionary];
  NSString* appID = infoDictionary[@"CFBundleIdentifier"];
//  NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/lookup?bundleId=%@", appID]];
  NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/lookup?bundleId=%@", appID]];
  NSData* data = [NSData dataWithContentsOfURL:url];
  NSDictionary* lookup = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
  if ([lookup[@"resultCount"] integerValue] == 1){
    NSString* appStoreVersion = lookup[@"results"][0][@"version"];
    NSString* currentVersion = infoDictionary[@"CFBundleShortVersionString"];
    NSLog(@"appStoreVersion=%@",appStoreVersion);
    NSLog(@"currentVersion=%@",currentVersion);
    if (![appStoreVersion isEqualToString:currentVersion]){
      NSLog(@"Need to update [%@ != %@]", appStoreVersion, currentVersion);
      callback(@[@"YES"]);
      return;
    }
  }
  callback(@[@"NO"]);
}


RCT_EXPORT_METHOD(emitterAPNsTokenTest:(NSString*) str){
  RCTLogInfo(@"emitterAPNsTokenTest, %@", str);
  APNsTokenEmitter *tokenEmitter = [APNsTokenEmitter allocWithZone:nil];
  [tokenEmitter apnsTokenReceive:str];
}

RCT_EXPORT_METHOD(getAPNsToken:(RCTResponseSenderBlock)callback){
  APNsTokenEmitter *tokenEmitter = [APNsTokenEmitter allocWithZone:nil];
  RCTLogInfo(@"getAPNsToken, %@", tokenEmitter.apnsToken);
  if(tokenEmitter.apnsToken == nil) {
    callback(@[@""]);
  }else {
    callback(@[tokenEmitter.apnsToken]);
  }
}

@end


