//
//  AllPayPassMethod.h
//  AllPayPassRN
//
//  Created by Lisa on 2017/6/7.
//  Copyright © 2017年 Facebook. All rights reserved.
//

//匯入RCTBridgeModule標頭
#import <React/RCTBridgeModule.h>
//宣告AllPayPassMethod類別是NSObject的子類別並實作RCTBridgeModule界面，以@end結束界面得宣告。
@interface AllPayPassMethod : NSObject <RCTBridgeModule>
@end

