//
//  APNsTokenEmitter.m
//  AllPayPassRN
//
//  Created by Lisa on 2018/1/17.
//  Copyright © 2018年 Facebook. All rights reserved.
//
#import "APNsTokenEmitter.h"
#import <React/RCTLog.h>

@implementation APNsTokenEmitter
{
  bool hasListeners;
}

RCT_EXPORT_MODULE();
@synthesize apnsToken;
/**
 想再其它.m檔內使用，得先取得APNsTokenEmitter，但用下方方法初始化會有Error
  APNsTokenEmitter * tokenEmitter = [[APNsTokenEmitter alloc] init];
 
 請參考 https://github.com/facebook/react-native/issues/15421 解法（singleton pattern）
 **/
+ (id)allocWithZone:(NSZone *)zone {
  static APNsTokenEmitter *sharedInstance = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedInstance = [super allocWithZone:zone];
  });
  return sharedInstance;
}

-(NSArray<NSString *> *)supportedEvents
{
  return @[@"APNsToken"];
}

// Will be called when this module's first listener is added.
-(void)startObserving {
  RCTLogInfo(@"APNsTokenEmitter startObserving");
  hasListeners = YES;
  // Set up any upstream listeners or background tasks as necessary
}

// Will be called when this module's last listener is removed, or on dealloc.
-(void)stopObserving {
  RCTLogInfo(@"APNsTokenEmitter stopObserving");
  hasListeners = NO;
  // Remove upstream listeners, stop unnecessary background tasks
}

-(void)apnsTokenReceive:(NSString *)token
{
  self.apnsToken = token;
  if (hasListeners) {
    [self sendEventWithName:@"APNsToken" body:token];
    RCTLogInfo(@"APNsTokenEmitter apnsTokenReceive(Has Listener), %@", token);
  }else {
    RCTLogInfo(@"APNsTokenEmitterapnsTokenReceive(No Listener), %@", token);
  }
}

@end
