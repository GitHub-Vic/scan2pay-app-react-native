//
//  APNsTokenEmitter.h
//  AllPayPassRN
//
//  Created by Lisa on 2018/1/17.
//  Copyright © 2018年 Facebook. All rights reserved.
//
#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface APNsTokenEmitter : RCTEventEmitter<RCTBridgeModule>
@property (nonatomic, retain) NSString *apnsToken;
- (void)apnsTokenReceive:(NSString *)token;
@end
