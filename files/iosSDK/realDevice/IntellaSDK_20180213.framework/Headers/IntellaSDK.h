//
//  allpaylib.h
//  allpaylib
//
//  Created by intella on 2017/4/26.
//  Copyright © 2017年 intella. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <CommonCrypto/CommonCrypto.h>
#import <Foundation/Foundation.h>

static NSString *API_URL=@"https://a.intella.co/allpaypass/api/general";
static NSString *SANDBOX_URL=@"https://s.intella.co/allpaypass/api/general";

//============ Pi =================
/**
 * PiPayControllerSDK
 * this is used for Pi Credit cards, but not yet ready caused by Pi
 */
/*@interface PiPayControllerSDK : NSObject
 
 @end
 @protocol PiSDKPayViewControllerDelegate;
 
 @interface PiPayViewController : UIViewController
 
 - (instancetype)initWithParameterDictionary:(NSDictionary *)parameterDictionary
 delegate:(id<PiSDKPayViewControllerDelegate>)delegate;
 
 @property (nonatomic, weak) id<PiSDKPayViewControllerDelegate> delegate;
 @property (nonatomic, assign) BOOL isCellPhoneValidated;
 @property (nonatomic, assign) NSUInteger amount;
 @property (nonatomic, copy) NSString *orderId;
 @property (nonatomic, copy) NSString *partnerId;
 @property (nonatomic, copy) NSString *partnerName;
 @property (nonatomic, copy) NSString *userId;
 @property (nonatomic, copy) NSString *cellPhone;
 @property (nonatomic, copy) NSString *appUrlScheme;
 @end
 
 @protocol PiSDKPayViewControllerDelegate <NSObject>
 
 @required
 - (void)piPayViewController:(PiPayViewController *)piPayViewController
 paymentStatus:(BOOL)success
 errorCode:(NSString *)errorCode;
 
 @optional
 - (void)piPayViewControllerDidCancel:(PiPayViewController *)piPayViewController;
 - (void)piPayViewController:(PiPayViewController *)piPayViewController willPayByPiApp:(BOOL)success;
 @end*/

/*
 *  PiPaySDK
 *  this is used for Pi wallet
 */
@interface PiPaySDK : NSObject

+ (PiPaySDK *)sharedPiPaySDK;
- (void)willPayByPiAppWithParameterDictionary:(NSDictionary *)parameterDictionary
                            completionHandler:(void(^)(BOOL success))completionBlock;
@property (nonatomic, assign) BOOL isTestMode;
@end

/*@interface allpaylib : NSObject <PiSDKPayViewControllerDelegate> {
 
 }*/


/**
 * Pi
 */
@interface allpaylib : NSObject {
    //NSMutableDictionary *request;
}
//@property (nonatomic, retain) NSMutableDictionary *request;


//sharedAllPaySDK unused
+(allpaylib *) sharedAllPaySDK;

//setPiOrderNumber save pi order num as buffer
+(void)setPiOrderNumber:(NSString *)piOrderNum;
+(NSString *)getPiOrderNumber;  // get buffer
+(NSString *)piReturnProcess:(NSURL *)app_url :(NSString *)server_url;// convert errorCode from Pi response url
+(NSString *)scan2PayReturnProcess:(NSString *)app_url :(NSString *)server_url;
+(NSString *)piSingleOrderQuery:(NSString *)StoreOrderNo :(NSString *)app_url :(NSString *)server_url;//  request to scan2 pay to get trade result (Private)
+(NSString *)piWalletPay:(NSString *)requestJson :(NSString *)url;                  //  request to pi only(Private)
+(NSString *)piSingleOrderQuery:(NSString *)StoreOrderNo :(NSString *)url;          //  request to scan2 pay to get trade result (Private)

/*
 */
+(BOOL) initOpay:(UIApplication *)application :(NSDictionary *)launchOption;
+(BOOL) initOpayUrl:(UIApplication *) application :(NSURL *) url : (NSDictionary<UIApplicationOpenURLOptionsKey, id> *) options;
+(NSString *)walletPay:(NSString *)requestJson :(NSString *) url;
+(NSString *)singleOrderQuery:(NSString *)storeOrderNo :(NSString *)url :(NSString *) method :(NSString *) merchantId;
//+(void)PiWalletPayEx:(NSDictionary *) PiPayInfo completion:(void (^)(BOOL success))completionBlock;

//@property(nonatomic,retain) NSString *jsonPostRsp,*postMethodRsp,*getMethodRsp;
+(NSString *)showMessage: (NSString *)msg;  // for lisa ?
+(NSString *)encryptRSA:(NSString *)plainTextString key:(SecKeyRef)publicKey;   //
+(void)addObject:(NSString *)key :(NSString *)value;                            // add request scan2pay parameters to allpaylib:NSObject
+(void)addObject:(NSString *)key withInteger:(NSInteger)value;                  // add request scan2pay parameters to allpaylib:NSObject
+(void)removeAllObjects;                                                        // clear allpaylib
+(NSString *)convertJsonString;                                                 // convert allpaylib's request

+(NSString *)OneAppPay:(NSMutableDictionary *)requestJsonObj :(NSString *)url;  // main transaction call
+(NSString *)OneAppPayProc:(NSString *)requestJson :(NSString *)url;            // redundant
+(NSString *)allpayReq:(NSString *)requestJson :(NSString *)url;                // (Private) deal with scan2pay transaction
+(NSString *)allpayReqObj:(NSMutableDictionary *)requestJsonObj :(NSString *)url;// (Private) deal with scan2pay transaction
+(NSString *)allpayRsp:(NSString *)rspJson ;                                    // decrypt scan2 pay encrypt response

+(void)jsonPost:(NSString *)pData :(NSString *)pUrl;    //(Private) post
+(void)postMethod:(NSString *)pData :(NSString *)pUrl;  //(Private) post
+(void)getMethod:(NSString *)gData :(NSString *)gUrl;   //(Private) get
+(NSString *) randomStringWithLength: (int) len;        // random num
+(NSString *)AESOperation:(CCOperation)operation :(NSString *)content :(NSString *)key;
+(NSString *)AESEncryptWithKey:(NSString *)content :(NSString *)key;
+(NSString *)AESDecryptWithKey:(NSString *)content :(NSString *)key;

// return base64 encoded string
+ (NSString *)encryptString:(NSString *)str publicKey:(NSString *)pubKey;
// return raw data
+ (NSData *)encryptData:(NSData *)data publicKey:(NSString *)pubKey;
// return base64 encoded string
// enc with private key NOT working YET!
//+ (NSString *)encryptString:(NSString *)str privateKey:(NSString *)privKey;
// return raw data
//+ (NSData *)encryptData:(NSData *)data privateKey:(NSString *)privKey;

// decrypt base64 encoded string, convert result to string(not base64 encoded)
+ (NSString *)decryptString:(NSString *)str publicKey:(NSString *)pubKey;
+ (NSData *)decryptData:(NSData *)data publicKey:(NSString *)pubKey;
+ (NSString *)decryptString:(NSString *)str privateKey:(NSString *)privKey;
+ (NSData *)decryptData:(NSData *)data privateKey:(NSString *)privKey;
+ (SecKeyRef)addPublicKey:(NSString *)key;
+ (SecKeyRef)addPrivateKey:(NSString *)key;

//======== internal use =========
+ (NSString *)getUDID;      // redundant
+ (NSString *)getSecreKey;  //
+ (NSString *)allPayPackage:(NSString *)requestJson;    // redundant
+ (NSString *)allPayEncrypt:(NSString *)requestJson :(NSString *)url;    // redundant
+ (NSString *)allpayPost:(NSString *)requestJson :(NSString *)url; // redundant
@end
@interface AesCryptoUtil : NSObject{
}
//加密
+(NSString *)encrypt:(NSString *)key :(NSString *)data;
//解密
+(NSString *)decrypt:(NSString *)key :(NSString *)data;
@end
@interface RsaCryptoUtil : NSObject{
}
+(NSData *)PKCSSignBytesSHA256withRSA:(NSData *) plainData :(SecKeyRef) privateKey;
+(BOOL)PKCSVerifyBytesSHA256withRSA:(NSData*) plainData :(NSData*) signature :(SecKeyRef) publicKey;
+(NSString*)sha256:(NSString *)plaintext;
+(NSString *)sign:(NSString *)data :(NSString *)privKey;
+(BOOL)verify:(NSString *)data :(NSString *)signdata :(NSString *)pubKey;
+(NSString *)server_sign:(NSString *)data :(NSString *)priKey;
+(BOOL)server_verify:(NSString *)data :(NSString *)sign :(NSString *)pubKey;
+(NSString *)server_pub_encrypt:(NSString *)data :(NSString *)publicKey;
+(NSString *)server_pri_encrypt:(NSString *)data :(NSString *)privateKey;
+(NSString *)server_pub_decrypt:(NSString *)data :(NSString *)publicKey;
+(NSString *)server_pri_decrypt:(NSString *)data :(NSString *)privateKey;
@end

@interface NSString (Base64Addition)
+(NSString *)stringFromBase64String:(NSString *)base64String;
+(NSString *)stringFromBase64UrlEncodedString:(NSString *)base64UrlEncodedString;
-(NSString *)base64String;
-(NSString *)base64UrlEncodedString;
@end
void *NewBase64Decode(
                      const char *inputBuffer,
                      size_t length,
                      size_t *outputLength);

char *NewBase64Encode(
                      const void *inputBuffer,
                      size_t length,
                      bool separateLines,
                      size_t *outputLength);

@interface NSData (Base64)

+ (NSData *)dataFromBase64String:(NSString *)aString;
- (NSString *)base64EncodedString;

// added by Hiroshi Hashiguchi
- (NSString *)base64EncodedStringWithSeparateLines:(BOOL)separateLines;
@end

enum {
    kJSONSerializationOptions_EncodeSlashes = 0x01,
};
typedef NSUInteger EJSONSerializationOptions;


@interface CJSONSerializer : NSObject {
}

@property (readwrite, nonatomic, assign) EJSONSerializationOptions options;

+ (CJSONSerializer *)serializer;

- (BOOL)isValidJSONObject:(id)inObject;

/// Take any JSON compatible object (generally NSNull, NSNumber, NSString, NSArray and NSDictionary) and produce an NSData containing the serialized JSON.
- (NSData *)serializeObject:(id)inObject error:(NSError **)outError;

- (NSData *)serializeNull:(NSNull *)inNull error:(NSError **)outError;
- (NSData *)serializeNumber:(NSNumber *)inNumber error:(NSError **)outError;
- (NSData *)serializeString:(NSString *)inString error:(NSError **)outError;
- (NSData *)serializeArray:(NSArray *)inArray error:(NSError **)outError;
- (NSData *)serializeDictionary:(NSDictionary *)inDictionary error:(NSError **)outError;

@end

extern NSString *const kJSONSerializerErrorDomain /* = @"CJSONSerializerErrorDomain" */;

typedef enum {
    CJSONSerializerErrorCouldNotSerializeDataType = -1,
    CJSONSerializerErrorCouldNotSerializeObject = -2,
} CJSONSerializerError;

extern NSString *const kJSONDeserializerErrorDomain /* = @"CJSONDeserializerErrorDomain" */;

typedef enum {
    
    // Fundamental scanning errors
    kJSONDeserializerErrorCode_NothingToScan = -11,
    kJSONDeserializerErrorCode_CouldNotDecodeData = -12,
    kJSONDeserializerErrorCode_CouldNotScanObject = -15,
    kJSONDeserializerErrorCode_ScanningFragmentsNotAllowed = -16,
    kJSONDeserializerErrorCode_DidNotConsumeAllData = -17,
    kJSONDeserializerErrorCode_FailedToCreateObject = -18,
    
    // Dictionary scanning
    kJSONDeserializerErrorCode_DictionaryStartCharacterMissing = -101,
    kJSONDeserializerErrorCode_DictionaryKeyScanFailed = -102,
    kJSONDeserializerErrorCode_DictionaryKeyNotTerminated = -103,
    kJSONDeserializerErrorCode_DictionaryValueScanFailed = -104,
    kJSONDeserializerErrorCode_DictionaryNotTerminated = -106,
    
    // Array scanning
    kJSONDeserializerErrorCode_ArrayStartCharacterMissing = -201,
    kJSONDeserializerErrorCode_ArrayValueScanFailed = -202,
    kJSONDeserializerErrorCode_ArrayValueIsNull = -203,
    kJSONDeserializerErrorCode_ArrayNotTerminated = -204,
    
    // String scanning
    kJSONDeserializerErrorCode_StringNotStartedWithBackslash = -301,
    kJSONDeserializerErrorCode_StringUnicodeNotDecoded = -302,
    kJSONDeserializerErrorCode_StringUnknownEscapeCode = -303,
    kJSONDeserializerErrorCode_StringNotTerminated = -304,
    kJSONDeserializerErrorCode_StringBadEscaping = -305,
    kJSONDeserializerErrorCode_StringCouldNotBeCreated = -306,
    
    // Number scanning
    kJSONDeserializerErrorCode_NumberNotScannable = -401
    
} EJSONDeserializerErrorCode;

enum {
    // The first three flags map to the corresponding NSJSONSerialization flags.
    kJSONDeserializationOptions_MutableContainers = (1UL << 0),
    kJSONDeserializationOptions_MutableLeaves = (1UL << 1),
    kJSONDeserializationOptions_AllowFragments = (1UL << 2),
    kJSONDeserializationOptions_LaxEscapeCodes = (1UL << 3),
    kJSONDeserializationOptions_Default = kJSONDeserializationOptions_MutableContainers,
};
typedef NSUInteger EJSONDeserializationOptions;

@interface CJSONDeserializer : NSObject

/// Object to return instead when a null encountered in the JSON. Defaults to NSNull. Setting to null causes the deserializer to skip null values.
@property (readwrite, nonatomic, strong) id nullObject;

/// JSON must be encoded in Unicode (UTF-8, UTF-16 or UTF-32). Use this if you expect to get the JSON in another encoding.

@property (readwrite, nonatomic, assign) EJSONDeserializationOptions options;

+ (CJSONDeserializer *)deserializer;

- (id)deserialize:(NSData *)inData error:(NSError **)outError;

- (id)deserializeAsDictionary:(NSData *)inData error:(NSError **)outError;
- (id)deserializeAsArray:(NSData *)inData error:(NSError **)outError;

@end

@interface NSDictionary (NSDictionary_JSONExtensions)

+ (id)dictionaryWithJSONData:(NSData *)inData error:(NSError **)outError;
+ (id)dictionaryWithJSONString:(NSString *)inJSON error:(NSError **)outError;

@end

