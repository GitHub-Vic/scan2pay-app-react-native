import {Platform} from 'react-native'
import store from '../store'
import * as settingsActions from '../actions/settingsActions'
import {URL_APN_REGISTER, URL_APN_UNREGISTER, APP_ID} from '../config'
import {mapToJsonString, consoleForDebug} from './functions'

var checkAPNsTokenRegisterIntervalSec = 1 * 60
var intervalArray = []

export default () => ({
  startCheckAPNsToken() {
    checkTokenIsNeedRegister()
    var array = intervalArray.slice()
    var intervalId = setInterval(()=>
      checkTokenIsNeedRegister(), checkAPNsTokenRegisterIntervalSec *1000)
    array.push(intervalId)
    intervalArray = array
  },

  stopCheckAPNsToken() {
    var intervals = intervalArray.slice();
    while(intervals.length){
        var interval = intervals.pop();
        interval && clearInterval(interval);
    }
    intervalArray = intervals
  },

  register() {
    checkTokenIsNeedRegister()
  },

  unregister() {
    checkTokenIsNeedRegister()
  },
  
})

checkTokenIsNeedRegister = () => {
  consoleForDebug(`checkTokenIsNeedRegister()`)
  var state = store.getState().settings
  if(state.isLogin) {
    if(state.hasRegisterAPNsToken) {
      consoleForDebug(`checkTokenIsNeedRegister() - need register`)
      if(state.udid.length < 0) {
        consoleForDebug(`not register(apnsToken) beacause udid:'${state.udid}'`)
        return
      }
      if(state.apnsToken && state.apnsToken.length > 0) {
        registerToken(state.merchantId, state.udid, state.apnsToken)
        return
      }
      consoleForDebug(`not register(apnsToken) beacause apnsToken:'${state.apnsToken}'`)
      return
    }
  }

  if(state.hasUnregisterAPNsToken) {
    consoleForDebug(`checkTokenIsNeedRegister() - need unregister`)
    if(state.udid && state.udid.length > 0) {
      unregisterToken(state.udid)
      return
    }
    consoleForDebug(`not unregister(apnsToken) beacause udid:'${state.udid}'`)
    return
  }
  
}

registerToken = async (mchId, udid, token) => {
  consoleForDebug(`registerToken() - mchId:${mchId},udid:${udid},token:${token}`)
  var requestData = new Map()
  requestData.set("mchId", mchId)
  requestData.set("udid", udid)
  requestData.set("token", token)
  requestData.set("appId", APP_ID)
  var msg = await httpsPost(URL_APN_REGISTER, mapToJsonString(requestData))
  console.log('URL_APN_REGISTER msg', msg)
  if('OK' === msg) {
    store.dispatch(settingsActions.alreadyRegisterAPNsToken())
    consoleForDebug(`register(apnsToken) success, apnsToken:'${token}'`)
  }else {
    consoleForDebug(`register(apnsToken) fail, apnsToken:'${token}'${'\n'}${msg}`)
  }
}

unregisterToken = async (udid) => {
  consoleForDebug(`unregisterToken() - udid:${udid}`) 
  var requestData = new Map()
  requestData.set("udid", udid)
  requestData.set("appId", APP_ID)
  var msg = await httpsPost(URL_APN_UNREGISTER, mapToJsonString(requestData))
  if('OK' === msg) {
    store.dispatch(settingsActions.alreadyUnregisterAPNsToken())
    consoleForDebug(`unregister(apnsToken) success, udid:'${udid}'`)
  }else {
    consoleForDebug(`unregister(apnsToken) fail, udid:'${udid}'${'\n'}${msg}`)
  }
}

async function httpsPost(url, bodyData){
  try{
    let response = await fetch(url,
    {
        headers: {
        // 'Accept': 'application/json',
        'Content-Type': 'application/json; charset=UTF-8',
        },
        method: "POST",
        body: bodyData
    })
    return response.text()
  }catch(error){
    return `Error:${error}`
  }
}