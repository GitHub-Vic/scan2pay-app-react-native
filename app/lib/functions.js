import React, {Component} from 'react'
import { NavigationActions } from 'react-navigation'
import {Platform,Alert} from 'react-native'
import {windowH, windowW, fontSizeScaler} from '../commonStyles'
import {DEBUGGABLE} from '../config'
// 初始化moment.js
var moment = require('moment');
moment().format();

export function _formatDate(date){
  return moment(date, 'YYYYMMDDHHmmss').format('YYYY/MM/DD HH:mm:ss')
}

export function goHome(navigation) {
  const resetAction = NavigationActions.reset({
    index: 0,
    actions: [
      NavigationActions.navigate({ routeName: 'HomePage'})
    ]
  })
  navigation.dispatch(resetAction)
}

export function getRandomString(randomLength) {
  var seeds = ['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','0','1','2','3','4','5','6','7','8','9'];
  var len = randomLength;
  var randStr = "";
  for (var i = 0; i < len; i++)
  {
      randStr += seeds[Math.round(Math.random() * (seeds.length - 1))];
  }
  return randStr;
}

export function getDateToMillsecode(){
  let time = moment().format('YYMMDDHHmmssMS')
  return time.substr(0,time.length-1);
}

export function dateFormat(Date) {
  var mm = Date.getMonth() + 1;
  var dd = Date.getDate();
  var hh = Date.getHours();
  var minutes = Date.getMinutes();
  var s = Date.getSeconds();
  return Date.getFullYear() +
          (mm > 9 ? '' : '0') + mm +
          (dd > 9 ? '' : '0') + dd +
          (hh > 9 ? '' : '0') + hh +
          (minutes > 9 ? '' : '0') + minutes +
          (s > 9 ? '' : '0') + s;
      
}

export function getNowTime(){
  var today = new Date();
  return dateFormat(today);
}

export function showDateTime(str) {
  return str.substring(0,4) + '-' +
          str.substring(4,6) + '-' +
          str.substring(6,8)
}

export function stringToDate(str) {
  try {
    return new Date(str.substring(0,4), str.substring(4,6) - 1, str.substring(6,8), '00', '00', '00')
  }catch(e) {
    return new Date()
  }
}

// iPhoneX
export const PORTRAIT = 'PORTRAIT'
export const LANDSCAPE = 'LANDSCAPE'
const X_WIDTH = 375
const X_HEIGHT = 812
const portraitStyle = {marginTop: 44*fontSizeScaler, marginBottom: 34*fontSizeScaler}
const landscapeStyle = {marginLeft: 44*fontSizeScaler,marginRight: 44*fontSizeScaler, marginBottom: 34*fontSizeScaler}

function isIphoneX() {
  return (
    Platform.OS === 'ios' &&
      ((windowH === X_HEIGHT && windowW === X_WIDTH) ||
      (windowH === X_WIDTH && windowW === X_HEIGHT))
  )
}

export function forIphoneXStyleByOrientation(orientation) {
  var style
  if(isIphoneX()) {
    if('LANDSCAPE' === orientation)
      style = landscapeStyle
    else
      style = portraitStyle

    return style
  }else {
    return {}
  }
}


export function getValueByKeyFromJsonObject(item, key) {
  try{
    if(null === item[key] || undefined === item[key]) {
      if('systemOrderId' === key ) {
        return '支付方未提供';
      }
    }
    return item[key];
  }catch(e){
    return '';
  }
}

export function mapToJsonString(map){
  var json = "{";
  for (var [key, value] of map) {
     json += `"${key}":"${value}",`;
   }
   json = json.substring(0, json.length-1);
   json += "}";
   return json;
}

export async function httpsPost(url, bodyData){
  try{
    let response = await fetch(url,
    {
        headers: {
          // 'Accept': 'application/json',
          'Content-Type': 'application/json; charset=UTF-8',
        },
        method: "POST",
        body: bodyData
    });
    let res = await response.json();
    return JSON.stringify(res);
  }catch(error){
    return `Error:${error}`;
  }
}

export function consoleForDebug(msg) {
  if(DEBUGGABLE === 'true') {
    console.log(msg)
  }
}

import { NativeModules,} from 'react-native';
var AllPayPassMethod = NativeModules.AllPayPassMethod;
export function nativeEncryptJson(json, url, sucessCallback, errorCallback){
  consoleForDebug(`(Request)${url}\n${json}`);
  AllPayPassMethod.encryptMap(json, url,
    async(successMsg, secretKey) => {
      sucessCallback(successMsg, secretKey);
    },
    (errMsg) => {
      errorCallback(errMsg);
    });
}

export function nativeDecryptResponse(secretKey, encryptResponse, sucessCallback, errorCallback){
  AllPayPassMethod.decryptResponse(secretKey, encryptResponse,
    (successMsg) => {
      sucessCallback(successMsg);
    },
    (errMsg) => {
      errorCallback(errMsg);
    });
}

function nativeGetPhoneOnlyNumber() {
  AllPayPassMethod.getPhoneOnlyNumber((msg)=> {console.log(`phone only number:${msg}`)});
}

//尚無法測試 沒bundleId 和上架
function appNeedsUpdate() {
  AllPayPassMethod.needsUpdate((msg)=> {console.log(`appNeedsUpdate:${msg}`)});
}

export function bridgeTest() {
  AllPayPassMethod.bridgeTest(`RN BridgeTest`)
}

export function timeOutMinute(s , actions){
  return setTimeout(()=>{
          fetch('https://www.google.com/').then((res)=>{
              if(!res.ok){
                  Alert.alert("網路狀態異常,請檢查網路是否正常")
              }else{
                  Alert.alert("網路狀態正常,請再嘗試一次")
              }
          })
          actions.endFetch("");
  },1000*s)
}