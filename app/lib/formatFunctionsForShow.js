//for show RecordPage & MessagePage 's data
import { commonColor } from '../commonStyles'
const ALLPAYPASS_ONLINE = 'OLPay'

export const REFUND_STATUS_REFUND_SUCCESS = 'Refund success'
export const REFUND_STATUS_CANCEL_SUCCESS = 'Cancel success'

const ORDER_STATUS_CREATINGORDER = 'Creating order'
const ORDER_STATUS_WAITING = 'Waiting'
const ORDER_STATUS_TRADE_SUCCESS = 'Trade success'
const ORDER_STATUS_TRADE_FAIL = 'Trade fail'
const ORDER_STATUS_INTERRUPTED = 'Interrupted'

//time = 時間字串 ex.20171010000000
export function formatTime(time) {
  try {
    return (
      time.substring(0, 4) +
      '-' +
      time.substring(4, 6) +
      '-' +
      time.substring(6, 8) +
      ' ' +
      time.substring(8, 10) +
      ':' +
      time.substring(10, 12) +
      ':' +
      time.substring(12, 14)
    )
  } catch (e) {
    return ''
  }
}

export function formatSystemOrderId(orderId) {
  try {
    const length = orderId.length
    if (length < 8) return orderId
    return orderId.substring(length - 8, length)
  } catch (e) {
    return ''
  }
}

/**
 * return String
 * @param {*} item 交易紀錄裡面的一個JsonObject
 */
export function formatMethod(item) {
  try {
    switch (item.method) {
      case '10110':
        return '微信'
      case '10220':
      case '10520':
        if (ALLPAYPASS_ONLINE === item.serviceType) return '支付寶 線上'
        else return '支付寶 線下'
      case '10300':
        return 'Pi拍錢包'
      case '10500':
        return '歐付寶'
      case '10900':
        return '街口支付'
      case '11100':
        return '元大支付寶線下'
      case '11300':
        return '橘子支'
      case '11400':
        return '愛貝'
      case '11500':
        return 'LINE Pay'
      case '12000':
        return '台灣Pay'
      case '12300':
        return 'Friday'
      case '12400':
        return 'ezPay'
      case '12520':
        return '上海支付寶'
      case '12710':
        return '中信微信'
      case '20400':
        return '信用卡(大眾)'
      case '20600':
        return '智付通'
      case '20800':
        return '信用卡(台新)'
      case '20840':
        return 'ApplePay(台新)'
      case '21100':
        return '綠界'
      case '22600':
        return '信用卡(上海商銀)'
      case '23240':
        return 'ApplePay(富邦)'
      case '31800':
        return '悠遊卡'
      case '32800':
        return 'iCash卡'
      case '60300':
        return 'Pi拍錢包'
      case '60500':
        return '歐付寶'
      case '61300':
        return '橘子支'
      case '61500':
        return 'LINE Pay'
      case '62000':
        return '台灣Pay'
      case '62300':
        return 'Friday'
      case '62400':
        return 'ezPay'
      case '13100':
        return '悠遊付'
      default:
        return '其他支付'
    }
  } catch (e) {
    return '其他(支付)'
  }
}

/**
 * return orderStatus(String) textColor(String)
 * @param {*} item 交易紀錄裡面的一個JsonObject
 */
export function formatTradeStatus(item) {
  try {
    var refundStatus = item.refundStatus
    if (
      REFUND_STATUS_REFUND_SUCCESS === refundStatus ||
      REFUND_STATUS_CANCEL_SUCCESS === refundStatus
    ) {
      var refundTime
      if (
        item.hasOwnProperty('refundDetails') &
        (!'null' === item.refundDetails)
      ) {
        refundTime = item.refundDetails[0].createTime
      }
      return {
        orderStatus: '退款成功',
        textColor: commonColor.warning,
        refundTime: refundTime
      }
    }
    var tradeStatus = item.status
    switch (tradeStatus) {
      case ORDER_STATUS_CREATINGORDER:
        return {
          orderStatus: '訂單建立中',
          textColor: commonColor.hintTextColor
        }

      case ORDER_STATUS_WAITING:
        return {
          orderStatus: '等待交易中',
          textColor: commonColor.hintTextColor
        }

      case ORDER_STATUS_TRADE_SUCCESS:
        return { orderStatus: '交易成功', textColor: commonColor.success }

      case ORDER_STATUS_TRADE_FAIL:
        return { orderStatus: '交易失敗', textColor: commonColor.darkGrayColor }

      case ORDER_STATUS_INTERRUPTED:
        return {
          orderStatus: '交易中斷',
          textColor: commonColor.defaultTextColor
        }

      default:
        return { orderStatus: '其他狀態', textColor: commonColor.hintTextColor }
    }
  } catch (e) {
    return '其他(狀態)'
  }
}

/**
 * return canRefund(boolean) showPrompt(boolean)
 * @param {*} item item 交易紀錄裡面的一個JsonObject
 */
export function analyticRefundBtnAndPrompt(item) {
  if (item == undefined) return { canRefund: true, showPrompt: false }
  if (item.status == undefined) return { canRefund: true, showPrompt: false }

  var refundStatus
  if (item.refundStatus == undefined) refundStatus = ''
  else refundStatus = item.refundStatus

  var tradeStatus = item.status

  //easy card
  if ('31800' === item.method) {
    return { canRefund: false, showPrompt: false }
  }

  if (ORDER_STATUS_TRADE_FAIL !== tradeStatus) {
    if (
      REFUND_STATUS_REFUND_SUCCESS === refundStatus ||
      REFUND_STATUS_CANCEL_SUCCESS === refundStatus
    ) {
      return { canRefund: false, showPrompt: false }
    } else if (ORDER_STATUS_TRADE_SUCCESS === tradeStatus) {
      if (canRefundOrCancel(item)) {
        return { canRefund: true, showPrompt: false }
      } else {
        return { canRefund: false, showPrompt: true }
      }
    } else return { canRefund: false, showPrompt: false }
  } else {
    return { canRefund: false, showPrompt: false }
  }

  return { canRefund: true, showPrompt: false }
}

/**
 * return boolean 可否按退款按鈕
 * @param {*} item item 交易紀錄裡面的一個JsonObject
 */
function canRefundOrCancel(item) {
  if ('10220' === item.method) {
    //支付寶
    if (ALLPAYPASS_ONLINE === item.serviceType) return false
    else return true
  } else if ('10500' === item.method) {
    var now = new Date().getTime()
    var createYear = item.createDate.substring(0, 4)
    var createMonth = item.createDate.substring(4, 6)
    var createDate = item.createDate.substring(6, 8)
    var createHour = item.createDate.substring(8, 10)
    var createMim = item.createDate.substring(10, 12)
    var createSec = item.createDate.substring(12, 14)

    var createTime = new Date(
      createYear,
      createMonth - 1,
      createDate,
      createHour,
      createMim,
      createSec
    ).getTime()
    var timeP = now - createTime
    var day = timeP / (3600 * 24 * 1000) //=天
    if (createTime === 0) return false
    //歐付寶
    if (ALLPAYPASS_ONLINE === item.serviceType) {
      if (item.type === 1) {
        //錢包
        if (day > 2) return true
        else return false
      } else if (item.type === 2) {
        //信用卡
        if (day > 8) return true
        else return false
      } else return false
    } else return true
  } else {
    return true
  }
}

//money = 數字
export function formatMoneyToCama(money) {
  return money.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
}

//show messages for merchId
export function messagesForMerchId(state) {
  var merchId = state.settings.merchantId
  var merchIdMess = state.message.message.filter(m => m.merchantId === merchId)
  var newArray = []
  if (merchIdMess instanceof Array) {
    var count = merchIdMess.length
    var newArray =
      count > 0
        ? merchIdMess.map(m => (true ? { ...m, index: count-- } : m))
        : merchIdMess

    return newArray
  } else {
    return []
  }
}
