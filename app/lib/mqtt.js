
import {Platform} from 'react-native'
import store from '../store'
import * as messageActions from '../actions/messageActions'
import {HOST_MQTT, PORT_MQTT, ENVIRONMENT} from '../config'
import {consoleForDebug} from '../lib/functions'
require('./pahoMqttSource')

var checkMqttIntervalSec = 1 * 60
var mqttClient
var topic
var intervalArray = []
const simulatorUdid = Platform.select({
  ios: 'simulatorIOS',
  android: 'simulatorAndroid',
});

var tmpMessages = []
var timerId
var receiveMessInterval = 100

export default () => ({
  startConnectMqtt() {
    connectMqtt(false)
    var array = intervalArray.slice()
    var intervalId = setInterval(()=>
        connectMqtt(false), checkMqttIntervalSec *1000)
    array.push(intervalId)
    intervalArray = array
  },

  stopConnectMqtt() {
    var intervals = intervalArray.slice();
    while(intervals.length){
        var interval = intervals.pop();
        interval && clearInterval(interval);
    }
    intervalArray = intervals
    disconnectMqtt()
  },

  connectForLogin() {
    connectMqtt(true)
  },

  disConnectForLogout() {
    disconnectMqtt()
  },

})

connectMqtt = (isNewLogin) => {
  consoleForDebug(`connectMqtt()`)
  
  const settingsState = store.getState().settings
  const isLogin = settingsState.isLogin
  const merchantId = settingsState.merchantId
  var udid = settingsState.udid
  var isCleanMqttSession = settingsState.isCleanMqttSession
  if(isNewLogin) {
    isCleanMqttSession = true
  }
  const merchantState =  store.getState().merchant
  let merchants = merchantState.merchants
  if (merchantState.tokenId === '' && merchantState.userId === '') {
    merchants = [{accountId: merchantId}]
  }
  console.log('merchants', merchants)
  // consoleForDebug(`ENVIRONMENT:${ENVIRONMENT}`)
  switch (ENVIRONMENT){
    case 'pro':
      topic = merchants.map(item => item.accountId);
      break; 
    case 'stage':
      topic = merchants.map(item => `${item.accountId}-stage`);
      break;
    case 'dev':
      topic = merchants.map(item => `${item.accountId}-dev`);
      break; 
    }
  if(isLogin) {
    // if(merchantId.length <= 0)
    //   return
    if(mqttClient && mqttClient.isConnected())
      return
    if(udid.length <= 0)
      udid = simulatorUdid

    console.log(topic)
    consoleForDebug(`topic:${topic}`)
    consoleForDebug(`udid:${udid}`)
    consoleForDebug(`HOST_MQTT:${HOST_MQTT}`)
    consoleForDebug(`PORT_MQTT:${PORT_MQTT}`)
    mqttClient = new Paho.MQTT.Client(HOST_MQTT, PORT_MQTT, udid)
    mqttClient.onConnectionLost = onConnectionLost
    mqttClient.onMessageArrived = onMessageArrived
    mqttClient.connect({
    onSuccess:onConnect, 
    onFailure:onFailure, 
    // keepAliveInterval: 5,
    cleanSession: isCleanMqttSession});
  }else {
    disconnectMqtt()
  }
}

disconnectMqtt = () => {
  if(mqttClient) {
    if(mqttClient.isConnected()) {
      mqttClient.disconnect()
      consoleForDebug(`mqtt disconnect`)
    }
    mqttClient = null
  }
}

onConnectionLost = (responseObject) => {
  if (responseObject.errorCode !== 0) 
    consoleForDebug("onConnectionLost:"+responseObject.errorMessage)
}

onConnect = () => {
  consoleForDebug("mqtt connected");
  store.dispatch(appActions.settingsActions.setIsCleanMqttSession(false))
  for (var i = 0; i < topic.length; i ++) {
    console.log('topic[i] ', topic[i])
    mqttClient.subscribe(topic[i], {qos: 2});
  }
  consoleForDebug(`mqtt subscribe ${topic} with qos:2`);
}

onFailure = (e) => {
  consoleForDebug(`mqtt connect fail..:\n${e.errorMessage}`);
}

onMessageArrived = (message) => {
  consoleForDebug("onMessageArrived:"+message.payloadString)
  try{
    var mes = JSON.parse(message.payloadString)
    if(mes instanceof Object) {
      if(mes.orderId && mes.orderId.length > 1) {
        loadingMessages(mes)
      }
    }
  }catch(e){
    consoleForDebug(`onMessageArrived Catch:${e}\nMessage.payloadString:${message.payloadString}`)
  }
  
}

loadingMessages = (message) => {
  if(timerId != undefined) 
    timerId && clearTimeout(timerId)
  
  tmpMessages.unshift(message)

  timerId = setTimeout(() => {
    store.dispatch(messageActions.addMessage(tmpMessages))
    tmpMessages = []
  }, receiveMessInterval)
}

