import Config from 'react-native-config'

export const ENVIRONMENT = Config.ENVIRONMENT
export const SOFTWARE_VERSION = Config.SOFTWARE_VERSION
export const URL_INTELLA_DOMAIN = Config.URL_INTELLA_DOMAIN
export const URL_INTELLA_OAUTH2_DOMAIN = Config.URL_INTELLA_OAUTH2_DOMAIN
export const URL_DOMAIN = Config.URL_DOMAIN
export const HOST_MQTT = Config.HOST_MQTT
export const DEBUGGABLE = Config.DEBUGGABLE
export const APP_VERSION = Config.APP_VERSION
export const APP_ID = Config.APP_ID
export const APP_STORE_ID = Config.APP_STORE_ID
export const PLATFORM = Config.PLATFORM
export const URL_SCHEME = Config.URL_SCHEME

export const PORT_MQTT = 61614
export const URL_INTELLA_GENERAL = URL_INTELLA_DOMAIN + "general"
export const URL_LOGIN = URL_INTELLA_DOMAIN + 'system/app/login'
export const URL_LOGOUT = URL_INTELLA_DOMAIN + 'system/app/logout'
export const URL_APN_REGISTER = URL_INTELLA_DOMAIN + 'apns/register'
export const URL_APN_UNREGISTER = URL_INTELLA_DOMAIN + 'apns/unregister'
export const URL_INTELLA_FORGETPASSWORD = URL_INTELLA_DOMAIN + 'system/forgetPassword'
export const URL_INTELLA_ORDER = URL_INTELLA_DOMAIN + 'order'