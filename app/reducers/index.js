import receipt from './receiptReducers'
import settings from './settingsReducers'
import record from './recordReducers'
import message from './messageReducers'
import easycard from './easycardReducers'
import merchant from './merchantReducers'

export default appReducers = {
  receipt,
  settings,
  record,
  message,
  easycard,
  merchant
}