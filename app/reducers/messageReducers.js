import * as types from '../constants/actionTypes'
import {consoleForDebug} from '../lib/functions'
import store from '../store'


const initialState = {
  isDeleteMode: false,
  message: [],//sample=>app/components/messagePage/message.json
};
  

export default function message(state = initialState, action) {
  let selectedMerchantId
  if (store && store.getState().settings.merchantId) {
    selectedMerchantId = store.getState().settings.merchantId
  }
  
  switch (action.type){
    case types.SET_ALL_MESSAGE:
      var newArray = [];
      if(action.message instanceof Array) {
        var count = action.message.length;
        var newArray = count > 0 ?
          action.message.map(m=> true ? {...m, index:count--} : m) 
          : action.message;
      }
      return { 
        ...state,
        message: newArray,
      }

    case types.ADD_MESSAGE:
      //multiple messages (JSONArray)
      if(action.message instanceof Array) {
        consoleForDebug(`multiple messages`)
        if(action.message.length <= 0)
          return state
        const set = new Set();
        let filterOrderIdArray = action.message.filter(item => !set.has(item.orderId) ? set.add(item.orderId) : false);
        let filteredArray = filterOrderIdArray.filter(outerItem => {
          const duplicateMessage = state.message.find(item => item.orderId === outerItem.orderId)
          if (duplicateMessage) {
            return false
          } else {
            return true
          }
        })
        var newArray = filteredArray.concat(state.message)
        var count = newArray.length
        return { 
          ...state,
          message: newArray.map(m=> true ? {...m, index:count--} : m),
        }
      }else {
        //single message (JSONObject)
        consoleForDebug(`single message`)
        var message;
        try {
          message = JSON.parse(action.message);
          if(!message.orderId && message.orderId.length < 1) {
            return state
          }
          const duplicateMessage = state.message.find(item => item.orderId === message.orderId)
          if (duplicateMessage) {
            return state
          }
        }catch(e) {
          return state
        }
        
        var index = state.message.length ?  state.message.length+1 : 1;
        return {
          ...state,
          message: [
            {...message, isRead: false, isCheck: false, index: index},
            ...state.message
          ]
        }
      }

    case types.SET_READ:
      return {
        ...state,
        message: state.message.map(m=> m.orderId === action.orderId ?
          {...m, isRead: true} : m  
        )
      }
      
    case types.SET_ALL_READ:
      return {
        ...state,
        message: state.message.map(m=> m.merchantId ===  selectedMerchantId?
          {...m, isRead: true} :{...m, isRead: false} 
        )
      }
      
    case types.TOGGLE_DELETE_MODE:
      return {
        ...state,
        isDeleteMode: !state.isDeleteMode,
        message: state.message.map(m=> true ?
          {...m, isCheck: false} :{...m, isCheck: false} 
        )
      }
    
    case types.TOGGLE_IS_CHECK:
      return {
        ...state,
        message: state.message.map(m=> m.orderId === action.orderId ?
          {...m, isCheck: !m.isCheck} : m  
        )
      }

    case types.SET_ISCHECK_BY_COUNT:
      let newMessage = []
      let counter = 0
      for (let i = state.message.length - 1; i >= 0; i -- ) {
        if (state.message[i].merchantId ===  selectedMerchantId && action.forwardCount > counter) {
          newMessage.unshift({...state.message[i], isCheck: action.isCheck})
          counter++
        } else {
          newMessage.unshift({...state.message[i]})
        }
      }
      return {
        ...state,
        message: newMessage
      }

    case types.DELETE_CHECK_MESSAGE:
      var newArray = state.message.filter(m=> m.merchantId !==  selectedMerchantId || !m.isCheck);
      return {
        ...state,
        isDeleteMode: false,
        message: newArray,
      }

    
    default:
      return state
  }
}