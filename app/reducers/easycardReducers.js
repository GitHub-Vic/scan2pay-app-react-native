import * as types from '../constants/actionTypes'
import {REFUND_STATUS_REFUND_SUCCESS, REFUND_STATUS_CANCEL_SUCCESS} from '../lib/formatFunctionsForShow'

const initialState = {
    isFetching: false,
    serviceType: '',
    fetchResponse: '',
    easycardDevice: '',
};

export default function easycard(state = initialState, action) {
    switch(action.type) {
      case types.FETCH_EZC:
        return {
          ...state,
          isFetching: true,
          serviceType: action.serviceType,
        }

      case types.FETCH_EZC_END:
        return {
          ...state,
          isFetching: false,
          fetchResponse: action.fetchResponse,
          serviceType: '',
        }

      case types.SET_EASYCARD_DEVICE:
        return {
          ...state,
          easycardDevice: action.easycardDevice
        }

      case types.CLEAN_EASYCARD_DEVICE:
        return {
          ...state,
          easycardDevice: ''
        }


      // case types.FETCH_RECORD:
      //   return {
      //     ...state,
      //     isFetching: true,
      //   }

      // case types.END_FETCHING:
      //   try{
      //     var json = JSON.parse(action.response)
      //     if(!json.hasOwnProperty('ServiceType')) {
      //       json = {...json, time: action.time}
      //       return {
      //         ...state,
      //         isFetching: false,
      //         fetchResponse: JSON.stringify(json)
      //       }
      //     }
      //   }catch(e){
      //   }
      //   return {
      //     ...state,
      //     isFetching: false,
      //     fetchResponse: action.response
      //   }

      // case types.RESET_RECORD:
      //   return {
      //     ...initialState
      //   }

      // case types.SET_RECORD:
      //   return {
      //     ...state,
      //     isFetching: false,
      //     nowPage: state.nowPage+1,
      //     record: state.record.concat(action.recordData),
      //   }

      // case types.SET_RECORD_NO_DATA:
      //   return {
      //     ...state,
      //     isFetching: false,
      //     noData: true,
      //     isEnd: true,
      //   }

      // case types.SET_RECORD_IS_END:
      //   return {
      //     ...state,
      //     isFetching: false,
      //     isEnd: true,
      //   }
      
      // case types.SET_ORDER_REFUND:
      //   return {
      //     ...state,
      //     record: state.record.map(r=>
      //       r.orderId === action.orderId ?
      //       {...r, 
      //         refundStatus: action.serviceType === types.SERVICE_TYPE.CANCEL? REFUND_STATUS_CANCEL_SUCCESS : REFUND_STATUS_REFUND_SUCCESS,
      //         refundDetails:[{createTime: action.createTime}]
      //       } :
      //       r
      //     )
      //   }

      default:
        return state
    }
}