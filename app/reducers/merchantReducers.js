import * as types from '../constants/actionTypes'

const initialState = {
    isFetching: false,
    merchants: [],
    fetchResponse: '',
    selectedMerchant: {},
    tokenId: '',
    userId: '',
    isFetchingTitle: '',
    ssoFlag: false,
};

export default function merchant(state = initialState, action) {
    switch(action.type) {
      case types.END_LOAD_SSO:
        return {
          ...state,
          isFetching: false,
        }
      case types.LOADING:
        return {
          ...state,
          isFetching: true,
          isFetchingTitle: action.isFetchingTitle || '加載中'
        }
      case types.PARSE_INSERT_USER_MERCHANT:
        return {
          ...state,
          isFetching: false,
        }
      case types.PARSE_DELETE_USER_MERCHANT:
        return {
          ...state,
          isFetching: false,
        }  
      case types.PARSE_UPDATE_USER_MERCHANT:
        return {
          ...state,
          isFetching: false,
        }
      case types.PARSE_TOKEN:
        let {userId, tokenId} = action.data.data
        return {
          ...state,
          ssoFlag: true,
          isFetching: false,
          tokenId,
          userId
        }  
      case types.PARSE_USER_MERCHANT:
        let merchants = action.data.data
        let selectedMerchant
        if (!state.selectedMerchant.accountId) {
          selectedMerchant = merchants.find((element) => {
            return element.isDefault === '1'
          })
        } else {
          let existSelectedMerchant = merchants.find((element) => {
            return element.accountId === state.selectedMerchant.accountId
          })
          if (existSelectedMerchant === undefined) {
            selectedMerchant = merchants.find((element) => {
              return element.isDefault === '1'
            })
          } else {
            selectedMerchant = state.selectedMerchant
          }
        }
        console.log('selectedMerchant', selectedMerchant)
        selectedMerchant && action.successFunction(selectedMerchant.accountId)
        return {
          ...state,
          isFetching: false,
          merchants,
          selectedMerchant: selectedMerchant || {},
        }
      case types.FETCH_MERCHANT:
        return {
          ...state,
          fetchResponse: '',
          isFetching: true,
        }

      case types.END_FETCHING:
        try{
          var json = JSON.parse(action.response)
          return {
            ...state,
            isFetching: false,
            fetchResponse: JSON.stringify(json)
          }
        }catch(e){
          return {
            ...state,
            isFetching: false,
            fetchResponse: JSON.stringify(json)
          }
        }
      case types.SET_CHOOSED_MERCHANT:
        return {
          ...state,
          selectedMerchant: action.data
        }

      case types.SET_MERCHANTS :
        let defaultMerchant
        if (action.merchantsData) {
          if (state.accountId === '') {
            defaultMerchant = action.merchantsData.find((element) => {
              return element.isDefault === '1'
            })
          } else {
            defaultMerchant = action.merchantsData.find((element) => {
              return element.accountId === state.accountId
            })
            if (defaultMerchant === undefined) {
              defaultMerchant = action.merchantsData.find((element) => {
                return element.isDefault === '1'
              })
            }
          }
        }
        return {
          ...state,
          isFetching: false,
          merchants: action.merchantsData ? action.merchantsData : [],
          accountId: defaultMerchant ? defaultMerchant.accountId : '',
          name: defaultMerchant ? defaultMerchant.name : '',
        }

      case types.RESET_CHOOSED_MERCHANT:
          return initialState

      default:
        return state
    }
}