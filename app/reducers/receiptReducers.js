import * as types from '../constants/actionTypes'
import { getRandomString } from '../lib/functions'

const initialState = {
  mode: types.RECEIPT_MODE.CUSTOMER_SCAN,
  money: '',
  orderId: null,
  [types.SERVICE_TYPE.SINGLE_ORDER_QUERY]: {},//singleOrderQuery's response => initialPaymentDataState
  [types.SERVICE_TYPE.CUSTOMER_SCAN]: {},//OLPay's response => initialPaymentDataState
  [types.SERVICE_TYPE.CUSTOMER_BE_SCANNED]: {},//Micropay's response => initialPaymentDataState
  tradeResult: {},
};

export default function receipt(state = initialState, action) {
  switch (action.type){
    case types.TOGGLE_RECEIPT_MODE:
      return { 
        ...state,
        mode: action.mode 
      }

    case types.SET_RECEIPT_MONEY:
      return { 
        ...state,
        money: action.money 
      }
    
    case types.GENERATE_ORDER_ID:
      return {
        ...state,
        orderId: action.data,
      }

    case types.REQUEST_INTELLA_API:
    case types.RECEIVE_INTELLA_API:
      return {
        ...state,
        [action.serviceType]: intellaApiData(state[action.serviceType], action)
      }

    case types.RESET_TRADE_DATA:
      return {
        ...state,
        orderId: null,
        [types.SERVICE_TYPE.SINGLE_ORDER_QUERY]: {},
        [types.SERVICE_TYPE.CUSTOMER_SCAN]: {},
        [types.SERVICE_TYPE.CUSTOMER_BE_SCANNED]: {},
      }

    case types.RESET_TRADE_MONEY:
      return {
        ...state,
        money: '',
      }

    case types.SET_TRADE_RESULT:
      return {
        ...state,
        tradeResult: action
    }

    default:
      return state
  }
}


const initialPaymentDataState = {
  isFetching: false,
  // below is server response 
  // Data: {},
  // Header: {},
}

function intellaApiData(state = initialPaymentDataState, action) {
  switch (action.type) {
    case types.REQUEST_INTELLA_API:
      return {
        ...state,
        isFetching: true,
      }
    case types.RECEIVE_INTELLA_API:
      return {
        ...state,
        isFetching: false,
        ...action.data,
      }
    default:
      return state
  }
}
