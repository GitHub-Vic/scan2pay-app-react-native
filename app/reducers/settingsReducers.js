import * as types from '../constants/actionTypes'

const initialState = {
  isShowLogoPage: true,
  sofeVersion: 'RN beta',
  udid: '',
  apnsToken: '',
  hasRegisterAPNsToken: false,
  hasUnregisterAPNsToken: false,
  merchantId: '',
  tradeKey: '',
  randomid:'',
  orderNumber: 0,
  isLogin: false,
  isStoreAccount: false,
  account: '',
  isCleanMqttSession: true,
  storeName: 'storeName',
};
  

export default function receipt(state = initialState, action) {
  switch (action.type){
    case types.RESET_SETTINGS:
      return { 
        ...state,
        isShowLogoPage: true,
      }

    case types.LOGIN:
      return { 
        ...state,
        isLogin: true,
        merchantId: action.merchantId,
        account: state.isStoreAccount ? action.merchantId : '',
        storeName: action.storeName,
        tradeKey: action.tradeKey,
        randomid:action.randomid,
        isShowLogoPage: false,
        isCleanMqttSession: false,
        hasRegisterAPNsToken: true,
        hasUnregisterAPNsToken: false,
      }

    case types.LOGOUT:
      return {
        ...state,
        isLogin: false,
        merchantId: '',
        storeName: '',
        tradeKey: '',
        randomid:'',
        isCleanMqttSession: true,
        hasRegisterAPNsToken: false,
        hasUnregisterAPNsToken: true,
      }

    case types.SET_UDID:
      return {
        ...state,
        udid: action.udid,
      }

    case types.SET_APNS_TOKEN:
      return {
        ...state,
        apnsToken: action.token,
        hasRegisterAPNsToken: true,
      }

    case types.SET_IS_CLEANMQTTSESSION:
      return {
        ...state,
        isCleanMqttSession: action.isCleanMqttSession,
      }
      
    case types.ALREADY_SHOWN_LOGOPAGE:
      return {
        ...state,
        isShowLogoPage: false,
      }

    case types.SET_IS_STORE_ACCOUNT:
      return {
        ...state,
        isStoreAccount: action.isStoreAccount,
      }

    case types.ALREADY_REGISTER_APNSTOKEN:
      return {
        ...state,
        hasRegisterAPNsToken: false,
      }

    case types.ALREADY_UNREGISTER_APNSTOKEN:
      return {
        ...state,
        hasRegisterAPNsToken: true,
        hasUnregisterAPNsToken: false,
      }
    default:
      return state
  }
}