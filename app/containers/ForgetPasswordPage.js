import React, {Component} from 'react'
import {StyleSheet, Platform, View, Text} from 'react-native'
import {windowH, windowW, commonColor, fontSizeScaler} from '../commonStyles'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import {httpsPost, mapToJsonString,forIphoneXStyleByOrientation} from '../lib/functions'
import *  as settingsActions from '../actions/settingsActions'
import {URL_INTELLA_FORGETPASSWORD} from '../config'
import Orientation from 'react-native-orientation'
import { NavigationActions } from 'react-navigation'

import InputIconPassword from  '../components/InputIconPassword'
import Toolbar from '../components/Toolbar'
import MainButton from  '../components/MainButton'
import LoadingAlert from '../components/alert/TitleWithOneBtn'
import EndAlert from '../components/alert/TitleOneBtn'


class ForgetPasswordPage extends Component {
  constructor() {
    super()
    this.state = {
      iphoneXstyle: forIphoneXStyleByOrientation(),
      account: '',
      isShowLoadingAlert: false,
      isShowEndAlert: false,
      endAlertMessage: '驗證確認信已送出'
    }
  }

  componentWillMount() {
    Orientation.lockToPortrait()
  }

  componentDidMount() {
    this.props.isStoreAccount ? this.setState({account: this.props.merchantId}) : {}
  }

  render() {
    return (
      <View style={[styles.wrapper, this.state.iphoneXstyle]}>
        <Toolbar
          title={'忘記密碼'}
          backgroundImg={'bg_toolbar_create'}
          backOnPress={() => this.props.navigation.goBack()}
          homeOnPress={() => this.goLogin()} />
        <View style={styles.topArea} />

        <InputIconPassword style={styles.input}
          textInputRef={(r)=> this.accountTextInput = r}
          password={false} 
          icon={'icon_settings_account'}
          onTextChange={(text) => this.setState({account: text})}
          inputValue={this.state.account}
          hintText={'使用者帳號'}/>

        <View style={styles.middleArea}>
          <Text style={styles.text}>{`按下送出後，請至註冊時填寫的信箱收取驗證連結`}</Text>
        </View>

        <MainButton style={styles.btn}
              btnColors={commonColor.createGradient}
              title={'送出'}
              onPress={()=> this.forgetPassword()}/>
        <View style={styles.bottomArea} />
        {this.state.isShowLoadingAlert ? <LoadingAlert title={'處理中'} /> : null}
        {this.state.isShowEndAlert ? 
          <EndAlert title={this.state.endAlertMessage} onPress={()=> this.goLogin()} btnText={'確 定'} /> 
          : null}
      </View>
    )
  }

  goLogin(navigation) {
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({ routeName: 'LoginPage'})
      ]
    })
    this.props.navigation.dispatch(resetAction)
  }

  async forgetPassword() {
    var error = ''
    this.accountTextInput.blur()
    this.setState({isShowLoadingAlert: true})
    var requestData = new Map()
    requestData.set("merchant", this.state.account)
    let msg =  await httpsPost(URL_INTELLA_FORGETPASSWORD, mapToJsonString(requestData))
    try {
      var response = JSON.parse(msg)
      this.setState({isShowLoadingAlert: false})
      if(response.status === 'ok') {
        this.setState({isShowEndAlert: true, })
        return
      }

      error = '失敗，請再嘗試一次'
    }catch(e) {
      error = '請再嘗試一次'
    }
    this.setState({isShowEndAlert: true, endAlertMessage: error})
  }

}

export default connect(
  state => ({
    isStoreAccount: state.settings.isStoreAccount,
    merchantId: state.settings.account,
  }),
  (dispatch) => ({
    actions: bindActionCreators(settingsActions, dispatch)
 })
)(ForgetPasswordPage);


const styles = StyleSheet.create({
  wrapper:{
    height: windowH,
    marginTop: Platform.OS === 'ios'? 30 : 0,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  topArea: {
    flex: 2,
    width: windowW,
  },
  input: {
    width: windowW *2/3,
  },
  middleArea: {
    flex: 2,
    width: windowW *2/3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottomArea: {
    flex: 6,
    width: windowW,
  },
  text: {
    color: commonColor.defaultTextColor,
    textAlign: 'center',
    fontSize: 15 *fontSizeScaler,
  },
  btn: {
    marginTop: 10,
  },
})
    