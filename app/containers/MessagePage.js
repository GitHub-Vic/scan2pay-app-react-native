import React, {Component} from 'react'
import {StyleSheet, Platform, View, Text, TouchableOpacity} from 'react-native'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import Orientation from 'react-native-orientation'

import * as messageActions from '../actions/messageActions'
import { goHome, forIphoneXStyleByOrientation } from '../lib/functions'
import { messagesForMerchId } from '../lib/formatFunctionsForShow'

import Toolbar from '../components/messagePage/Toolbar'
import DeteleToolbar from '../components/messagePage/DeteleToolbar'
import MessageList from '../components/messagePage/MessageList'
import DeleteDoubleCheck from '../components/messagePage/DeleteDoubleCheck'
import ActionBtn from '../components/ActionBtn'

import {windowW, commonColor} from '../commonStyles'

const totalOptions = ['全選', '前20筆', '前50筆', '前100筆']
class MessagePage extends Component {
  
  constructor() {
    super();
    this.state = {
      iphoneXstyle: forIphoneXStyleByOrientation(),
      deteleQuickChoose: '',
      isShowDeleteDoubleCheck: false,
    }
  }

  componentWillMount() {
    Orientation.lockToPortrait()
  }

  componentDidMount() {
    if (this.props.isDeleteMode) {
      this.toogleDeleteMode()
    }
  }

  render() {
    return (
        <View style={[styles.wrapper, this.state.iphoneXstyle]}>
          {this.props.isDeleteMode ? this.renderDeleteBar() : this.renderToolBar()}
          <MessageList 
            listRef={(ref)=> {this.flatList = ref}}
            data={this.props.message} 
            onPress={(orderId)=> this.onListItemPress(orderId)}
            isDeleteMode={this.props.isDeleteMode}/>
          {this.props.isDeleteMode ? this.renderDeleteBtn() : null}
          {this.state.isShowDeleteDoubleCheck ? this.renderDeleteDoubleCheck() : null}
          <ActionBtn onPress={()=> this.props.navigation.navigate('MessageDonglePage')} imgPath={'icon_action_dongle'} />
        </View>
    );
  }

  renderToolBar() {
    return (
      <Toolbar 
        backgroundImg={'bg_toolbar_create'}
        allReadOnPress={() => this.props.actions.setAllRead()}
        homeOnPress={() => goHome(this.props.navigation)}
        deteleMode={()=> this.toogleDeleteMode()}
        dongleMode={()=> this.props.navigation.navigate('MessageDonglePage')} />
    );
  }

  renderDeleteBar() {
    const options = [totalOptions[0]];
    const count = this.props.message.length;
    if(count >= 20)
      options.push(totalOptions[1])
    if(count >= 50)
      options.push(totalOptions[2])
    if(count >= 100)
      options.push(totalOptions[3])

    return (
      <DeteleToolbar 
        backgroundImg={'bg_toolbar_create'}
        cancelOnPress={()=> this.toogleDeleteMode()}
        value={this.state.deteleQuickChoose} 
        options={options}
        onChange={(value)=> this.onToggleItemClick(value)} />
    );
  }

  renderDeleteBtn() {
    return (
      <TouchableOpacity style={styles.deleteBtn}
        onPress={()=> this.setState({isShowDeleteDoubleCheck: true})}>
        <View style={styles.deleteBtn}>
          <Text style={styles.deleteText}>{`刪 除 (${this.props.deteleCount})`}</Text>
        </View>
      </TouchableOpacity>
    );
  }

  renderDeleteDoubleCheck() {
    return (
      <DeleteDoubleCheck 
        cancelOnPress={()=> this.setState({isShowDeleteDoubleCheck: false})}
        certainOnPress={()=>  this.onDeleteDoubleCheckCertain()} />
    )
  }

  onListItemPress(orderId) {
    if(this.props.isDeleteMode){
      this.props.actions.toggleIsCheck(orderId);
    }else {
      this.props.actions.setRead(orderId);
    }
  }

  onToggleItemClick(chooseValue) {
    var chooseIndex;
    totalOptions.forEach((value, index)=> {
      if(chooseValue === value) {
          chooseIndex = index
      }
    }); 

    var count;
    switch(chooseIndex) {
      case 0:
        count = this.props.message.length;
        break;
      case 1:
        count = 20;
        break;
      case 2:
        count = 50;
        break;
      case 3:
        count = 100;
        break;
    }
    if(this.state.deteleQuickChoose === chooseValue) {
      if(count) {
        this.props.actions.setIsCheckByCount(false, count)
      }
      this.setState({deteleQuickChoose: ''})
      this.scrollTo(0)
    }else {
      if(count) {
        this.props.actions.setIsCheckByCount(false, this.props.message.length)
        this.props.actions.setIsCheckByCount(true, count)
      }
      var scrollTo = this.props.message.length-count
      this.setState({deteleQuickChoose: chooseValue})
      this.scrollTo(scrollTo)
    }
      
  }

  toogleDeleteMode() {
    this.props.actions.toggleDeleteMode()
    this.setState({deteleQuickChoose: ''})
  }

  onDeleteDoubleCheckCertain() {
    this.setState({isShowDeleteDoubleCheck: false})
    this.props.actions.deleteCheckMessage()
  }

  scrollTo(index) {
    if (index > 0) {
      this.flatList.scrollToIndex({animated:false , index: index, viewPosition: 0})
    }
  }

}


function countDeleteCount(state) {
  var merchId = state.settings.merchantId
  var merchIdMess = state.message.message.filter(m => m.merchantId === merchId)
  var count = 0
  for (var m in merchIdMess) {
    if(merchIdMess[m].isCheck)
      count++
  }
  return count
}
export default connect(
  state => ({
    isDeleteMode: state.message.isDeleteMode,
    message: messagesForMerchId(state),
    deteleCount: countDeleteCount(state),
  }),
  (dispatch) => ({
    actions: bindActionCreators(messageActions, dispatch)
  })
)(MessagePage);

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    marginTop: Platform.OS === 'ios'? 30 : 0,
    backgroundColor: 'white',
  },
  deleteBtn: {
    position: 'absolute',
    bottom: 0,
    height: 50,
    width: windowW,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: commonColor.defaultRedColor,
  },
  deleteText: {
    fontSize: 20,
    color: 'white',
  },
});