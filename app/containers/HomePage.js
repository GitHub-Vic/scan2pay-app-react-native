import React, { Component } from 'react'
import { StyleSheet, Platform, View, Image,Linking,Alert,TouchableHighlight,Text} from 'react-native'
import { NavigationActions } from 'react-navigation'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import Orientation from 'react-native-orientation'
import {forIphoneXStyleByOrientation, httpsPost, mapToJsonString} from '../lib/functions'
import {URL_INTELLA_DOMAIN, URL_LOGIN, APP_ID, PLATFORM, URL_LOGOUT } from '../config'
import store from '../store'
import appActions from '../actions'
import * as settingsActions from '../actions/settingsActions'
import * as merchantActions from '../actions/merchantActions'
import * as easycardActions from '../actions/easycardActions'
import { RECEIPT_MODE } from '../constants/actionTypes'
import { windowH, windowW, toolbarHeight, fontSizeScaler } from '../commonStyles'
import MenuBtn from '../components/homePage/MenuBtn'
import MessageBadge from '../components/messagePage/Badge'
import Menu, { MenuItem } from 'react-native-material-menu'
import LoadingAlert from '../components/alert/TitleWithOneBtn'
import mqtt from '../lib/mqtt'
import apnsToken from '../lib/apnsToken'
import Dialog from "react-native-dialog"
import RNExitApp from 'react-native-exit-app'
import {APP_VERSION, URL_DOMAIN, APP_STORE_ID} from '../config'
import TradeResult from '../components/alert/TradeResult'
import CookieManager from 'react-native-cookies';

class HomePage extends Component {
  constructor() {
    super()
    this.state={
      iphoneXstyle: forIphoneXStyleByOrientation(),
      dialogVisible: false, 
      loadingCheckVersion: false,
      resultMsg: '',
      resultTitle: '',
      showResult: false,
      notLogin: false,
    }
  }

  handleUpdate () {
    Linking.openURL(`itms-apps://itunes.apple.com/tw/app/id${APP_STORE_ID}?mt=8`).catch(err => console.log('open market',err))
  }

  handleExit () {
    RNExitApp.exitApp()
  }

  mapToJsonString(map){
    var json = "{";
    for (var key in map) {
       json += `"${key}":"${map[key]}",`;
     }
     json = json.substring(0, json.length-1);
     json += "}";
     return json;
  }

  async checkVersion() {
    this.setState({ loadingCheckVersion: true })
    const response = await fetch(`${URL_DOMAIN}allpaypass/api/system/app/version`,
    {
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
      method: "POST",
      body: this.mapToJsonString({
        "type": "IOS",
        "appversion": APP_VERSION,
        "appId": APP_ID,
      })
    })
    const responseJson = await response.json()
    let jsonMessage = JSON.parse(responseJson.message)
    if (jsonMessage.updateversion === 'true') {
      this.setState({ dialogVisible: true })
    }
    this.setState({ loadingCheckVersion: false })
  }

  componentWillMount() {
    store.dispatch(appActions.receiptActions.toggleReceiptMode(RECEIPT_MODE.CUSTOMER_SCAN))
    store.dispatch(appActions.recordActions.resetRecord())
    store.dispatch(appActions.receiptActions.resetTradeData())
    store.dispatch(appActions.receiptActions.resetTradeMoney())
    Orientation.lockToPortrait()
  }

  async componentDidMount() {
    this.checkVersion()
    if(this.props.isLogin) {
      if(this.props.isShowLogoPage) {
        this.props.navigation.navigate('LogoPage')
        this.props.actions.alreadyShownLogoPage()
      }
    }else {
      this.goToLogin()
    }
    this.getUserMerchant(this.props.userId)
  }

  async login(accountId) {
    console.log('login', accountId)
    var requestData = new Map()
    requestData.set("accountId", accountId)
    requestData.set("userId", this.props.userId)
    requestData.set("tokenId", this.props.tokenId)
    requestData.set("serialId", this.props.udid)
    requestData.set("appId", APP_ID)
    requestData.set("platform", PLATFORM)
    console.log('requestData home ' ,requestData)
    let msg =  await httpsPost(URL_LOGIN, mapToJsonString(requestData))
    var response = JSON.parse(msg)
    if(response.loggedIn) {
      this.loginSuccess(response.username, response.storeName, response.tradeKey,response.randomid)
    }else {
      this.setState({status: response.message, isShowLoadingAlert: false})
    }
  }

  loginSuccess(merchId, storeName, tradeKey,randomid) {
    this.props.actions.login(merchId, storeName, tradeKey,randomid)
    store.dispatch(appActions.easycardActions.setEasycardDevice(''))
    mqtt().disConnectForLogout()
    mqtt().connectForLogin()
    Platform.OS === 'ios' ? apnsToken().register() : null
  }

  getUserMerchant(userId) {
    if (userId) {
      var requestData = new Map()
      requestData.set('userId', userId)
      this.props.merchActions.getUserMerchantByUserId(`${URL_INTELLA_DOMAIN}userMerchant/userId` ,requestData, 
        {
          successFunction: (accountId) => this.login(accountId), 
          failFunction: (result) => {
          },
          logoutFunction: (result) => {
            this.setState({
              resultMsg: result.resultMsg,
              resultTitle: '錯誤',
              showResult: true,
              notLogin: true,
            })
          }
        }
      )
    }
  }

  async logout() {
    this.setState({
      showLoading: true,
      loadingTitle: '登出中'
    })
    this.props.actions.logout()
    this.props.easycardAction.logoutEasycard()
    this.props.merchActions.resetChoosedMerchant()
    mqtt().disConnectForLogout()
    Platform.OS === 'ios' ? apnsToken().unregister() : null
    var requestData = new Map()
    requestData.set("tokenId", this.props.tokenId)
    requestData.set("userId", this.props.userId)
    requestData.set("accountId", this.props.merchantId)
    requestData.set("serialId", this.props.udid)
    requestData.set("appId", APP_ID)
    requestData.set("platform", PLATFORM)
    let msg =  await httpsPost(URL_LOGOUT, mapToJsonString(requestData))
    try {
      var response = JSON.parse(msg)
    } catch (e) {
      console.log(e)
    }
    CookieManager.clearAll()
    .then((res) => {
      const resetAction = NavigationActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({ routeName: 'LoginPage'})
        ]
      })
      this.setState({
        showLoading: false,
        loadingTitle: ''
      })
      this.props.navigation.dispatch(resetAction)
    })

  }

  goToLogin() {
    const resetAction = NavigationActions.reset({
      index: 0,
      actions: [
        NavigationActions.navigate({routeName: 'LoginPage'})
      ]
    })
    this.props.navigation.dispatch(resetAction)
  }

  _menu = null;
 
  setMenuRef = ref => {
    this._menu = ref;
  };
 
  hideMenu = (item) => {
    this.props.merchActions.setChoosedMerchant(item)
    this.getUserMerchant(item.userId)
    this._menu.hide();
  };
 
  showMenu = () => {
    this._menu.show();
  };

  render() {
    return (
      <View style={[styles.wrapper, this.state.iphoneXstyle]}>
        {this.props.ssoFlag && (this.props.isFetching || this.state.loading) ? (
          <LoadingAlert title={'加載中'} />
        ) : null}
        {
          this.props.ssoFlag ?
          <View style={styles.wrapperHeader} >
            <Text style={styles.title} numberOfLines={1}> {this.props.selectedMerchant.accountId ? `${this.props.selectedMerchant.accountId}(${this.props.selectedMerchant.name})` : ''} </Text>
            { this.props.userId !== '' ? 
              (<View style={styles.home} >
                  <Menu
                    ref={this.setMenuRef}
                    button={
                      <TouchableHighlight onPress={() => this.showMenu()} disabled={!this.props.selectedMerchant.accountId}>
                        <Image style={styles.image} source={{uri: 'icon_home_drop_down_list'}} />
                      </TouchableHighlight>
                    }
                  >
                    {this.props.merchants ? this.props.merchants.map((item, index) => <MenuItem key={index} onPress={() => {this.hideMenu(item)}}>{`${item.accountId}(${item.name})`}</MenuItem>) : null}
                  </Menu> 
              </View>)
              : 
              null
            }
          </View>
          : null
        }
        {
          (this.props.merchants.length > 0 || !this.props.ssoFlag) ? //this.props.merchants.length > 0
          <Image style={styles.ad} source={{uri: 'home_ad1'}} />
          :
          <Image style={styles.ad} source={{uri: 'home_ad1_gray'}} />
        }
        {
          this.state.dialogVisible
          ?
          <Dialog.Container visible={true}>
            <Dialog.Title>請更新版本</Dialog.Title>
            <Dialog.Button label="取消退出" onPress={this.handleExit} />
            <Dialog.Button label="前往更新" onPress={this.handleUpdate} />
          </Dialog.Container>
          :
          null
        }
        <View style={styles.menuWrapper} >
          <View style={styles.menu} >
            {(this.props.merchants.length > 0 || !this.props.ssoFlag) > 0 ? //this.props.merchants.length > 0
              <MenuBtn imgPath={'icon_home_receipt'} text={`收款`} onPress={()=> this.props.navigation.navigate('ReceiptMoneyPage')}/>
              : 
              <MenuBtn imgPath={'icon_home_receipt_gray'} text={`收款`} onPress={()=> {}}/>
            }
            {(this.props.merchants.length > 0 || !this.props.ssoFlag) > 0 ? 
              <MenuBtn imgPath={'icon_home_record'} text={`交易紀錄`} onPress={()=> this.props.navigation.navigate('RecordPage')}/>
              : 
              <MenuBtn imgPath={'icon_home_record_gray'} text={`交易紀錄`} onPress={()=> {}}/>
            }
            <MenuBtn imgPath={'icon_home_settings'} text={`設定`} onPress={()=> this.props.navigation.navigate('SettingsPage')}/>
          </View>
          <View style={styles.menu} >
            {(this.props.merchants.length > 0 || !this.props.ssoFlag) > 0 ? 
              <MessageBadge menuBtn={
                  <MenuBtn imgPath={'icon_home_message'} text={`訊息通知`} onPress={()=> this.props.navigation.navigate('MessagePage')} /> 
                }
                count={this.props.messageNotReadCount}  
              />
              :
              <MenuBtn imgPath={'icon_home_message_gray'} text={`訊息通知`} onPress={()=> {}}/> 
            }
            {(this.props.merchants.length > 0 || !this.props.ssoFlag) > 0 ? 
              <MenuBtn imgPath={'icon_easycard'} text={'悠遊卡'} onPress={()=> this.props.navigation.navigate('EZCardPage')}/>
              :
              <MenuBtn imgPath={'icon_easycard_gray'} text={'悠遊卡'} onPress={()=> {}}/>
            }
            <MenuBtn imgPath={'icon_home_book'} text={'使用手冊'} onPress={()=>{
              Alert.alert(
                    'Scan2Pay 使用手冊','將以瀏覽器開啟文件',
                    [
                      {text:'取消',onPress:()=>{}},
                      {text:'前往',onPress:()=>{Linking.openURL('https://intella.co/pdf/Scan2Pay-App.pdf').catch(err => console.log('video',err))}}  
                    ]
                  )
                }}             
              />
          </View>
          <View style={styles.menu} >
          </View>
        </View>
        {
          this.state.showResult ?
          <TradeResult
            imgPath={'icon_receipt_result_fail'}
            title={this.state.resultTitle}
            msg={
              this.state.resultMsg
            }
            onPress={() => {
              this.setState({ showResult: false })
              if (this.state.notLogin) {
                this.logout()
                this.goToLogin() 
              }
            }}
            style={{zIndex: 100}}
          />
          : null
        }
        {this.state.loadingCheckVersion ? <LoadingAlert title={'確認版本資訊中'} /> : null}
      </View>
    );
  }
  
}

function countNotReadMessage(state) {
  var merchId = state.settings.merchantId
  var merchIdMess = state.message.message.filter(m => m.merchantId === merchId)
  var count = merchIdMess.filter(m=> !m.isRead).length
  return count
}

export default connect( 
  state => ({
    isLogin: state.settings.isLogin,
    isShowLogoPage: state.settings.isShowLogoPage,
    messageNotReadCount: countNotReadMessage(state),
    merchants: state.merchant.merchants,
    selectedMerchant: state.merchant.selectedMerchant,
    isFetching: state.merchant.isFetching,
    userId: state.merchant.userId,
    udid: state.settings.udid,
    tokenId: state.merchant.tokenId,
    ssoFlag: state.merchant.ssoFlag,
    merchantId: state.settings.merchantId,
  }),
  (dispatch) => ({
    actions: bindActionCreators(settingsActions, dispatch),
    merchActions: bindActionCreators(merchantActions, dispatch),
    easycardAction:bindActionCreators(easycardActions, dispatch),
  })
)(HomePage);

const styles = StyleSheet.create({
  wrapper:{
    height: windowH,
    marginTop: Platform.OS === 'ios'? 30 : 0,
    backgroundColor: 'white',
  },
  ad: {
    flex: 7,
    width: windowW,
    resizeMode: 'stretch'
  },
  menuWrapper: {
    flex: 13,
  },
  menu: {
    flex: 1,
    flexDirection: 'row',
    paddingTop: 10,
  },
  image: {
    resizeMode: 'contain',
    width: 30,
    height: 30,
  },
  wrapperHeader: {
    width: windowW,
    height: toolbarHeight,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  title: {
    fontSize: 23 *fontSizeScaler,
    color: 'black',
    backgroundColor: 'transparent',
    maxWidth: 300
  },
  home: {
    position: 'absolute',
    right: 10,
  }
});
  