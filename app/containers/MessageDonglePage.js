import React, {Component} from 'react'
import {StyleSheet, Platform, View, Text, FlatList, Image, TouchableHighlight} from 'react-native'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import Orientation from 'react-native-orientation'

import * as formatFunctions from '../lib/formatFunctionsForShow'
import {getValueByKeyFromJsonObject, forIphoneXStyleByOrientation, LANDSCAPE, PORTRAIT } from '../lib/functions'
import * as messageActions from '../actions/messageActions'
import MessageList from '../components/messagePage/MessageList'
import {windowW, windowH, commonColor, fontSizeScaler} from '../commonStyles'
import DoubleCheckAlert from '../components/alert/TitleWithTwoBtnLandscape'

class MessageDonglePage extends Component {
  
  constructor() {
    super();
    this.state = {
      iphoneXstyle: forIphoneXStyleByOrientation(LANDSCAPE),
      isShowAnimation: false,
      isShowDoubleCheckAlert: false,
    }
  }

  componentWillMount() {
    Orientation.lockToLandscape()
  }

  shouldComponentUpdate(nextProps, nextState) {
    if(nextState !== this.state)
      return true
      
    if(nextProps.message === this.props.message) {
      return false;
    }
    return true;
  }

  componentWillUnmount() {
    this.setState({iphoneXstyle: forIphoneXStyleByOrientation(PORTRAIT)})
    Orientation.lockToPortrait()
  }

  render() {
    return (
      <View style={[styles.wrapper, this.state.iphoneXstyle]}>
        <FlatList
          ref={(ref)=> {this.flatList = ref}}
          data={this.props.message}
          keyExtractor={(item, index) => index}
          renderItem={({item, index}) => this.renderItem({...item})}
          ItemSeparatorComponent={() => this.separator()} />
        <TouchableHighlight style={styles.back} 
          onPress={()=> this.setState({isShowDoubleCheckAlert: true})}
          underlayColor={'transparent'}  >
          <Image style={styles.backImg} source={{uri: 'icon_dongle_back'}}/>
        </TouchableHighlight>
        {this.state.isShowDoubleCheckAlert ? this.renderDoublceCheckAlert() : null}
      </View>  
    );
  }

  renderItem(props) {
    const item = props;
    return(
      <View style={styles.itemWrapper}>
        <View style={styles.indexContent}>
          <Text style={styles.valueText}>{item.index}</Text>
        </View>

        <View style={styles.content}>
          <View style={styles.row}>
            <Image style={styles.timeImg} source={{uri: 'icon_message_item_time'}} />
            <Text style={styles.timeText}>{formatFunctions.formatTime(item.date)}</Text>
          </View>
          {this.renderRow(`商品名稱: `, item.description)}
          {this.renderRow(`交易單號: `, formatFunctions.formatSystemOrderId(getValueByKeyFromJsonObject(item, 'systemOrderId')))}
          {this.renderRow(`訂單編號: `, formatFunctions.formatSystemOrderId(item.orderId))}
          {this.renderRow(`付款類別: `, item.zhifuType)}
        </View>

        <View style={styles.moneyContent}>
          <Text style={styles.money}>{`NT$ ${formatFunctions.formatMoneyToCama(item.money)}`}</Text>
        </View>
      </View>
    );
  }
  
  separator() {
    return <View style={styles.separator} />
  }

  renderRow(key, value) {
    return (
      <View style={styles.row}>
        <Text style={styles.keyText}>{key}</Text>
        <Text style={styles.valueText}>{value}</Text>
      </View>
    );
  }

  renderDoublceCheckAlert() {
    return (
      <DoubleCheckAlert 
        title='是否退出Dongle模式？'
        cancelOnPress={()=> this.setState({isShowDoubleCheckAlert: false})} 
        certainOnPress={()=> this.props.navigation.goBack()}
        defaultBtn={'cancel'}/>
    );
  }

}

export default connect(
  state => ({
    message: formatFunctions.messagesForMerchId(state),
  }),
  (dispatch) => ({
    actions: bindActionCreators(messageActions, dispatch)
  })
)(MessageDonglePage);

var totlaW = windowH - 20;
const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: 'white',
  },
  separator: {
    height: 1,
    backgroundColor: commonColor.listViewDividerColor,
  },
  itemWrapper: {
    flex: 1,
    flexDirection: 'row',
    padding: 10,
  },
  row: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  timeImg: {
    height: 20 * fontSizeScaler,
    width: 20 * fontSizeScaler,
  },
  timeText: {
    color: commonColor.timeTextColor,
    fontSize: 20 * fontSizeScaler,
  },
  indexContent: {
    width: totlaW * 1/10,
    justifyContent: 'center',
  },
  index: {
    width: totlaW * 1/10,
    color: commonColor.defaultTextColor,
    fontSize: 17 * fontSizeScaler,
    textAlign: 'center',
  },
  content: {
    width: totlaW * 6/10,
    paddingLeft: 5,
    justifyContent: 'center'
  },
  moneyContent: {
    flexDirection: 'row',
    width: totlaW * 3/10,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  money: {
    color:commonColor.customerScan,
    marginRight: 5,
    fontSize: 23 * fontSizeScaler,
  },
  keyText: {
    color: commonColor.defaultTextColor,
    fontSize: 23 * fontSizeScaler,
  },
  valueText: {
    width: totlaW * 5.5/10 * 2/3 - 5,
    color: commonColor.defaultTextColor,
    fontSize: 23 * fontSizeScaler,
  },
  back: {
    position: 'absolute',
    top: windowH/25,
    right: windowH/25,
    height: windowW/14,
    width: windowW/14,
  },
  backImg: {
    resizeMode: 'contain',
    height: windowW/10,
    width: windowW/10,
  }
});