import React, {Component} from 'react'
import { StyleSheet, Platform, View, Text, Image, FlatList, TouchableHighlight,Alert } from 'react-native'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as easycardActions from '../actions/easycardActions'
import * as easycardStrings from '../constants/easycardString'
import { goHome, forIphoneXStyleByOrientation, _formatDate,getNowTime, consoleForDebug, getRandomString,timeOutMinute,getDateToMillsecode } from '../lib/functions'
import { formatMoneyToCama } from '../lib/formatFunctionsForShow'
import { METHOD, SERVICE_TYPE, TRADE_RESULT_TYPE } from '../constants/actionTypes'
import { parserErrorCode } from '../constants/easycardErrorCode'
import { SHA256 } from '../lib/SHA256'

import Toolbar from '../components/Toolbar'
import { commonColor,windowH, windowW, fontSizeScaler } from '../commonStyles'
import LoadingAlert from '../components/alert/TitleWithOneBtn'
import InputMoneyAlert from '../components/ezcardPage/InputMoneyAlert'
import NormalTradeResultAlert from '../components/ezcardPage/NormalTradeResultAlert'
import CardNumberAlert from '../components/ezcardPage/JustOneViewAlert'
import RefundPasswordAlert from '../components/ezcardPage/RefundPasswordAlert'
import ListAlert from '../components/ezcardPage/ListAlert'
import CancelQueryAlert from '../components/ezcardPage/CancelQueryAlert';


const funs = [{name: '購貨',imgPath: 'icon_easycard_deduction'},
              {name: '金額退款',imgPath: 'icon_easycard_refund'} , 
              {name: '卡片資料查詢',imgPath: 'icon_easycard_addvalue_query'}, 
              {name: '結帳',imgPath: 'icon_easycard_checkout'}, 
            //   {name: '卡號查詢',imgPath: 'icon_easycard_cardid'}, 
            //   {name: '扣款退貨',imgPath: 'icon_easycard_moneyrefund'}, 
            //   {name: '現金加值',imgPath: 'icon_easycard_addvalue'},
            //   {name: '取消交易',imgPath: 'icon_easycard_cancel'} , 
            //   {name: '加值查詢',imgPath: 'icon_easycard_addvalue_query'}, 
            //   {name: '卡片交易明細查詢',imgPath: 'icon_easycard_transaction'}
             ]
const funsBgColor = ['#D0368D', '#1B82B7', '#DB75AB', '#61AEBF', '#85BC20','#5ca67d', '#14A289','#D37A31', '#EDAB47', '#E27549']
class EZCardPage extends Component {
    
    constructor() {
        super()
        this.state = {
            iphoneXstyle: forIphoneXStyleByOrientation(),
            isSignOn: false,
            loadingAlertTitle: '',
            serviceType:'',
            balance: '',
            inputMoneyText: '',
            alertOnPress: ()=>{},
            isShowInputMoneyAlert: false,
            isShowNormalTradeResultAlert: false,
            isShowErrorMessageAlert:false,
            isShowRefoundListAlert: false,
            isShowCardNumberAlert: false,
            isShowRefundPasswordAlert: false,
            isShowCancelQueryAlert: false, // 顯示取消交易畫面
            isShowDetailQueryAlert: false, // 加值查詢,交易明細查詢
            tradeResultTitle: '',
            tradeResultStatus: '',
            tradeResultMsg: null,
            refundPassword: '', // 退款密碼
            refundTitle: '', 
            recordRefundMoney:'', // 金額退款
            recordRefundOrderNo:'',// 金額退款
            recordDatas:[], // 查詢記錄資料
            cancelActionType:'', // 取消交易
            cancelOrderId:'', //取消交易
            cancelRefundStatus: ''//取消交易資料狀態

         
        }
    }

    componentDidMount() {
        if(this.props.easycardDevice){
            this.setState({loadingAlertTitle: easycardStrings.SIGNON})
            this.signOn()  // 登入
        }else{
            this.fetchDevices()
        }
    }

    componentWillReceiveProps (nextProps) {
        // api 打完後會更新 fetchResponse 如果有則呼 parserResponse
        if(nextProps.fetchResponse !== undefined)
            if(nextProps.fetchResponse !== this.props.fetchResponse){
                this.parserResponse(nextProps.fetchResponse)
               
            }  
        if(nextProps.easycardDevice !== undefined)
            if(nextProps.easycardDevice !== this.props.easycardDevice){
                this.setState({loadingAlertTitle: easycardStrings.SIGNON})
                this.signOn(nextProps.easycardDevice)
            }
               
    }
    
    renderToolBar() {
        return (
          <Toolbar 
            title = {'悠遊卡'}
            backgroundImg={'bg_toolbar_create'}
            backOnPress={() => goHome(this.props.navigation)}
            homeOnPress={() => goHome(this.props.navigation)}
            actionImgPath={'icon_easycard_setting'}
            actionOnPress={()=> this.props.navigation.navigate('EZCardSetting')} />
        )
    }

    renderTopView() {
        return (
            <View style={styles.topViewWrapper}>
                <View style={styles.eziconWrapper}>
                    <Image style={styles.ezicon} source={{uri: 'icon_easycard_img'}} />
                </View>
                <Text style={styles.topDivLine} />
                <View style={styles.topTextWrapper}>
                    <Text style={styles.topTitleText}>餘額</Text>
                    <Text style={styles.topMsgText}>{this.state.balance ? `NT$ ${this.state.balance}` : "尚未感應票卡"}</Text>
                </View>
            </View>
        )
    }

    renderBottomView() {
        nowDivceTextColor = this.state.isSignOn ? commonColor.customerScan : 'red'
        return (
            <View style={styles.bottomViewWrapper}>
                <View style={styles.bottomTextWrapper}>
                    <Text style={styles.chooseText}>請選擇功能</Text>
                    <Text style={[styles.nowDevice, {color: nowDivceTextColor}]}>{`使用機台:${this.props.easycardDevice}`}</Text>
                </View>
                <View style={styles.bottomBtnWrapper}>
                    <FlatList style={{flex:1, paddingTop: 5}}
                        numColumns={2}
                        data={funs}
                        keyExtractor={(item, index) => index}
                        renderItem={({item, index}) => this.renderFunListItem(item, index)} />
                </View>
            </View>
        )
    }

    renderFunListItem(item, index) {
        buttonBgColor = this.state.isSignOn ? funsBgColor[index] : '#999'
        return (
            <TouchableHighlight style={{flex:1}}
                disabled ={!this.state.isSignOn}
                onPress={() => this.funsOnPress(index)} 
                underlayColor = 'transparent' >
                <View style={[styles.itemWrapper, {backgroundColor: buttonBgColor}]}>
                    <Image style={styles.itemImage} source={{uri: item.imgPath}}/>
                    <Text style={styles.itemText} >{item.name}</Text>
                </View>
            </TouchableHighlight>
        )
    }

    renderLoadingAlert() {
       return  <LoadingAlert title={`${this.state.loadingAlertTitle}中`} />
    }

    renderInputMoneyAlert() {
        return (
            <InputMoneyAlert 
                onImgPress = {()=> this.closeInputMoneyAlert()}
                onTextChange = {(text)=>this.setState({inputMoneyText: text})}
                inputValue = {this.state.inputMoneyText}
                onPressCertenBtn = {()=> this.state.alertOnPress()}// this.deduction(this.state.inputMoneyText)
                title = {this.state.loadingAlertTitle}
            />
        )
    }

    closeInputMoneyAlert() {
        this.setState({
            isShowInputMoneyAlert: false,
            inputMoneyText: '',
            alertOnPress: ()=>{},
        })
    }

    renderNormalTradeResultAlert() {
        var normalAlertImg
        if(this.state.tradeResultStatus === TRADE_RESULT_TYPE.SUCCESS){
            normalAlertImg = 'icon_receipt_result_success';
        }else if(this.state.tradeResultStatus === TRADE_RESULT_TYPE.ALERT){
            normalAlertImg = 'icon_receipt_result_alert';
        }else {
            normalAlertImg = 'icon_receipt_result_fail';
        }
        return (
            <NormalTradeResultAlert 
                onPress = {()=> this.state.alertOnPress()}
                imgPath = {normalAlertImg}
                title = {this.state.tradeResultTitle}
                map = {this.state.tradeResultMsg}
                containerStyle={styles.alertContainerStyle}
                contentStyle={styles.alertContentStyle}
                rowValueStyle = {styles.rowValueStyle}
                rowStyle = {{flex:7 }}
                bottomStyle = {styles.bottomStyle}
            />
        )
    }

    closeNormalTradeResultAlert() {
        this.setState({
            alertOnPress: ()=>{},
            isShowNormalTradeResultAlert: false,
            tradeResultTitle: '',
            tradeResultStatus: '',
            tradeResultMsg: null,
        })
    }

    renderCradNumberAlert() {
        return (
            <CardNumberAlert 
                imgPath={'icon_easycard_reserve_item'} 
                text={this.state.cardNumber}
                onPress={()=>this.state.alertOnPress()} />
        )
    }
    // render 退款密碼 alert
    renderRefundPasswordAlert() {
        return (
            <RefundPasswordAlert
                onImgPress = {()=> this.closeRefundPasswordAlert()}
                onTextChange = {(text)=>this.setState({refundPassword: text})}
                inputValue = {this.state.refundPassword}
                onPressCertenBtn = {()=> this.state.alertOnPress()}
                title = {this.state.refundTitle}
            />
        )
    }
    // 顯示 退款密碼 alert
    showRefundPasswordAlert(alertOnPress) {
        this.setState({
            isShowInputMoneyAlert: false,
            isShowRefundPasswordAlert: true,
            refundPassword: '',
            alertOnPress: ()=> alertOnPress(this.state.refundPassword, this.state.inputMoneyText),
            refundTitle: `密碼`,
        })
    }
    // 關閉 退款密碼 alert
    closeRefundPasswordAlert() {
        this.setState({
            isShowRefundPasswordAlert: false,
            refundPassword: '',
            alertOnPress: ()=> {},
            refundTitle: '',
        })
    }
    // 金額退款
    renderRecordForRefund(){
        return(
            <ListAlert 
                renderTitle = {()=>this.renderRecordForRefundTitle()}
                isSelected = {this.state.recordRefundMoney ? true : false}
                onCancelPress = {()=>{
                    this.setState({
                        isShowRefoundListAlert:false , 
                        recordRefundMoney: "",
                        recordRefundOrderNo:"",
                        refundTitle:"",
                        serviceType:""
                    })
                }}
                onClickRecoedItem ={(recordRefundMoney,recordRefundOrderNo)=> this.state.alertOnPress(this.setState({
                    recordRefundMoney : recordRefundMoney,
                    recordRefundOrderNo: recordRefundOrderNo
                }))}
                serviceType = {this.state.serviceType}
                recordDatas = {this.state.recordDatas}
                onPressCertenBtn = {()=>{
                    this.showRecordForRefund((refundMoney , refundOrderNo ,password)=> {
                        this.deductionRefund(refundMoney,refundOrderNo,password)
                    })
                }}
            /> 
        )
    }
    // 金額退款標題
    renderRecordForRefundTitle(){
        return(
            <View >            
                <Text style={styles.recordRefundTitle}>請選擇<Text style={{color:"red"}}>金額退款</Text>資料</Text>
                <Text style={[styles.recordRefundTitle,{color:"red"}]}>每次限一筆交易</Text>        
            </View>
           
        )
    }
    // 顯示金額退款密碼
    showRecordForRefund(alertOnPress){
        this.setState({
            isShowRefoundListAlert:false,
            isShowRefundPasswordAlert: true,
            serviceType:'',
            refundPassword:'',
            refundTitle: `密碼`,
            alertOnPress: ()=> alertOnPress(this.state.recordRefundMoney ,this.state.recordRefundOrderNo,this.state.refundPassword),
        })
    }

    renderCancelQueryAlert(){
        return (
            <CancelQueryAlert
                recordDatas = {this.state.recordDatas}
                onPressCertenBtn ={()=>{
                    this.setState({
                        isShowCancelQueryAlert:false,
                    })
                    if(this.state.cancelRefundStatus !== 'Y'){
                        this.showRefundPasswordAlert((password, money)=> { this.cancelOrder(password)})
                    }else{
                        Alert.alert("此筆交易已經退款")
                        return
                    } 
                }}
                onPress={()=>{this.setState({isShowCancelQueryAlert:false})}}
            />
        )
    }

     // 加值查詢, 交易明細查詢
     renderDetailQueryAlert(){
        return(
            <ListAlert 
                renderTitle = {()=>this.renderDetailQueryTitle()}
                onCancelPress = {()=>{
                    this.setState({
                        isShowDetailQueryAlert:false , 
                        serviceType:'',
                    })
                }}
                serviceType = {this.state.serviceType}
                recordDatas = {this.state.recordDatas}
            /> 
        )
        
    }

    renderDetailQueryTitle(){
        return(
            <View>            
                <Text style={styles.recordRefundTitle}>{this.state.loadingAlertTitle}</Text>
                <Text style={[styles.recordRefundTitle]}>{`機台名稱：${this.props.easycardDevice}`}</Text>        
            </View>
           
        )
    }

    // render 錯誤訊息 alert 畫面
    rederErrorAlert(){
        var normalAlertImg
        if(this.state.tradeResultStatus === TRADE_RESULT_TYPE.ALERT){
            normalAlertImg = 'icon_receipt_result_alert';
        }else {
            normalAlertImg = 'icon_receipt_result_fail';
        }
        return (
            <NormalTradeResultAlert 
                onPress = {()=> this.state.alertOnPress()}
                imgPath = {normalAlertImg}
                title = {this.state.tradeResultTitle}
                containerStyle={styles.alertContainerStyle}
                contentStyle={[styles.alertContentStyle , {marginTop:-40 }]}
                rowStyle = {styles.rowStyle}
                rowValueStyle ={{flex:0}}
                map = {this.state.tradeResultMsg}
            />
        )
    }
    // close 錯誤訊息 alert 畫面
    closeRederErrorAlert() {
        this.setState({
            alertOnPress: ()=>{},
            isShowErrorMessageAlert: false,
            tradeResultTitle: '',
            tradeResultStatus: '',
            tradeResultMsg: null,
        })
    }

    render() {
        let isSignOn = this.props.serviceType === SERVICE_TYPE.EASYCARD_SIGNON
        let isDeviceQuery = this.props.serviceType === SERVICE_TYPE.EASYCARD_DEVICE_QUERY
        let isShowLoadingAlert = this.props.isFetching ? (isSignOn ? true : (isDeviceQuery ? false : true)) : false
      return(
        <View style={[styles.wrapper, this.state.iphoneXstyle]}>
            {this.renderToolBar()}
            {this.renderTopView()}
            <Text style={styles.dividingLine} />
            {this.renderBottomView()}
            {isShowLoadingAlert ? this.renderLoadingAlert() : null}
            {this.state.isShowInputMoneyAlert ? this.renderInputMoneyAlert() : null}
            {this.state.isShowNormalTradeResultAlert ? this.renderNormalTradeResultAlert() : null}
            {this.state.isShowCardNumberAlert ? this.renderCradNumberAlert() : null}
            {this.state.isShowRefundPasswordAlert? this.renderRefundPasswordAlert() : null} 
            {this.state.isShowErrorMessageAlert? this.rederErrorAlert() : null} 
            {this.state.isShowRefoundListAlert? this.renderRecordForRefund(): null} 
            {this.state.isShowCancelQueryAlert? this.renderCancelQueryAlert(): null}
            {this.state.isShowDetailQueryAlert? this.renderDetailQueryAlert(): null}
        </View>
      )
    }

// 功能選單按鈕被按下時 對應各個功能 api function
    funsOnPress(index) {
        if(index === 0) {
            this.setState({
                loadingAlertTitle: easycardStrings.DEDUCTION, 
                alertOnPress: () => {  
                    this.deduction(this.state.inputMoneyText)
                 },
                isShowInputMoneyAlert: true,
            })
        }else if(index === 1) {
            this.OrderQuery()
            this.setState({
                loadingAlertTitle: easycardStrings.SEARCH_ORDER,
            })
        }else if(index === 2) {
            this.setState({loadingAlertTitle: easycardStrings.CARD_QUERY})
            this.balanceQuery()
        }else if(index === 3) {
            this.setState({loadingAlertTitle: easycardStrings.CHECKOUT})
            this.checkOut()
        }else if(index === 4) {
            this.setState({loadingAlertTitle: easycardStrings.EZCARD_NUMBER_QUERY})
            this.cardNumberQuery()
        }else if(index === 5) {
            this.setState({
                loadingAlertTitle: easycardStrings.MONEY_REFUND,
                alertOnPress: () => this.showRefundPasswordAlert((password, money)=> {
                    this.ezcRefund(password, money)
                }),
                isShowInputMoneyAlert: true,
            })
        }else if(index === 6) {
            this.setState({
                loadingAlertTitle: easycardStrings.ADD_VALUE,
                alertOnPress: () => this.addValue(this.state.inputMoneyText),
                isShowInputMoneyAlert: true,
            })
        }else if(index === 7) {
            this.cancelOrderQuery()
            this.setState({
                loadingAlertTitle: easycardStrings.GET_DATA,
            })
        }
        else if(index === 8) {
            this.reserveOrderQuery()
            this.setState({
                loadingAlertTitle: easycardStrings.ADDVALUEQUERY,
            })
        }
        else if(index === 9) {
            this.transactionDetailQuery()
            this.setState({
                loadingAlertTitle: easycardStrings.EZCARDTRANSACTIONDETAULSQUERY,
            })
        }
    }

// 各種行為 api
    fetchDevices() {
        var requestData = new Map()
        requestData.set("Method", METHOD.EASYCARD)
        requestData.set("ServiceType", SERVICE_TYPE.EASYCARD_DEVICE_QUERY)
        requestData.set("MchId", this.props.merchId)
        requestData.set("CreateTime", getNowTime())
        requestData.set("DeviceId", '00000000')
        requestData.set("TradeKey", this.props.tradeKey)
        requestData.set("Retry", "0")
        this.props.actions.fetchIntellaApi(requestData); 
        this.timer = timeOutMinute(60,this.props.actions);  
    }

    // 登入
    signOn(easycardDevice) {
        this.setState({isSignOn: false})
        var requestData = new Map()
        requestData.set("Method", METHOD.EASYCARD)
        requestData.set("ServiceType", SERVICE_TYPE.EASYCARD_SIGNON)
        requestData.set("MchId", this.props.merchId)
        requestData.set("CreateTime", getNowTime())
        if(undefined === easycardDevice)
            requestData.set("DeviceId", this.props.easycardDevice)
        else
            requestData.set("DeviceId", easycardDevice)
        requestData.set("TradeKey", this.props.tradeKey)
        this.props.actions.fetchIntellaApi(requestData);
        this.timer = timeOutMinute(60,this.props.actions);
    }
    // 卡片扣款
    deduction(money) {
        this.closeInputMoneyAlert()
        var requestData = new Map()
        requestData.set("Method", METHOD.EASYCARD)
        requestData.set("ServiceType", SERVICE_TYPE.EASYCARD_PAYMENT)
        requestData.set("MchId", this.props.merchId)
        requestData.set("CreateTime", getNowTime())
        requestData.set("Amount", money)
        requestData.set("DeviceId", this.props.easycardDevice)
        requestData.set("StoreOrderNo", this.props.randomid+getDateToMillsecode())
        requestData.set("Retry", "0")
        requestData.set("TradeKey", this.props.tradeKey)
        this.props.actions.fetchIntellaApi(requestData)
        this.timer = timeOutMinute(60,this.props.actions);
    }
    // 搜尋查詢
    OrderQuery() {
        var requestData = new Map()
        requestData.set("Method", METHOD.EASYCARD)
        requestData.set("ServiceType", SERVICE_TYPE.EASYCARD_ORDERQUERY)
        requestData.set("MchId", this.props.merchId)
        requestData.set("CreateTime", getNowTime())
        requestData.set("DeviceId", this.props.easycardDevice)
        requestData.set("TradeKey", this.props.tradeKey)
        this.props.actions.fetchIntellaApi(requestData)
        this.timer = timeOutMinute(60,this.props.actions);
    }
    // 餘額查詢
   balanceQuery() {
       
        var requestData = new Map()
        requestData.set("Method", METHOD.EASYCARD)
        requestData.set("ServiceType", SERVICE_TYPE.EASYCARD_BALANCE_QUERY)
        requestData.set("MchId", this.props.merchId)
        requestData.set("CreateTime", getNowTime())
        requestData.set("DeviceId", this.props.easycardDevice)
        requestData.set("TradeKey", this.props.tradeKey)
        requestData.set("Retry", "0")
        this.props.actions.fetchIntellaApi(requestData)
        this.timer = timeOutMinute(60,this.props.actions);
    }
    // 結帳
    checkOut() {
        var requestData = new Map()
        requestData.set("Method", METHOD.EASYCARD)
        requestData.set("ServiceType", SERVICE_TYPE.EASYCARD_SETTLEMENT)
        requestData.set("MchId", this.props.merchId)
        requestData.set("CreateTime", getNowTime())
        requestData.set("DeviceId", this.props.easycardDevice)
        requestData.set("TradeKey", this.props.tradeKey)
        requestData.set("Retry", "0")
        this.props.actions.fetchIntellaApi(requestData)
        this.timer = timeOutMinute(60,this.props.actions);
    }
    // 卡片退款
    deductionRefund(refundMoney,refundOrderNo,password){
        let refundPassword = SHA256(password)
        let amount = refundMoney
        this.closeRefundPasswordAlert()
        this.setState({
            loadingAlertTitle: easycardStrings.REFUND,
        })
        var requestData = new Map()
        requestData.set("Method", METHOD.EASYCARD)
        requestData.set("ServiceType", SERVICE_TYPE.EASYCARD_REFUND)
        requestData.set("MchId", this.props.merchId)
        requestData.set("CreateTime", getNowTime())
        requestData.set("DeviceId", this.props.easycardDevice)
        requestData.set("Amount", amount)
        requestData.set("Retry", '0')
        requestData.set("RefundKey", refundPassword)
        requestData.set("StoreOrderNo",refundOrderNo)
        requestData.set("TradeKey", this.props.tradeKey)
        this.props.actions.fetchIntellaApi(requestData)
        this.timer = timeOutMinute(60,this.props.actions);
    }
    // 金額退款
    ezcRefund(password, money) {
        var refundPassword = SHA256(password)
        var amount = money
        this.closeInputMoneyAlert()
        this.closeRefundPasswordAlert()
        this.setState({
            loadingAlertTitle: easycardStrings.MONEY_REFUND,
        })
        var requestData = new Map()
        requestData.set("Method", METHOD.EASYCARD)
        requestData.set("ServiceType", SERVICE_TYPE.EASYCARD_EZCREFUND)
        requestData.set("MchId", this.props.merchId)
        requestData.set("CreateTime", getNowTime())
        requestData.set("DeviceId", this.props.easycardDevice)
        requestData.set("Amount", amount)
        requestData.set("Retry", '0')
        requestData.set("RefundKey", refundPassword)
        requestData.set("TradeKey", this.props.tradeKey)
        this.props.actions.fetchIntellaApi(requestData)
        this.timer = timeOutMinute(60,this.props.actions);
    }
    //加值
    addValue(money) {
        this.closeInputMoneyAlert()
        var requestData = new Map()
        requestData.set("Method", METHOD.EASYCARD)
        requestData.set("ServiceType", SERVICE_TYPE.EASYCARD_ADD_VALUE)
        requestData.set("MchId", this.props.merchId)
        requestData.set("CreateTime", getNowTime())
        requestData.set("Amount", money)
        requestData.set("DeviceId", this.props.easycardDevice)
        requestData.set("StoreOrderNo", this.props.randomid+getDateToMillsecode())
        requestData.set("Retry", "0")
        requestData.set("TradeKey", this.props.tradeKey)
        this.props.actions.fetchIntellaApi(requestData)
        this.timer = timeOutMinute(60,this.props.actions);
    }
    // 卡號查詢
    cardNumberQuery() {
        var requestData = new Map()
        requestData.set("Method", METHOD.EASYCARD)
        requestData.set("ServiceType", SERVICE_TYPE.EASYCARD_ID_QUERY)
        requestData.set("MchId", this.props.merchId)
        requestData.set("CreateTime", getNowTime())
        requestData.set("DeviceId", this.props.easycardDevice)
        requestData.set("TradeKey", this.props.tradeKey)
        requestData.set("Retry", "0")
        this.props.actions.fetchIntellaApi(requestData)
        this.timer = timeOutMinute(60,this.props.actions);
    }
    // 查詢取消交易資料
    cancelOrderQuery(){
        var requestData = new Map()
        requestData.set("Method", METHOD.EASYCARD)
        requestData.set("ServiceType", SERVICE_TYPE.EASYCARD_CANCEL_ORDER_QUERY)
        requestData.set("MchId", this.props.merchId)
        requestData.set("CreateTime", getNowTime())
        requestData.set("DeviceId", this.props.easycardDevice)
        requestData.set("TradeKey", this.props.tradeKey)
        this.props.actions.fetchIntellaApi(requestData)
        this.timer = timeOutMinute(60,this.props.actions);
    }
    // 取消交易
    cancelOrder(password){
        var refundPassword = SHA256(password)
        this.closeRefundPasswordAlert()
        this.setState({
            loadingAlertTitle: easycardStrings.CANCELORDER,
        })
        var requestData = new Map()
        requestData.set("Method", METHOD.EASYCARD)
        requestData.set("ServiceType", SERVICE_TYPE.EASYCARD_CANCEL)
        requestData.set("MchId", this.props.merchId)
        requestData.set("CreateTime", getNowTime())
        requestData.set("DeviceId", this.props.easycardDevice)
        requestData.set("Retry", '0')
        requestData.set("RefundKey", refundPassword)
        requestData.set("ActionType",this.state.cancelActionType)
        requestData.set("StoreOrderNo",this.state.cancelOrderId)
        requestData.set("TradeKey", this.props.tradeKey)
        this.props.actions.fetchIntellaApi(requestData)
        this.timer = timeOutMinute(60,this.props.actions);
    }
    // 加值查詢
    reserveOrderQuery() {
        var requestData = new Map()
        requestData.set("Method",  METHOD.EASYCARD);
        requestData.set("ServiceType", SERVICE_TYPE.EASYCARD_RESERVEORDER_QUERY);
        requestData.set("MchId", this.props.merchId);
        requestData.set("CreateTime", getNowTime());
        requestData.set("DeviceId", this.props.easycardDevice);
        requestData.set("TradeKey", this.props.tradeKey);
        this.props.actions.fetchIntellaApi(requestData)
        this.timer = timeOutMinute(60,this.props.actions);
    }

    // 卡片交易明細查詢
    transactionDetailQuery() {
        var requestData = new Map()
        requestData.set("Method", METHOD.EASYCARD);
        requestData.set("ServiceType", SERVICE_TYPE.EASYCARD_TRANSACTION_DETAIL_QUERY);
        requestData.set("MchId", this.props.merchId);
        requestData.set("CreateTime", getNowTime());
        requestData.set("DeviceId", this.props.easycardDevice);
        requestData.set("TradeKey", this.props.tradeKey);
        this.props.actions.fetchIntellaApi(requestData)
        this.timer = timeOutMinute(60,this.props.actions);
    }
    // 當ErrorCode為000125,則依照serviceType 重打一次交易
    retry(json){
        let header     = json.Header
        let data       = json.Data
        let serviceType = header.ServiceType
        let mchId      = header.MchId
        let retry      = data.Retry
        let deviceId   = data.request.DeviceID
        let amount     = data.request.Amount
        let orderId    = data.OrderId
        let terminal   = data.request.TerminalTXNNumber
        let hostSerialNumber = data.request.HostSerialNumber
        let refundKey  = data.request.refundKey
        let actionType = data.request.ActionType
        let requestData = new Map()
        if(serviceType == SERVICE_TYPE.REFUND || serviceType == SERVICE_TYPE.CANCEL || serviceType == SERVICE_TYPE.EASYCARD_EZCREFUND ){
            requestData.set("RefundKey", refundKey)
        }
        if(serviceType == SERVICE_TYPE.CANCEL){
            requestData.set("ActionType", actionType)
        }
        requestData.set("Method", METHOD.EASYCARD)
        requestData.set("ServiceType", serviceType)
        requestData.set("MchId", mchId)
        requestData.set("TradeKey", this.props.tradeKey)
        requestData.set("CreateTime", getNowTime())
        requestData.set("Retry", retry)
        requestData.set("DeviceId", deviceId)
        requestData.set("Amount", amount)
        requestData.set("StoreOrderNo", orderId)
        requestData.set("TerminalTXNNumber", terminal)
        requestData.set("HostSerialNumber", hostSerialNumber)
        this.props.actions.fetchIntellaApi(requestData)
        this.timer = timeOutMinute(60,this.props.actions);
    }

// 執行完api後 根據各種response 匹配對應function    
    parserResponse(msg) {
        clearTimeout(this.timer)
        if(msg.length == 0)
            return
        try {
            var json = JSON.parse(msg)
            if(this.parserHeader(json))
                return  
            var serviceType = json.Header.ServiceType
            if (SERVICE_TYPE.EASYCARD_SIGNON === serviceType) {
                this.parserSignOn(json)
            } else if (SERVICE_TYPE.EASYCARD_DEVICE_QUERY === serviceType) {
                this.parserDeviceQuery(json)
            } else if (SERVICE_TYPE.EASYCARD_PAYMENT === serviceType) {
                this.parserPayment(json)
            } else if (SERVICE_TYPE.EASYCARD_BALANCE_QUERY === serviceType) {
                this.parserBalance(json)
            } else if (SERVICE_TYPE.EASYCARD_SETTLEMENT === serviceType) {
                this.parserCheckout(json)
            } else if (SERVICE_TYPE.EASYCARD_ORDERQUERY === serviceType) {
                this.parserOrderQuery(json);
            } else if (SERVICE_TYPE.EASYCARD_REFUND === serviceType) {
                this.parserDeductionRefund(json);
            } else if (SERVICE_TYPE.EASYCARD_ADD_VALUE === serviceType) {
                this.parserAddValue(json)
            } else if (SERVICE_TYPE.EASYCARD_ID_QUERY === serviceType) {
                this.parserCardNumberQuery(json)
            } else if (SERVICE_TYPE.EASYCARD_EZCREFUND === serviceType) {
                this.parserEzcRefund(json)
            } else if (SERVICE_TYPE.EASYCARD_CANCEL_ORDER_QUERY === serviceType) {
                this.parserCancelOrderQuery(json)
            } else if (SERVICE_TYPE.EASYCARD_CANCEL === serviceType) {
                this.parserCancel(json);
            } else if (SERVICE_TYPE.EASYCARD_RESERVEORDER_QUERY === serviceType) {
                this.parserReserveOrderQuery(json);
            } else if (SERVICE_TYPE.EASYCARD_TRANSACTION_DETAIL_QUERY === serviceType) {
                this.parserTransactionDetailQuery(json);
            // } else {
            //     //todo closeInprogressDialog();
            }
        }catch(e){
            consoleForDebug(e)
        }
    }

    parserHeader(json) {
        try {
            let stateCode = json.Header.StatusCode
            let stateCodeExplain =""
            if('0000' !== stateCode) {
                var title = ''
                try {
                    var serviceType = json.Header.ServiceType
                    switch(stateCode) {
                        case '7168':
                            stateCodeExplain = '退款密碼錯誤'
                            break
                        case '0001':
                            stateCodeExplain = '目前無交易需結帳'
                            break
                        case '8015':
                            stateCodeExplain = '網路連線失敗'
                    }
                    if('0001' === stateCode && SERVICE_TYPE.EASYCARD_SETTLEMENT === serviceType) {
                        //todo  showTradeFailDialog(tradeFailStatus[0], 目前無交易需結帳, "")
                        let valueMap = new Map()
                        valueMap.set(stateCodeExplain)
                        this.setState({
                            isShowErrorMessageAlert: true,
                            tradeResultStatus:TRADE_RESULT_TYPE.ALERT,
                            tradeResultTitle: easycardStrings.CHECKOUT,
                            tradeResultMsg: valueMap,
                            alertOnPress: ()=> this.closeRederErrorAlert(),
                        })
                        return true
                    }
                    if('8015' === stateCode && stateCodeExplain){
                        let valueMap = new Map()
                        valueMap.set(`錯誤代碼: ${stateCode} `)
                        valueMap.set(`錯誤訊息: ${stateCodeExplain}`)
                        this.setState({
                            isShowErrorMessageAlert: true,
                            tradeResultTitle: '失敗',
                            tradeResultMsg: valueMap,
                            alertOnPress: ()=> this.closeRederErrorAlert(),
                        })
                        return true
                    }
                    if('7168' === stateCode && stateCodeExplain) {
                        let valueMap = new Map()
                        valueMap.set(`錯誤代碼: ${stateCode} `)
                        valueMap.set(`錯誤訊息: ${stateCodeExplain}`)
                        this.setState({
                            isShowErrorMessageAlert: true,
                            tradeResultTitle: '交易失敗',
                            tradeResultMsg: valueMap,
                            alertOnPress: ()=> this.closeRederErrorAlert(),
                        })
                        return true
                    }

                }catch(e){
                }
                // return true
            }
            return false
        }catch(e) {
            // consoleForDebug(`parserErrorCode() Error:\n${e}`)
        }
        return true
    }
    
    parserErrorMessage(json , status , title){ // status 代表要表示的狀態 1.alert緊告 2.fail失敗
        try{
            let errorStatus = (status === "alert" ? TRADE_RESULT_TYPE.ALERT : TRADE_RESULT_TYPE.FAIL)
            let errorTitle = title
            let errorCode = json.Data.ErrorCode
            let errorCodeExplain = parserErrorCode(errorCode)
            let valueMap = new Map()
            valueMap.set(`錯誤代碼: ${errorCode}`)
            if(errorCodeExplain){
                valueMap.set(`錯誤訊息: ${errorCodeExplain}`)
            }
            if(errorCode === '000125'){
                alertOnPress = ()=> {
                    this.retry(json)
                    this.closeRederErrorAlert()
                }
            }else{
                alertOnPress = ()=> this.closeRederErrorAlert()
            }
            this.setState({
                isShowErrorMessageAlert: true,
                tradeResultTitle: errorTitle,
                tradeResultStatus: errorStatus,
                tradeResultMsg: valueMap,
                alertOnPress: alertOnPress,
            })
        }catch(e){

        }
    }

    parserDeviceQuery(json){
        let deviceList = json.Data.DeviceList
        if(deviceList.length == 1){
            this.props.actions.setEasycardDevice(deviceList[0].ownerDeviceId)
            return
        }
        return
    }

    parserSignOn(json) {
        try {
            if('Success' === json.Data.TXNResult){
                this.setState({isSignOn: true})
            }else{
                this.setState({isSignOn: false})
                return this.parserErrorMessage(json , 'alert' ,easycardStrings.SIGNON + easycardStrings.FAIL)
            }      
        }catch(e){
            this.setState({isSignOn: false})
        }
    }

    parserPayment(json) {
        try {
            let txnResult = json.Data.TXNResult //交易結果
            if("Success" === txnResult) {
                let tradeTime = json.Data.Date + json.Data.Time //交易時間
                let EZCardID  = json.Data.EZCardID // 卡號
                let balance = json.Data.Balance // 扣款後餘額
                let beforeTXNBalance = json.Data.BeforeTXNBalance // 扣款前餘額
                let amount = json.Data.Amount // 交易金額
                let autoTopUpAmount = json.Data.AutoTopUpAmount
                let valueMap = new Map()
                valueMap.set(`${easycardStrings.TRADETIME}:`, _formatDate(tradeTime))
                valueMap.set(`${easycardStrings.EZCARDID}:` , EZCardID)
                valueMap.set(`${easycardStrings.TRADETYPE}:`,json.Data.CardDesc)
                valueMap.set(`${easycardStrings.DEDUCTION}前餘額:`, formatMoneyToCama(beforeTXNBalance))
                valueMap.set(`自動加值金額:`, autoTopUpAmount)
                valueMap.set(`${easycardStrings.DEDUCTION}金額:`, formatMoneyToCama(amount))
                valueMap.set(`${easycardStrings.DEDUCTION}後餘額:`, formatMoneyToCama(balance))
                this.setState({
                    balance: formatMoneyToCama(balance),
                    isShowNormalTradeResultAlert: true,
                    tradeResultTitle: '扣款成功',
                    tradeResultStatus: TRADE_RESULT_TYPE.SUCCESS,
                    tradeResultMsg: valueMap,
                    alertOnPress: ()=> this.closeNormalTradeResultAlert(),
                })
                return;
            }else{
              return this.parserErrorMessage(json , "fail" , easycardStrings.DEDUCTION + easycardStrings.FAIL)
            }
        }catch(e){
        }
        // showTradeFailDialog(tradeFailStatus[1], getResources().getString(R.string.deductions)+getResources().getString(R.string.fail), "parserPayment Error");
    }

    parserOrderQuery(json) {
        let data = json.Data
        let orderListAddSelect = data.OrderList
        try {
            if('00000' === data.ErrorCode) {
                this.setState({
                    recordDatas: orderListAddSelect,
                    isShowRefoundListAlert: true,
                    serviceType: SERVICE_TYPE.EASYCARD_ORDERQUERY
                })
                return
            }else{
                return this.parserErrorMessage(json , "fail" , easycardStrings.SEARCH + easycardStrings.FAIL)
            }
           
        }catch(e){
            // consoleForDebug(`Error: parserOrderQuery()\ne\nJson:${JSON.stringify(json)}`)
        }
        // showTradeFailDialog(tradeFailStatus[1], "搜尋紀錄"+getResources().getString(R.string.fail), "parserOrderQuery Error");
    }

    parserBalance(json) {
        //todo
        try {
            var txnResult = json.Data.TXNResult
            if("Success" === txnResult) {
                var money =  json.Data.Balance
                // setBalance(money)
                // closeInprogressDialog()
                this.setState({balance: formatMoneyToCama(money)})
                return
            }else{
                return this.parserErrorMessage(json , "fail" , easycardStrings.CARD_QUERY + easycardStrings.FAIL)
            }
            // showTradeFailDialog(tradeFailStatus[1], getResources().getString(R.string.cardQuery)+getResources().getString(R.string.fail), EZCErrorCode.parserErrorCode(errorCode));
            // return
        }catch(e){
            consoleForDebug(`Error: parserBalance()\ne\nJson:${JSON.stringify(json)}`)
        }
        // showTradeFailDialog(tradeFailStatus[1], getResources().getString(R.string.cardQuery)+getResources().getString(R.string.fail), "parserBalance Error");
    }

    parserCheckout(json) {
        try {
            var datas     = json.Data
            var txnResult = json.Data.TXNResult
            if("Success" === txnResult) {
                // Map valueMap = new LinkedHashMap();
                let valueMap = new Map();
                valueMap.set(`${easycardStrings.ADD_VALUE}筆數`, json.Data.TxReserveCount)
                valueMap.set(`${easycardStrings.ADD_VALUE}總金額`, formatMoneyToCama(json.Data.TxReserveAmount))
                valueMap.set("加值取消筆數", json.Data.TxCancelCount)
                valueMap.set("加值取消總金額", formatMoneyToCama(json.Data.TxCancelAmount))
                valueMap.set(`${easycardStrings.TRADE_COUNT}筆數`, json.Data.TxTradeCount)
                valueMap.set(`${easycardStrings.TRADE_COUNT}總金額`, formatMoneyToCama(json.Data.TxTradeAmount))
                valueMap.set(`${easycardStrings.REFUND}筆數`, json.Data.TxRefundCount)
                valueMap.set(`${easycardStrings.REFUND}總金額`,formatMoneyToCama(json.Data.TxRefundAmount))
                this.setState({
                    isShowNormalTradeResultAlert: true,
                    tradeResultTitle: easycardStrings.CHECKOUT,
                    tradeResultStatus: TRADE_RESULT_TYPE.SUCCESS,
                    tradeResultMsg: valueMap,
                    alertOnPress: ()=> this.closeNormalTradeResultAlert(),
                })
                // showCheckOutResultDialog(getResources().getString(R.string.checkOut)+"成功", valueMap)
                // consoleForDebug(JSON.stringify(json.Data))
                return
            }
        }catch(e){
            console.log(e)
        }
        // showTradeFailDialog(tradeFailStatus[1], getResources().getString(R.string.checkOut)+getResources().getString(R.string.fail), "parserCheckout Error");
    }
    parserDeductionRefund(json){
        try {
            var txnResult = json.Data.TXNResult
            if("Success" === txnResult) {
                let tradeTime = json.Data.Date + json.Data.Time
                let EZCardID  = json.Data.EZCardID 
                let balance = json.Data.Balance
                let beforeTXNBalance = json.Data.BeforeTXNBalance
                let amount = json.Data.Amount
                let valueMap = new Map()
                valueMap.set(`${easycardStrings.TRADETIME}:`, _formatDate(tradeTime))
                valueMap.set(`${easycardStrings.EZCARDID}:` , EZCardID)
                valueMap.set(`${easycardStrings.TRADETYPE}:`,easycardStrings.REFUND)
                valueMap.set(easycardStrings.REFUND+"前金額:", formatMoneyToCama(beforeTXNBalance))
                valueMap.set(easycardStrings.REFUND+"金額:", formatMoneyToCama(amount))
                valueMap.set(easycardStrings.REFUND+"後金額:", formatMoneyToCama(balance))
                this.setState({
                    balance: formatMoneyToCama(balance),
                    isShowNormalTradeResultAlert: true,
                    tradeResultTitle: `${easycardStrings.REFUND}成功`,
                    tradeResultStatus: TRADE_RESULT_TYPE.SUCCESS,
                    tradeResultMsg: valueMap,
                    alertOnPress: ()=> this.closeNormalTradeResultAlert(),
                })
                return;
            }else{
                return this.parserErrorMessage(json , "fail" ,easycardStrings.REFUND + easycardStrings.FAIL )
            }
           
        }catch(e){
            // Log.e(TAG, "parserAddValue() Error:\n"+e.getMessage());
        }
    }
    parserEzcRefund(json) {
        //todo
        try {
            var txnResult = json.Data.TXNResult
            if("Success" === txnResult) {
                let tradeTime = json.Data.Date + json.Data.Time
                let EZCardID  = json.Data.EZCardID 
                var balance = json.Data.Balance
                var beforeTXNBalance = json.Data.BeforeTXNBalance
                var amount = json.Data.Amount
                var valueMap = new Map()
                valueMap.set(`${easycardStrings.TRADETIME}:`, _formatDate(tradeTime))
                valueMap.set(`${easycardStrings.EZCARDID}:` , EZCardID)
                valueMap.set(`${easycardStrings.TRADETYPE}:`,easycardStrings.CANCELORDER)
                valueMap.set(easycardStrings.REFUND+"前金額:", formatMoneyToCama(beforeTXNBalance))
                valueMap.set(easycardStrings.REFUND+"金額:", formatMoneyToCama(amount))
                valueMap.set(easycardStrings.REFUND+"後金額:", formatMoneyToCama(balance))
                this.setState({
                    balance: formatMoneyToCama(balance),
                    isShowNormalTradeResultAlert: true,
                    tradeResultTitle: `${easycardStrings.MONEY_REFUND}成功`,
                    tradeResultStatus: TRADE_RESULT_TYPE.SUCCESS,
                    tradeResultMsg: valueMap,
                    alertOnPress: ()=> this.closeNormalTradeResultAlert(),
                })
                return
            }else{
                return this.parserErrorMessage(json , "fail" ,easycardStrings.MONEY_REFUND + easycardStrings.FAIL )
            }
            // showTradeFailDialog(tradeFailStatus[1], getResources().getString(R.string.moneyRefund)+getResources().getString(R.string.fail), EZCErrorCode.parserErrorCode(errorCode));
            // return
        }catch(e){
            consoleForDebug(`Error: parserEzcRefund()\ne\nJson:${JSON.stringify(json)}`)
        }
    }

    parserAddValue(json) {
        try {
            var txnResult = json.Data.TXNResult
            if("Success" === txnResult) {
                let tradeTime = json.Data.Date + json.Data.Time
                let EZCardID  = json.Data.EZCardID 
                let balance = json.Data.Balance
                let beforeTXNBalance = json.Data.BeforeTXNBalance
                let amount = json.Data.Amount
                let valueMap = new Map()
                valueMap.set(`${easycardStrings.TRADETIME}:`, _formatDate(tradeTime))
                valueMap.set(`${easycardStrings.EZCARDID}:` , EZCardID)
                valueMap.set(`${easycardStrings.TRADETYPE}:`,easycardStrings.ADD_VALUE)
                valueMap.set("加值前金額", formatMoneyToCama(beforeTXNBalance))
                valueMap.set("加值金額", formatMoneyToCama(amount))
                valueMap.set("加值後金額", formatMoneyToCama(balance))
                this.setState({
                    balance: formatMoneyToCama(balance),
                    isShowNormalTradeResultAlert: true,
                    tradeResultTitle: '加值成功',
                    tradeResultStatus: TRADE_RESULT_TYPE.SUCCESS,
                    tradeResultMsg: valueMap,
                    alertOnPress: ()=> this.closeNormalTradeResultAlert(),
                })
                return
            }else{
                return this.parserErrorMessage(json , "fail" ,easycardStrings.ADD_VALUE + easycardStrings.FAIL)
            }
            // showTradeFailDialog(tradeFailStatus[1], getResources().getString(R.string.addValue)+getResources().getString(R.string.fail), EZCErrorCode.parserErrorCode(errorCode));
            // return;
        }catch(e){
            consoleForDebug(`Error: parserAddValue()\ne\nJson:${JSON.stringify(json)}`)
        }
        // showTradeFailDialog(tradeFailStatus[1], getResources().getString(R.string.addValue)+getResources().getString(R.string.fail), "parserAddValue Error");
    }

    parserCardNumberQuery(json) {
        try {
            var txnResult = json.Data.TXNResult 
            if("Success" === txnResult) {
                var cardNumber =  json.Data.EZCardID
                var title = "卡號： " + cardNumber
                this.setState({
                    cardNumber: title,
                    isShowCardNumberAlert: true,
                    alertOnPress: ()=> this.setState({isShowCardNumberAlert:false, cardNumber: ''})
                })
                return
            }else{
                return this.parserErrorMessage(json , "fail" ,easycardStrings.EZCARD_NUMBER_QUERY + easycardStrings.FAIL)
            }
            // showTradeFailDialog(tradeFailStatus[1], getResources().getString(R.string.ezCardNumberQuery)+getResources().getString(R.string.fail), EZCErrorCode.parserErrorCode(errorCode));
            // return
        }catch(e){
            consoleForDebug(`Error: parserCardNumberQuery()\ne\nJson:${JSON.stringify(json)}`)
        }
        // showTradeFailDialog(tradeFailStatus[1], getResources().getString(R.string.ezCardNumberQuery)+getResources().getString(R.string.fail), "parserCardNumberQuery Error");
    }

    parserCancelOrderQuery(json){
        let data = json.Data
        let actionType = data.ActionType
        let orderId = data.TxnOrderId
        let deviceId = data.TxnDeviceId
        let createTime = _formatDate(data.CreateDate)
        let amount = data.Amount
        let ezCardID = data.EZCardID
        let refundStatus = data.RefundStatus
        try {
            if('00000' === data.ErrorCode) {
                let recordDatas = { actionType, ezCardID ,deviceId, createTime, amount  }
                this.setState({
                    cancelActionType: actionType,
                    cancelOrderId: orderId,
                    cancelRefundStatus : refundStatus,
                    recordDatas: recordDatas,
                    isShowCancelQueryAlert: true,
                   
                })
                return
            }else{
                return this.parserErrorMessage(json , "fail" , easycardStrings.GET_DATA + easycardStrings.FAIL)
            }
        }catch(e){
            // consoleForDebug(`Error: parserOrderQuery()\ne\nJson:${JSON.stringify(json)}`)
        }
    } 
    parserCancel(json){
        try{
            let txnResult = json.Data.TXNResult
            if("Success" === txnResult) {
                let tradeTime = json.Data.Date + json.Data.Time
                let EZCardID  = json.Data.EZCardID 
                let balance = json.Data.Balance
                let beforeTXNBalance = json.Data.BeforeTXNBalance
                let amount = json.Data.Amount
                let valueMap = new Map()
                valueMap.set(`${easycardStrings.TRADETIME}:`, _formatDate(tradeTime))
                valueMap.set(`${easycardStrings.EZCARDID}:` , EZCardID)
                valueMap.set(`${easycardStrings.TRADETYPE}:`,easycardStrings.MONEY_REFUND)
                valueMap.set(easycardStrings.CANCELORDER+"前金額:", formatMoneyToCama(beforeTXNBalance))
                valueMap.set(easycardStrings.CANCELORDER+"金額:", formatMoneyToCama(amount))
                valueMap.set(easycardStrings.CANCELORDER+"後金額:", formatMoneyToCama(balance))
                this.setState({
                    balance: formatMoneyToCama(balance),
                    isShowNormalTradeResultAlert: true,
                    tradeResultTitle: `${easycardStrings.CANCELORDER}成功`,
                    tradeResultStatus: TRADE_RESULT_TYPE.SUCCESS,
                    tradeResultMsg: valueMap,
                    alertOnPress: ()=> this.closeNormalTradeResultAlert(),
                })
                return
            }else{
                return this.parserErrorMessage(json , "fail" ,easycardStrings.CANCELORDER + easycardStrings.FAIL )
            }
        }catch(e){

        }    
    }
    parserReserveOrderQuery(json){
        let data = json.Data
        let header = json.Header
        let orderListAddSelect = data.List
        try {
            if('0000' === header.StatusCode) {
                this.setState({
                    recordDatas: orderListAddSelect,
                    isShowDetailQueryAlert: true,
                    serviceType:SERVICE_TYPE.EASYCARD_RESERVEORDER_QUERY
                })
                return
            }else{
                return this.parserErrorMessage(json , "fail" , easycardStrings.ADDVALUEQUERY + easycardStrings.FAIL)
            }
        }catch(e){
            // consoleForDebug(`Error: parserOrderQuery()\ne\nJson:${JSON.stringify(json)}`)
        }
    }
    parserTransactionDetailQuery(json){
        let data = json.Data
        let orderListAddSelect = data.CardTXNData
        try {
            if('000000' === data.ErrorCode) {
                this.setState({
                    recordDatas: orderListAddSelect,
                    isShowDetailQueryAlert: true,
                    serviceType: SERVICE_TYPE.EASYCARD_TRANSACTION_DETAIL_QUERY
                })
                return
            }else{
                return this.parserErrorMessage(json , "fail" , easycardStrings.EZCARDTRANSACTIONDETAULSQUERY + easycardStrings.FAIL)
            }
           
        }catch(e){
            // consoleForDebug(`Error: parserOrderQuery()\ne\nJson:${JSON.stringify(json)}`)
        }
    }
  }

export default connect(
    state => ({
        merchId: state.settings.merchantId,
        tradeKey: state.settings.tradeKey,
        randomid: state.settings.randomid,
        isFetching: state.easycard.isFetching,
        fetchResponse: state.easycard.fetchResponse,
        serviceType: state.easycard.serviceType,
        easycardDevice: state.easycard.easycardDevice,
    }),
    (dispatch) => ({
        actions: bindActionCreators(easycardActions, dispatch)
    })
)(EZCardPage)

  const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        marginTop: Platform.OS === 'ios'? 30 : 0,
        backgroundColor: 'white'
    },
    topViewWrapper: {
        flex: 2.3,
        flexDirection: 'row',
    },
    dividingLine: {
        height: 3,
        backgroundColor: commonColor.ezcardPageDividingLineColor,
    },
    bottomViewWrapper: {
        flex: 7.7,
        padding: 10,
    },
    eziconWrapper: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    ezicon: {
        height: 80 *fontSizeScaler,
        width: 100 *fontSizeScaler,
        resizeMode: 'center'
    },
    topDivLine: {
        width: 1,
        backgroundColor: '#9f9f9f',
        marginVertical: 25,
    },
    topTextWrapper: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    topTitleText: {
        fontSize: 20 *fontSizeScaler,
        color: commonColor.darkGrayColor,
    },
    topMsgText: {
        paddingTop: 5,
        fontSize: 20 *fontSizeScaler,
        color: commonColor.customerScan,
    },
    bottomTextWrapper: {
        flex: 1.1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    bottomBtnWrapper: {
        flex: 3,
    },
    chooseText: {
        fontSize: 22 *fontSizeScaler,
    },
    nowDevice: {
        paddingTop: 5,
        fontSize: 20 *fontSizeScaler,
    },
    itemWrapper: {
        flex: 1,
        alignItems: 'center',
        marginBottom: 10,
        marginHorizontal: 5,
        paddingVertical: 10,
    },
    itemImage: {
        height: 40 *fontSizeScaler,
        width: 40 *fontSizeScaler,
        resizeMode: 'cover'
    },
    itemText: {
        color: 'white',
        fontSize: 15 *fontSizeScaler,
        paddingTop: 5,
    },
    rowStyle: {
        fontSize: 16 *fontSizeScaler,
        width:'100%',
        flex:0,
        textAlign: 'left',
        marginBottom: 6
    },
    rowValueStyle:{
        fontSize: 14 *fontSizeScaler,
        flex: 5,
        textAlign: 'right',
    },
    alertContainerStyle:{
        width:windowW-30, 
        height: windowH *2/3+20 ,
        left:15,
        top: (windowH * 1/3)/2

    },
    alertContentStyle:{
        width:windowW-15,
        height: windowH *2/3,
    },
    recordRefundTitle:{
        fontSize:16 *fontSizeScaler,
        textAlign:'center',
        marginTop:5
    },
    bottomStyle:{
        marginTop:5,
        marginBottom:-5
    }
  })