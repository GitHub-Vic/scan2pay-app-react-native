import React, {Component} from 'react'
import {StyleSheet, Platform, View, Image, Text, Keyboard, TouchableHighlight, WebView, Linking, ActivityIndicator} from 'react-native'
import {windowH, windowW, dialogBorderRadius, commonColor, fontSizeScaler} from '../commonStyles'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import * as settingsActions from '../actions/settingsActions'
import * as merchantActions from '../actions/merchantActions'
import {URL_LOGIN, URL_INTELLA_OAUTH2_DOMAIN, APP_STORE_ID, APP_ID, PLATFORM, URL_DOMAIN, APP_VERSION, URL_INTELLA_DOMAIN, URL_SCHEME} from '../config'
import {httpsPost, mapToJsonString, goHome, forIphoneXStyleByOrientation} from '../lib/functions'
import {SHA256} from '../lib/SHA256'
import store from '../store'
import appActions from '../actions'
import LoadingAlert from '../components/alert/TitleWithOneBtn'
import CheckBox from 'react-native-check-box'
import InputIconPassword from  '../components/InputIconPassword'
import LoginButton from  '../components/LoginButton'
import mqtt from '../lib/mqtt'
import apnsToken from '../lib/apnsToken'
import Toolbar from '../components/Toolbar'
import Dialog from "react-native-dialog"
import RNExitApp from 'react-native-exit-app'
import CookieManager from 'react-native-cookies';
import TradeResult from '../components/alert/TradeResult'

class LoginPage extends Component {
  constructor() {
    super()
    this.state = {
      iphoneXstyle: forIphoneXStyleByOrientation(),
      account: '',
      password: '',
      status: '',
      isKeyboardShow: false,
      keyboardHeight: 0,
      isShowLoadingAlert: false,
      showWebView: false,
      dialogVisible: false,
      loadingCheckVersion: true,
      canLeftWebView: true,
      resultTitle: '錯誤',
      resultMsg: '',
      showResult: false,
      webViewKey: 0
    }
    this.handleMessage = this.handleMessage.bind(this)
  }

  async componentDidMount() {
    await this.checkVersion()
    this.props.merchActions.resetChoosedMerchant()
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', (e) => this.keyboardDidShow(e))
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => this.keyboardDidHide())
    this.props.isStoreAccount ? this.setState({account: this.props.storeAccount}) : null
    CookieManager.clearAll()
    Linking.addEventListener('url', this._handleOpenURL);
  }

  componentWillUnmount() {
    this.keyboardDidShowListener && this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener && this.keyboardDidHideListener.remove();
    Linking.removeEventListener('url', this._handleOpenURL);
  }
  
  _handleOpenURL = (event) => {
    console.log('_handleOpenURL');
    console.log(event.url);
    var url = event.url
    if (url) {
      console.log('Initial url is: ' + url);
      if (url.startsWith(`${URL_SCHEME}://`)) {
        const queryString = url.replace(`${URL_SCHEME}://`, '')
        const jsonQueryString = JSON.parse('{"' + decodeURIComponent(queryString).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}')
        // console.log('jsonQueryString', jsonQueryString)
        //const appAccountId = jsonQueryString.accountId
        //const appTokenId = jsonQueryString.tokenId
        // console.log('this.state.appTokenId', this.state.appTokenId)
        // console.log('this.state.appAccountId', this.state.appAccountId )
        this.setState({
          appTokenId: jsonQueryString.tokenId,
          appAccountId: jsonQueryString.accountId
        })
        //console.log('_handleOpenURL', this.refWeb)
        // console.log("this.refWeb", this.refWeb)
        // console.log("this.state.appTokenId", this.state.appTokenId)
        // console.log("this.state.appAccountId", this.state.appAccountId)
        // console.log("this.refWeb && this.state.appTokenId && this.state.appAccountId", this.refWeb && this.state.appTokenId && this.state.appAccountId)
        if (this.refWeb && this.state.appTokenId && this.state.appAccountId)  {
          this.setState({
            webViewKey: this.state.webViewKey + 1
          })
        }
      }
    }
  }

  handleUpdate () {
    Linking.openURL(`itms-apps://itunes.apple.com/tw/app/id${APP_STORE_ID}?mt=8`).catch(err => console.log('open market',err))
  }

  handleExit () {
    RNExitApp.exitApp()
  }

  mapToJsonString(map){
    var json = "{";
    for (var key in map) {
       json += `"${key}":"${map[key]}",`;
     }
     json = json.substring(0, json.length-1);
     json += "}";
     return json;
  }

  async checkVersion() {
    const response = await fetch(`${URL_DOMAIN}allpaypass/api/system/app/version`,
    {
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
      },
      method: "POST",
      body: this.mapToJsonString({
        "type": "IOS",
        "appversion": APP_VERSION,
        appId: APP_ID,
      })
    })
    const responseJson = await response.json()
    let jsonMessage = JSON.parse(responseJson.message)
    if (jsonMessage.updateversion === 'true') {
      this.setState({ dialogVisible: true })
    }
    this.setState({ loadingCheckVersion: false })
  }

  render() {
    const topSpaceStyle = this.state.isKeyboardShow ? {height: 0} : null
    const logoSpaceStyle = this.state.isKeyboardShow ? {height: windowH * 2/14} : null
    const logoStyle = this.state.isKeyboardShow ? {height: windowH * 1.8/14, width: windowH * 1.8/14} : null
    const apiUrl = `${getOAuthAuthorizeURL('allpaypass_server', 'code')}`
    + (this.state.appAccountId ? `&appAccountId=${this.state.appAccountId}` : '')
    + (this.state.appTokenId ? `&appTokenId=${this.state.appTokenId}` : '')
    console.log('LoginPage apiUrl:',apiUrl)
    return (
      !this.state.showWebView ? 
      <Image source={{ uri: 'bg_login'}} style={[styles.wrapper, this.state.iphoneXstyle]}>
        {this.props.isFetching ? (
              <LoadingAlert title={'加載中'} />
            ) : null}
        <View style={{flex: 1, alignItems:'center'}}>
          {
            this.state.dialogVisible
            ?
            <Dialog.Container visible={true}>
                <Dialog.Title>請更新版本</Dialog.Title>
                <Dialog.Button label="取消退出" onPress={this.handleExit} />
                <Dialog.Button label="前往更新" onPress={this.handleUpdate} />
            </Dialog.Container>
            :
            null
          }
          <View style={[styles.topSpace, topSpaceStyle]} />
          <View style={[styles.logoSpace ,logoSpaceStyle]}>
            <Image style={[styles.logo, logoStyle]} source={{uri: 'icon_login_logo'}}></Image>
          </View>
            <View style={styles.contentSpace}>
              <InputIconPassword textInputRef={(r)=> this.accountTextInput = r}
                password={false} 
                icon={'icon_settings_account'}
                onTextChange={(text) => this.setState({account: text})}
                inputValue={this.state.account}
                hintText={'帳號'}
                returnKeyType={'next'}
                onSubmitEditing={()=> this.passwordTextInput ? this.passwordTextInput.focus() : {}} />
              <View style={styles.checkBox}>
                <CheckBox onClick={()=> this.props.actions.setIsStoreAccount(!this.props.isStoreAccount)}
                  isChecked={this.props.isStoreAccount}
                  checkBoxColor={commonColor.customerScan}
                  rightTextView={<Text style={styles.rememberAccount}>記住帳號</Text>}/>
              </View>
              <View style={{height:5}} />
              <InputIconPassword textInputRef={(r)=> this.passwordTextInput = r}
                password={true} 
                icon={'icon_inputtext_password'}
                onTextChange={(text) => this.setState({password: text})}
                inputValue={this.state.password}
                hintText={'密碼'}
                returnKeyType={'go'} 
                onSubmitEditing={()=> this.login()}/>
                
              <View style={styles.forgetPasswordWrapper}>
                <TouchableHighlight 
                  underlayColor={'transparent'}
                  onPress={()=> this.props.navigation.navigate('ForgetPasswordPage')}>
                  <Text style={styles.forgetPassword} >？忘記密碼</Text>
                </TouchableHighlight>
              </View>

              <Text style={styles.hint}
                numberOfLines={1}>{this.state.status}</Text>
              <View style={{flexDirection: 'row'}}>
                <LoginButton style={styles.login}
                  btnColors={commonColor.createGradient}
                  title={'一 般 登 入'}
                  onPress={()=> this.login()}/>
                <LoginButton style={styles.login}
                  btnColors={commonColor.createGradient}
                  title={'SSO 登 入'}
                  onPress={()=> this.SSOlogin()}/>
              </View>
            </View>
            <View style={styles.bottomSpace} />
          </View>
          {this.state.loadingCheckVersion ? <LoadingAlert title={'確認版本資訊中'} /> : null}
          {this.state.isShowLoadingAlert ? <LoadingAlert title={'登入中'} /> : null}
      </Image>
      :
      <View  style={[styles.wrapper, this.state.iphoneXstyle]}>
        {this.renderToolBar()}
        <WebView
          key={this.state.webViewKey}
          ref={(myWeb) => this.refWeb = myWeb}
          source={{uri: apiUrl, headers: { 'Cache-Control':'no-cache'} }}
          startInLoadingState
          automaticallyAdjustContentInsets={true}
          style={{maxHeight: windowH - 80}} // OR style={{height: 100, width: 100}}
          renderLoading={this.renderLoading}
          onMessage={this.handleMessage}
          onNavigationStateChange={this.onNavigationStateChange.bind(this)}
        />
        {
          this.state.showResult ?
          <TradeResult
            imgPath={'icon_receipt_result_fail'}
            title={this.state.resultTitle}
            msg={
              this.state.resultMsg
            }
            onPress={() => {
              this.setState({ 
                showResult: false,
                resultMsg: '',
                showWebView: false,
              })
              CookieManager.clearAll()
            }}
            style={{zIndex: 100}}
          />
          : null
        }
      </View>
    )
  }

  renderLoading() {
    return (<View style={[styles.infoBox, {height: '80%'}]}>
        <View style={styles.infoItem}>
          <ActivityIndicator size="large" color="#00ff00" />
        </View>
      </View>
    )
  }

  onNavigationStateChange(navState) {
    this.setState({canLeftWebView: false})
    if(URL_INTELLA_OAUTH2_DOMAIN.split('/')[2] === navState.url.split('/')[2]) {
      this.setState({canLeftWebView: true}) 
    }
    // this.setState({
    //   canGoBack: navState.canGoBack
    // })
  }

  handleMessage (evt) {
    const message = evt.nativeEvent.data
    console.log('handleMessage', message)
    let data
    try {
        console.log("message", message)
        data = JSON.parse(message);
    } catch (e) {
        return
    }
    if (data.type === 'getCode') {
      CookieManager.clearAll()
      var requestData = new Map()
      requestData.set('code', data.msg.code)
      requestData.set('clientId', 'allpaypass_server')
      this.props.merchActions.getToken(`${URL_INTELLA_DOMAIN}user/getToken` ,requestData, {
        successFunction: () => {
          this.setState({showWebView: false})
          goHome(this.props.navigation)
        },
        failFunction: (data) => {
          this.getTokenFailCallBack(data)
        }
      })
    } else if (data.type === 'openBrowser') {
      Linking.openURL(data.msg);
    } else if (data.type === 'getUrl') {
      let path = data.msg.path
      console.log('path', path)
      if('/login' === path) {
        this.setState({canLeftWebView: true})
      } else {
        this.setState({canLeftWebView: false})
      }
    }
  }

  getTokenFailCallBack(data) {
    this.setState({
      resultMsg: data.resultMsg,
      showResult: true,
    })
  }

  renderToolBar() {
    return (
      <Toolbar
        backgroundImg={'bg_toolbar_create'}
        backOnPress={() => this.pressBackBtn()}
        hideHome={true}
      />
    )
  }

  pressBackBtn () {
    console.log('pressBackBtn canLeftWebView', this.state.canLeftWebView)
    if (!this.state.canLeftWebView) {
      this.refWeb.goBack();
    }else{
      this.setState({showWebView: false})
    }
  }

  keyboardDidShow(e) {
    this.setState({
      isKeyboardShow: true,
      keyboardHeight: e.endCoordinates.height,
    });
  }

  keyboardDidHide() {
    this.setState({
      isKeyboardShow: false,
      keyboardHeight: 0,
    });
  }

  SSOlogin() {
    this.setState({showWebView: true})
  }

  async login() {
    this.passwordTextInput.blur()
    const account = this.state.account
    const password = this.state.password
    if(account.length <= 0 || password.length <= 0) {
      this.setState({status: '帳號或密碼不能為空'})
      return
    }

    this.setState({isShowLoadingAlert: true})
    var requestData = new Map()
    requestData.set("accountId", account)
    requestData.set("pin", SHA256(password))
    requestData.set("serialId", this.props.udid)
    requestData.set("appId", APP_ID)
    requestData.set("platform", PLATFORM)
    console.log('requestData login', requestData)
    let msg =  await httpsPost(URL_LOGIN, mapToJsonString(requestData))
    try {
      var response = JSON.parse(msg)
      if(response.loggedIn) {
        this.loginSuccess(response.username, response.storeName, response.tradeKey,response.randomid)
      }else {
        this.setState({status: response.message, isShowLoadingAlert: false})
      }
    }catch(e) {
      this.setState({status: '請再嘗試一次', isShowLoadingAlert: false})
    }
    
  }

  loginSuccess(merchId, storeName, tradeKey,randomid) {
    
    this.props.actions.login(merchId, storeName, tradeKey,randomid)
    store.dispatch(appActions.easycardActions.setEasycardDevice(''))
    mqtt().disConnectForLogout()
    mqtt().connectForLogin()
    Platform.OS === 'ios' ? apnsToken().register() : null
    goHome(this.props.navigation)
  }

}

export function  getOAuthAuthorizeURL(clientId: string, responseType: string): string {
  // https://dev.intella.co/oauth2-server/oauth/authorize?client_id=donate_app&response_type=code
  // return `${S2P_OAUTH_AUTHORIZE_URL}?client_id=${clientId}&response_type=${responseType}`;
  return `${URL_INTELLA_OAUTH2_DOMAIN}oauth2-ui/login?appId=${URL_SCHEME}&platForm=ios&oauth_login_type=oauth2Login&client_id=${clientId}&response_type=${responseType}`
}

export default connect(
  state => ({
    isStoreAccount: state.settings.isStoreAccount,
    storeAccount: state.settings.account,
    udid: state.settings.udid,
    isFetching: state.merchant.isFetching,
    tokenId: state.merchant.tokenId,
  }),
  (dispatch) => ({
    actions: bindActionCreators(settingsActions, dispatch),
    merchActions: bindActionCreators(merchantActions, dispatch)
  })
)(LoginPage);

const logoHW =  windowH * 4/14 -30
const loginBtnBottomPosition = windowH * 1/14 
const styles = StyleSheet.create({
  wrapper: {
    height: windowH,
    marginTop: Platform.OS === 'ios'? 30 : 0,
  },
  topSpace: {
    height: windowH * 1/14,
  },
  logoSpace: {
    height: windowH * 4.5/14,
    alignItems: 'center',
  },
  contentSpace: {
    height: windowH * 6.5/14,
    width: windowW * 7/9,
    borderRadius: dialogBorderRadius,
    backgroundColor: 'white',
    alignItems: 'center',
    paddingTop: 20,
  },
  bottomSpace: {
    height: windowH * 2/14,
  },
  logo: {
    height: logoHW,
    width: logoHW,
    resizeMode: 'contain'
  },
  forgetPasswordWrapper: {
    width: windowW * 1.5/3,
    alignItems: 'flex-end'
  },
  forgetPassword: {
    color: commonColor.hintTextColor,
    fontSize: 14 *fontSizeScaler,
  },
  hint: {
    width: windowW * 7/9,
    color: commonColor.defaultRedColor,
    textAlign: 'center',
    marginTop: 20,
  },
  login: {
    marginTop: 10,
    marginLeft: 10,
  },
  checkBox: {
    width: windowW * 1.5/3,
    justifyContent: 'flex-end',
    alignItems: 'flex-end'
  },
  rememberAccount: {
    color: commonColor.customerScan, 
    fontSize: 14 *fontSizeScaler
  },
  infoBox:{
    width:'100%',
    height:'100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  infoItem:{
    flexDirection:'row',
    marginTop: 3,
  },
})