import React, { Component } from 'react'
import { StyleSheet, Platform, View, Text, TouchableOpacity, Image} from 'react-native'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import {SOFTWARE_VERSION, URL_LOGOUT, APP_ID, PLATFORM} from '../config'

import { NavigationActions } from 'react-navigation'
import { goHome, forIphoneXStyleByOrientation, timeOutMinute, httpsPost, mapToJsonString } from '../lib/functions'
import * as settingsActions from '../actions/settingsActions'
import * as easycardActions from '../actions/easycardActions'
import * as merchantActions from '../actions/merchantActions'
import { commonColor, alertBtnTextSize, windowW, fontSizeScaler, } from '../commonStyles'

import MainButton from  '../components/MainButton'
import Toolbar from '../components/Toolbar'
import DoubleCheckAlert from '../components/alert/TitleWithTwoBtn'
import mqtt from '../lib/mqtt'
import apnsToken from '../lib/apnsToken'
import CookieManager from 'react-native-cookies';
import LoadingAlert from '../components/alert/TitleWithOneBtn'

class SettingsPage extends Component {

  constructor() {
    super()
    this.state = {
      iphoneXstyle: forIphoneXStyleByOrientation(),
      isShowDoubleCheckAlert: false,
      loadingTitle: '',
      showLoading: false,
      loadingTitle: ''
    }
  }

  render() {
    return (
      <View style={[styles.wrapper, this.state.iphoneXstyle]}>
        <Toolbar 
          title = {'設定'}
          backgroundImg={'bg_toolbar_create'}
          backOnPress={() => this.props.navigation.goBack()}
          homeOnPress={() => goHome(this.props.navigation)} />
        <View style={styles.itemWrapper}>
          <Image

            style={styles.image}
            source={{uri: 'icon_settings_appversion'}} />
          <Text style={styles.itemText}>軟體版本</Text>
          {/* <Text style={styles.hintText}>{this.props.sofeVersion}</Text> */}
          <Text style={styles.hintText}>{SOFTWARE_VERSION}</Text>
        </View>
        <View style={styles.itemWrapper}>
          <Image
            style={styles.image}
            source={{uri: 'icon_settings_account'}} />
          <Text style={styles.itemText}>帳號資訊</Text>
          <Text style={styles.hintText}>{this.props.merchantId}</Text>
        </View>
        {
          this.props.ssoFlag ? 
          <TouchableOpacity
              style={{width:"100%" }}
              onPress={()=> this.props.navigation.navigate('MerchantManagementPage')}>
            <View style={styles.itemWrapper}>
                  <Image
                    style={styles.ezcardImage}
                    source={{uri: 'icon_merchant_management.png'}} />
                  <Text style={styles.itemText}>特店管理</Text>
            </View>
          </TouchableOpacity>
          : null
        }
        <TouchableOpacity
           style={{width:"100%" }}
           onPress={()=> this.props.navigation.navigate('EZCardSettingConfig')}>
          <View style={styles.itemWrapper}>
                <Image
                  style={styles.ezcardImage}
                  source={{uri: 'icon_setting_ezcard.png'}} />
                <Text style={styles.itemText}>悠遊卡機</Text>
          </View>
        </TouchableOpacity>
        <View style={{height:10}} />
        <MainButton style={styles.logoutWrapper}
          btnColors={commonColor.createGradient}
          title={'登 出'}
          onPress={()=> this.setState({isShowDoubleCheckAlert: true})}/>
        <View style={styles.tradeMark}>
          <Text style={styles.markText}>{` 英特拉金融科技股份有限公司 `}</Text>
          <Text style={styles.markText}>{`客服專線:(+886)2-33225780      `}  </Text>
        </View>
        {this.state.showLoading ? (
          <LoadingAlert title={this.state.loadingTitle} />
        ) : null}
        {this.state.isShowDoubleCheckAlert ? this.renderDoublceCheckAlert() : null}
      </View>
    );
  }

  async logout() {
    this.setState({
      showLoading: true,
      loadingTitle: '登出中'
    })
    this.props.actions.logout()
    this.props.easycardAction.logoutEasycard()
    this.props.merchantAction.resetChoosedMerchant()
    mqtt().disConnectForLogout()
    Platform.OS === 'ios' ? apnsToken().unregister() : null
    var requestData = new Map()
    requestData.set("tokenId", this.props.tokenId)
    requestData.set("userId", this.props.userId)
    requestData.set("accountId", this.props.merchantId)
    requestData.set("serialId", this.props.udid)
    requestData.set("appId", APP_ID)
    requestData.set("platform", PLATFORM)
    let msg =  await httpsPost(URL_LOGOUT, mapToJsonString(requestData))
    try {
      var response = JSON.parse(msg)
    } catch (e) {
      console.log(e)
    }
    CookieManager.clearAll()
    .then((res) => {
      const resetAction = NavigationActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({ routeName: 'LoginPage'})
        ]
      })
      this.setState({
        showLoading: false,
        loadingTitle: ''
      })
      this.props.navigation.dispatch(resetAction)
    })

  }

  renderDoublceCheckAlert() {
    return (
      <DoubleCheckAlert 
        title='是否登出本程式？'
        cancelOnPress={()=> this.setState({isShowDoubleCheckAlert: false})} 
        certainOnPress={()=> this.logout()}
        defaultBtn={'cancel'}/>
    );
  }
    
}
    
export default connect(
  state => ({
    merchantId: state.settings.merchantId,
    sofeVersion: state.settings.sofeVersion,
    ssoFlag: state.merchant.ssoFlag,
    userId: state.merchant.userId,
    tokenId: state.merchant.tokenId,
  }),
  (dispatch) => ({
    actions: bindActionCreators(settingsActions, dispatch),
    easycardAction:bindActionCreators(easycardActions, dispatch),
    merchantAction:bindActionCreators(merchantActions, dispatch),
  })
)(SettingsPage);
    
const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    marginTop: Platform.OS === 'ios'? 30 : 0,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  itemWrapper: {
    flexDirection: 'row',
    alignItems: 'center', 
    paddingTop: 30,
    paddingBottom: 30,
    paddingRight: 15,
    paddingLeft: 15,
    borderBottomColor: '#DDDDDD',
    borderBottomWidth: 1,
  },
  image:{
    flex: 1,
    height: (windowW - 30) / 8,
    width: (windowW - 30) / 8,
  },
  ezcardImage:{
    
    height: (windowW - 30) / 8,
    width: (windowW - 100) / 8,
  },
  itemText:{
    flex: 4,
    fontSize: 17 *fontSizeScaler,
    paddingLeft: 20,
    color: 'black'
  },
  hintText:{
    flex: 6,
    color: commonColor.hintTextColor,
    textAlign: 'right',
    fontSize: 12 *fontSizeScaler,
  },
  logoutWrapper: {
    marginTop: 10,
  },
  logoutText: {
    color: 'white',
    fontSize: alertBtnTextSize
  },
  tradeMark: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    left: 0,
    padding: 10,
    borderTopColor: '#DDDDDD',
    borderTopWidth: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  markText: {
    textAlign:'center',
    color:commonColor.hintTextColor,
    fontSize: 12 *fontSizeScaler,
  },
    
});
        