import React, { Component } from 'react'
import { StyleSheet, Platform, Keyboard, Dimensions, View, Text, Image, TouchableHighlight,TouchableWithoutFeedback } from 'react-native'

import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as receiptActions from '../actions/receiptActions'
import { RECEIPT_MODE } from '../constants/actionTypes'

import LinearGradient from 'react-native-linear-gradient'
import { commonColor, commonStyles, windowH, windowW, elementHeigh, elementWitdh, borderRadius as radius, toggleModeBtnHeight, toolbarHeight, fontSizeScaler} from '../commonStyles'
import InputMoney from '../components/receiptPage/InputMoney'
import Toolbar from '../components/Toolbar'
import ToggleModeBtn from '../components/receiptPage/ToggleModeBtn'
import MainButton from  '../components/MainButton'

import { toggleReceiptMode } from '../actions/receiptActions'
import { goHome, forIphoneXStyleByOrientation } from '../lib/functions'
import Toast from '../components/Toast'

class ReceiptMoneyPage extends Component {

  constructor() {
    super();
    this.state = {
      iphoneXstyle: forIphoneXStyleByOrientation(),
      //change money's input position
      isKeyboardShow: true,

      //for ios's softkeyboard
      keyboardHeight: 0,
    }
  }

  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', (e) => this.keyboardDidShow(e));
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => this.keyboardDidHide());
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
    this.props.actions.resetTradeData();
    this.props.actions.resetTradeMoney();
  }

  keyboardDidShow(e) {
    this.setState({
      isKeyboardShow: true,
      keyboardHeight: e.endCoordinates.height,
    });
  }

  keyboardDidHide() {
    this.setState({
      isKeyboardShow: false,
      keyboardHeight: 0,
    });
  }

  render() {
    const majorColor = this.props.mode === RECEIPT_MODE.CUSTOMER_SCAN ?
      commonColor.customerScan : commonColor.customerBeScanned;
    const gradientColor = this.props.mode === RECEIPT_MODE.CUSTOMER_SCAN ?
      commonColor.createGradient : commonColor.scanGradient;
    const toolbarBackImg = this.props.mode === RECEIPT_MODE.CUSTOMER_SCAN ?
      'bg_toolbar_create' : 'bg_toolbar_scan';
    const title = this.props.mode === RECEIPT_MODE.CUSTOMER_SCAN ?
      '收錢碼' : '掃碼收款';
    return (
      <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss()}>
        <View style={[styles.wrapper, this.state.iphoneXstyle]}>
          <Toolbar
            title={title}
            backgroundImg={toolbarBackImg}
            backOnPress={() => this.props.navigation.goBack()}
            homeOnPress={() => goHome(this.props.navigation)} />
          <View style={this.state.isKeyboardShow ? styles.spaceKeyboardShow : styles.spaceKeyboardHide} >
            <View style={styles.storeNameSpace}>
              <Text style={styles.storeName} numberOfLines={1} >{this.props.storeName}</Text>
            </View>
            <Text style={[styles.storeNameUnderLine,{backgroundColor:majorColor}]} />
            <View style={this.state.isKeyboardShow ? {height: 10} : {flex: 1}} />
          </View>
          <InputMoney
            onTextChange={(input) => this.setReceiptMoney(input)}
            onSubmitEditing={Keyboard.dismiss}
            id={(c) => this.input = c}
            unitColor={majorColor}
            underLineColor={gradientColor}
            value={this.props.money} />

          <MainButton style={styles.certainBtn}
                btnColors={gradientColor}
                title={'確 定'}
                onPress={()=> this.onPressCertenBtn()}/>
          {this.renderToggleReceiptBtn()}
          <Toast ref="toast"/>
        </View>
      </TouchableWithoutFeedback>
    );
  }

  setReceiptMoney(input) {
    if(input.startsWith('0')) {
      this.refs.toast.show(`請輸入開頭不為 0 的正整數`)
      return
    }
    if(!this.checkPositiveInteger(input)) {
      this.refs.toast.show(`請輸入數字`)
      return
    }
    this.props.actions.setReceiptMoney(input)
  }

  renderToggleReceiptBtn() {
    return (
      <View style={[styles.toogleReceiptBtnWrapper,{bottom:Platform.OS === 'ios' ? this.state.keyboardHeight : 0}]}>
        <ToggleModeBtn wrapperStyle={{flex: 1}}
          onPress={() => this.toggleReceiptMode(RECEIPT_MODE.CUSTOMER_BE_SCANNED)}
          textColor={this.props.mode === RECEIPT_MODE.CUSTOMER_SCAN ?
            commonColor.darkGrayColor : commonColor.customerBeScanned }
          imgPath={this.props.mode === RECEIPT_MODE.CUSTOMER_SCAN ?  'icon_receipt_scan_false' : 'icon_receipt_scan_true'}
          text={'掃碼收款'} />
        <View style={{width: 5}} />
        <ToggleModeBtn wrapperStyle={{flex: 1}}
          onPress={() => this.toggleReceiptMode(RECEIPT_MODE.CUSTOMER_SCAN)}
          textColor={this.props.mode === RECEIPT_MODE.CUSTOMER_SCAN ?
            commonColor.customerScan : commonColor.darkGrayColor}
          imgPath={this.props.mode === RECEIPT_MODE.CUSTOMER_SCAN ? 'icon_receipt_create_true' : 'icon_receipt_create_false'}
          text={'收錢碼'} />
      </View>
    );
  }

  onPressCertenBtn() {
    if (this.props.money.length <= 0) {
      this.refs.toast.show(`請填入消費金額`)
      return;
    }

    if (this.checkPositiveInteger(this.props.money)) {
      const { navigate } = this.props.navigation;
      this.props.actions.setTradeResult('', '', '');
      navigate('ReceiptPage', { inputTextReFoucs: () => this.inputTextReFoucs() });
      this.input.blur();
    } else {
      this.refs.toast.show(`請輸入正整數`)
    }
  }

  checkPositiveInteger(str) {
    return /^[0-9]*$/.test(str);
  }

  inputTextReFoucs() {
    this.props.actions.setReceiptMoney('');
    if (this.input)
      this.input.focus();
  }

  toggleReceiptMode(receiptMode) {
    this.props.actions.toggleReceiptMode(receiptMode);
  }

}

export default connect(
  state => ({
    mode: state.receipt.mode,
    money: state.receipt.money,
    storeName: state.settings.storeName,
  }),
  (dispatch) => ({
    actions: bindActionCreators(receiptActions, dispatch)
  })
)(ReceiptMoneyPage);


const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 30 : 0,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  spaceKeyboardShow: {
    height: windowH * 5/19 /3,
  },
  spaceKeyboardHide: {
    height: (windowH - toolbarHeight - toggleModeBtnHeight - 30) * 1/4.5,
  },
  certainBtn: {
    marginTop: 30,
  },
  toogleReceiptBtnWrapper: {
    flexDirection: 'row',
    height: toggleModeBtnHeight,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
  },
  storeNameSpace: {
    flex: 1, 
    alignItems:'center', 
    backgroundColor: '#ededed',
    justifyContent: 'center'
  },
  storeNameUnderLine: {
    height: 1,
    width: windowW, 
  },
  storeName: {
    color: commonColor.darkGrayColor,
    fontSize: 20 *fontSizeScaler,
    paddingBottom: 1,
  }
});
