import React, {Component} from 'react'
import { StyleSheet, Platform, Alert,View, Text, Image, FlatList, TextInput } from 'react-native'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { FormLabel, FormInput } from 'react-native-elements'
import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import { commonColor,windowH, windowW, fontSizeScaler, } from '../commonStyles'
import { goHome, forIphoneXStyleByOrientation } from '../lib/functions'
import * as easycardActions from '../actions/easycardActions'
import NormalTradeResultAlert from '../components/ezcardPage/NormalTradeResultAlert'
import Toolbar from '../components/Toolbar'
import MainButton from '../components/MainButton'
import LoadingAlert from '../components/alert/TitleWithOneBtn'


class EZCardSettingConfig extends Component {
    constructor() {
        super()
        this.state = {
            iphoneXstyle: forIphoneXStyleByOrientation(),
            ssidValue:'',
            encryptionValue:'2', // 初始直 WAP2
            passwordValue:'',
            ssidValueFocus:[styles.formInputStyle], // SSID樣式
            passwordValueFocus:[styles.formInputStyle], // password樣式
            isWifiAlertShow : false,
            wifiRadio : [
                {label:'None', value:0},
                {label:'WEP', value:1},
                {label:'WPA', value:2},
                {label:'WPA2',value:2}],
            
        }
    }
    renderToolBar() {
        return (
          <Toolbar 
            title = {'悠遊卡'}
            backgroundImg={'bg_toolbar_create'}
            backOnPress={() => this.props.navigation.goBack()}
            homeOnPress={() => goHome(this.props.navigation)}
             />
        )
    }
    renderTitle(){
        return(
            <Text style={styles.title}>悠遊卡機設定</Text>
        )
    }
    renderInput(){
        return(
            <View style={styles.container }>
                <View style={{width:'100%'}}>
                    <FormLabel labelStyle={styles.inputTitle}>SSID</FormLabel>
                    <FormInput
                        inputStyle={this.state.ssidValueFocus}
                        onChangeText={(text)=>{this.setState({ssidValue:text})}}
                        value={this.state.ssidValue}
                        onFocus={()=>{
                            this.setState({ssidValueFocus: [styles.formInputStyle , styles.textInputFocus]})
                        }}
                        onBlur={()=>{
                        this.setState({ssidValueFocus: styles.formInputStyle })
                        }}
                    />
                </View>
                <View style={{width:'100%'}}>
                    <FormLabel labelStyle={styles.inputTitle}>Password</FormLabel>
                    <FormInput
                        inputStyle={this.state.passwordValueFocus}
                        onChangeText={(text)=>{this.setState({passwordValue:text})}}
                        value={this.state.passwordValue}
                        onFocus={()=>{
                            this.setState({passwordValueFocus: [styles.formInputStyle , styles.textInputFocus]})
                        }}
                        onBlur={()=>{
                        this.setState({passwordValueFocus: styles.formInputStyle })
                        }}
                    />
                </View>
                <View >
                    <RadioForm
                        radio_props={this.state.wifiRadio}
                        initial={3}
                        buttonSize={20}
                        buttonOuterSize={35}
                        style={styles.radioStyle}
                        labelStyle={{fontSize:18}}
                        onPress={(value) => {this.setState({encryptionValue:value})}}
                        />
                </View>
            </View>
        )
    }
    renderWifiRes() {
        normalAlertImg = 'icon_receipt_result_success';
        let valueMap = new Map()
        valueMap.set("請查看裝置綠色燈號")
        valueMap.set("等待閃爍轉為恆亮狀態,即完成設定")
        valueMap.set("若燈號持續閃爍,請檢查無線分享器是否以開機並正常工作")
        valueMap.set("請重新設定,並確認SSID和密碼輸入是否正確")
        return (
            <NormalTradeResultAlert 
                onPress = {()=>this.closeRenderWifiRest()}
                imgPath = {normalAlertImg}
                title = {"Wifi設定已發送完成"}
                contentStyle={{width:windowW, height: windowH}}
                containerStyle={{width:windowW, height: windowH,left:0}}
                rowStyle={styles.rowStyle}
                rowValueStyle = {styles.rowValueStyle}
                map = {valueMap}
            />
        )
    }
    closeRenderWifiRest() {
        this.setState({
            isWifiAlertShow:false
        })
    }
    render() {
        return (
            <View style={[styles.wrapper, this.state.iphoneXstyle]}>
                {this.renderToolBar()}
                {this.renderTitle()}
                {this.renderInput()}
                <MainButton
                    style={styles.certainButton}
                    btnColors={commonColor.createGradient}
                    title={'確 定'}
                    onPress={()=> this.onPressCertenBtn()} />
                {this.state.isWifiAlertShow ? this.renderWifiRes() : null}
            </View>
        )
    }
    onPressCertenBtn(){
        try{
            
            let wifiParamsTitle = "__SL_P_ULD="
            let ssid = decodeURIComponent(this.state.ssidValue.trim());
            let password = decodeURIComponent(this.state.passwordValue.trim())
            let encryptionValue = this.state.encryptionValue
            params = wifiParamsTitle 
            params += this.getLengthCode(this.lengthInUtf8Bytes(ssid)) + ssid
            params += this.getLengthCode(this.lengthInUtf8Bytes(password)) + password
            params += encryptionValue
            this.setWifi(params)
        }catch(e){
            console.log(e)
        }
        
    }
    getLengthCode(ln){
        if(ln > 30){
            throw "輸入長度過長" 
        }
        var codes = ['0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'];
        return codes[ln];
    }
    lengthInUtf8Bytes(str) {
        // Matches only the 10.. bytes that are non-initial characters in a multi-byte sequence.
        var m = encodeURIComponent(str).match(/%[89ABab]/g);
        return str.length + (m ? m.length : 0);
    }
   async setWifi(params){
        // Alert.alert("请求開始" , params)
        try{
            let response = await fetch("http://192.168.1.1", {
                method: 'POST',
                headers: {
                  'Content-Type': 'application/x-www-form-urlencoded',
                },
                body:params
            })
            this.setState({isWifiAlertShow:true});
            return
          
        }catch(e){

        }

    }
}
export default connect(
    state => ({
      merchId: state.settings.merchantId,
      tradeKey: state.settings.tradeKey,
      isFetching: state.easycard.isFetching,
      serviceType: state.easycard.serviceType,
      fetchResponse: state.easycard.fetchResponse,
      easycardDevice: state.easycard.easycardDevice,
    }),
    (dispatch) => ({
      actions: bindActionCreators(easycardActions, dispatch)
    })
)(EZCardSettingConfig)
const textSize = 16 * fontSizeScaler
const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        marginTop: Platform.OS === 'ios'? 30 : 0,
        backgroundColor: 'white',
        alignItems: 'center',
       
    },
    title:{
        width: windowW,
        fontSize: 25 *fontSizeScaler,
        marginTop:15,
        color: '#442215',
        fontWeight:'300',
        textAlign: 'center'
    },
    container:{
        flexDirection: 'row',
        alignItems: 'center', 
        flexWrap: "wrap",
        marginTop: 15,
        width: '90%'
       
    },
    inputTitle:{
        width: windowW,
        fontSize: 20 *fontSizeScaler,
        color: '#333'
    },
    formInputStyle:{
        borderWidth:0,  
        borderColor:'#9098A9' ,
        color:'#9098a9',
        width: '100%',
        height: windowH / 15,
        fontSize: 16 *fontSizeScaler,
        borderBottomWidth:3,  
    },
    textInputFocus:{
        borderBottomColor:'#0077ff',
        color:'#0077ff'
    },
    certainButton:{
        marginTop: 30
    },
    radioStyle: {
        height: windowH *2 / 8,
        marginTop:25  ,
        marginLeft:15, 
        alignItems:'flex-start' ,
        justifyContent:'space-between' 
    },
    rowStyle: {
        fontSize: textSize,
        flex: 10,
        textAlign: 'left',
        marginBottom: 7
    },
    rowValueStyle:{
        flex:0
    }
})