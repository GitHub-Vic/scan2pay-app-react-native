import React, { Component } from 'react'
import {
  StyleSheet,
  Platform,
  View,
  Text,
  TouchableHighlight,
  Image
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as recordActions from '../actions/recordActions'

import { SERVICE_TYPE, TRADE_RESULT_TYPE } from '../constants/actionTypes'
import {
  goHome,
  getNowTime,
  getValueByKeyFromJsonObject,
  showDateTime,
  dateFormat
} from '../lib/functions'
import {
  commonColor,
  commonStyles,
  windowW,
  toolbarHeight,
  fontSizeScaler
} from '../commonStyles'
import * as formatFunctions from '../lib/formatFunctionsForShow'
import { SHA256 } from '../lib/SHA256'

//request
import {
  forIphoneXStyleByOrientation,
  consoleForDebug
} from '../lib/functions'

import Toast from '../components/Toast'
import Toolbar from '../components/Toolbar'
import LinearGradient from 'react-native-linear-gradient'
import LoadingAlert from '../components/alert/TitleWithOneBtn'
import DatePicker from '../components/alert/datePicker/'
import RecordList from '../components/recordPage/RecordList'
import RecordDetailView from '../components/recordPage/RecordDetailView'
import RefundAlert from '../components/recordPage/RefundAlert'
import TradeResult from '../components/alert/TradeResult'
import SearchBar from '../components/recordPage/SearchBar'
import MainButton from '../components/MainButton'

const views = {
  chooseDate: 'isShowChooseDate',
  recordList: 'isShowRecordList',
  recordDetail: 'isShowRecordDetail'
}
const datePickerTypes = {
  startDate: 'startDate',
  endDate: 'endDate'
}

class RecordPage extends Component {
  constructor() {
    super()
    this.state = {
      iphoneXstyle: forIphoneXStyleByOrientation(),
      toolbarTitle: '交易紀錄',
      startDate: '',
      endDate: '',
      [views.chooseDate]: true,
      isShowLoadingAlert: false,
      alertTitle: '',
      [views.recordList]: false,
      [views.recordDetail]: false,
      nowListRecords: [],
      detailItemIndex: -1,
      isShowRefundAlert: false,
      isShowTradeResultAlert: false,
      tradeResultMsg: '',
      tradeResultImg: '',
      isShowSearchBar: false,
      isAdvancedSearch: false,
      advancedSearchContent: '',
      isShowDatePicker: false,
      datePickerType: '',
      isShowErrorAlert: false,
      errorImg: 'icon_receipt_result_fail',
      errorMsg: '',
    }
  }

  componentDidMount() {
    this.setState({
      startDate: getNowTime(),
      endDate: getNowTime()
    })
  }

  componentWillReceiveProps(nextProps) {
    var nextFetchResponse = nextProps.record.fetchResponse
    if (nextFetchResponse !== this.props.fetchResponse) {
      this.parserResponse(nextFetchResponse)
    }

    //for modify item's content after refund(cancel) success (Record Detail)
    if (this.state.isAdvancedSearch) {
      var nextRecord = nextProps.record.record
      if (nextRecord !== this.props.record.record) {
        const data = nextRecord.filter(r =>
          null === r['systemOrderId']
            ? false
            : r['systemOrderId'].endsWith(this.state.advancedSearchContent)
        )
        this.setState({
          nowListRecords: data
        })
      }
    }
  }

  render() {
    return (
      <View style={[styles.wrapper, this.state.iphoneXstyle]}>
        {this.renderToolBar()}
        {this.renderSearchBar()}
        {this.state[views.chooseDate] ? this.renderChooseData() : null}
        {this.state[views.recordList] ? this.renderRecordList() : null}
        {this.state[views.recordDetail] ? this.renderRecordDetail() : null}
        {this.state.isShowLoadingAlert ? (
          <LoadingAlert title={this.state.alertTitle} />
        ) : null}
        {this.state.isShowRefundAlert ? this.renderRefundAlert() : null}
        {this.state.isShowTradeResultAlert
          ? this.renderTradeResultAlert()
          : null}
        {this.state.isShowErrorAlert
          ? this.renderErrorAlert()
          : null}
        {this.state.isShowDatePicker
          ? this.renderDatePicker(this.state.datePickerType)
          : null}
        <Toast ref="toast" />
      </View>
    )
  }

  componentWillUnmount() {
    this.props.actions.resetRecord()
  }

  renderToolBar() {
    var isRenderSearchIcon =
      this.props.record.record.length > 0 && !this.state[views.recordDetail]
    return (
      <Toolbar
        title={this.state.toolbarTitle}
        backgroundImg={'bg_toolbar_create'}
        backOnPress={() => this.pressBackBtn()}
        homeOnPress={() => goHome(this.props.navigation)}
        actionImgPath={isRenderSearchIcon ? 'icon_toolbar_search' : null}
        actionOnPress={() =>
          isRenderSearchIcon ? this.pressAdvacedSearch() : {}
        }
      />
    )
  }

  pressAdvacedSearch() {
    this.setState({
      isShowSearchBar: true,
      isAdvancedSearch: true,
      nowListRecords: this.props.record.record
    })
    try {
      this.OrderSearchInput.focus()
    } catch (e) {}
  }

  renderSearchBar() {
    var isRenderSearchBar =
      this.state.isShowSearchBar && !this.state[views.recordDetail]
    return (
      <SearchBar
        isShow={isRenderSearchBar}
        textInputRef={ref => (this.OrderSearchInput = ref)}
        onTextChange={text => this.advancedSearch(text)}
        value={this.state.advancedSearchContent}
        cancelOnPress={() => this.cancelAdvancedSearch()}
      />
    )
  }

  advancedSearch(text) {
    const data = this.props.record.record.filter(r =>
      null === r['systemOrderId'] ? false : r['systemOrderId'].endsWith(text)
    )
    this.setState({
      nowListRecords: data,
      advancedSearchContent: text
    })
  }

  fetchRecord() {
    if (this.props.record.noData | this.props.record.isEnd) {
      return
    }

    if (this.props.record.isFetching) {
      return
    }

    if (this.state.isAdvancedSearch) {
      return
    }

    // if(this.props.record.nowPage === 0) {
    this.setState({ alertTitle: '搜尋紀錄中', isShowLoadingAlert: true })
    // }

    var requestData = new Map()
    requestData.set('Method', '00000')
    requestData.set('MchId', this.props.merchId)
    requestData.set('CreateTime', getNowTime())
    requestData.set('ServiceType', SERVICE_TYPE.PAGE_QUERY)
    requestData.set('Index', '' + this.props.record.nowPage)
    requestData.set('PageSize', this.props.record.pageSize)
    requestData.set('StartDate', this.state.startDate.substr(0, 8) + '000000') //"20171001000000"
    requestData.set('EndDate', this.state.endDate.substr(0, 8) + '235959') //"20171127235959"
    requestData.set('TradeKey', this.props.tradeKey)
    this.props.actions.fetchAPI(requestData)
  }

  refund(refundKey) {
    const item = this.state.isAdvancedSearch
      ? this.state.nowListRecords[this.state.detailItemIndex]
      : this.props.record.record[this.state.detailItemIndex]
    var method = getValueByKeyFromJsonObject(item, 'method')

    const ownerDeviceId =
      (item.deviceRandomId && item.deviceRandomId.ownerDeviceId) || ''

    var requestData = new Map()
    if (method.startsWith('2')) {
      requestData.set('ServiceType', SERVICE_TYPE.CANCEL)
      requestData.set(
        'SystemOrderId',
        getValueByKeyFromJsonObject(item, 'systemOrderId')
      )
      requestData.set(
        'StoreInfo',
        getValueByKeyFromJsonObject(item, 'storeInfo')
      )
      requestData.set('Cashier', getValueByKeyFromJsonObject(item, 'cashier'))
    } else {
      requestData.set('ServiceType', SERVICE_TYPE.REFUND)
      requestData.set(
        'StoreRefundNo',
        `r${getValueByKeyFromJsonObject(item, 'systemOrderId')}`
      )
      requestData.set(
        'TotalFee',
        '' + getValueByKeyFromJsonObject(item, 'payment')
      )
      requestData.set('RefundFeeType', 'TWD')
      requestData.set(
        'RefundFee',
        '' + getValueByKeyFromJsonObject(item, 'payment')
      )
    }
    requestData.set('Method', method)
    requestData.set('MchId', this.props.merchId)
    requestData.set('CreateTime', getNowTime())
    requestData.set(
      'StoreOrderNo',
      getValueByKeyFromJsonObject(item, 'orderId')
    )
    requestData.set('DeviceInfo', ownerDeviceId)
    requestData.set('RefundKey', SHA256(refundKey))
    requestData.set('TradeKey', this.props.tradeKey)

    this.props.actions.fetchAPI(requestData)
  }

  parserResponse(msg) {
    //避免reset
    if (msg.length !== 0) {
      try {
        var js = JSON.parse(msg)
        if (js.Header.StatusCode === '8002') {
          //response not has ServiceType .. don't know what reaction.. so just toast
          this.setState({
            isShowRefundAlert: false,
            isShowLoadingAlert: false
          })
          this.showRefundResult(
            false,
            `${js.Header.StatusCode}-${js.Header.StatusDesc}
          ${'\n'}請重新嘗試`
          )
          // this.refs.toast.show(`${js.Header.StatusCode}-${js.Header.StatusDesc}
          //   ${'\n'}請重新嘗試`)
          return
        }
        if (
          js.Header.ServiceType &&
          js.Header.ServiceType === SERVICE_TYPE.PAGE_QUERY
        ) {
          this.parserPageQuery(msg)
        } else {
          this.parserRefundResponse(msg)
        }
      } catch (e) {
        this.otherError(msg)
      }
    }
  }

  otherError (msg) {
    this.setState({
      isShowLoadingAlert: false,
      isShowErrorAlert: true,
      errorMsg: msg
    })
  }

  parserPageQuery(msg) {
    let json = JSON.parse(msg)
    this.setState({ isShowLoadingAlert: false })
    if (this.props.record.nowPage === 0) {
      this.showWitchView(views.recordList)
    }
    try {
      switch (json.Header.StatusCode) {
        case '0000':
          let list = JSON.parse(json.Data.list)
          if (list.length === 0) {
            if (this.props.record.nowPage === 0)
              this.props.actions.setRecordNoData()
            else this.props.actions.setRecordIsEnd()
          } else if (
            0 < list.length &&
            list.length < this.props.record.pageSize
          ) {
            this.props.actions.setRecordIsEnd()
          }
          this.props.actions.setRecord(this.props.record.nowPage, list)
          break

        default:
          this.error(
            `parserPageQuery StatusCode: ${json.Header.StatusCode}-${
              json.Header.StatusDesc
            }`
          )
          break
      }
    } catch (e) {
      this.error(`parserPageQuery Error:\n${e}`)
    }
  }

  parserRefundResponse(msg) {
    let js = JSON.parse(msg)
    var des = ''
    try {
      des = `${js.Header.StatusCode}-${js.Header.StatusDesc}`
      switch (js.Header.StatusCode) {
        case '0000':
          this.props.actions.setOrderRefund(
            js.Data.StoreOrderNo,
            js.Header.ServiceType,
            getNowTime()
          )
          this.showRefundResult(true, des)
          break

        case '7112':
        case '7001':
          if (js.Data.StoreOrderNo == undefined) {
            var refundItem = this.state.isAdvancedSearch
              ? this.state.nowListRecords[this.state.detailItemIndex]
              : this.props.record.record[this.state.detailItemIndex]
            this.props.actions.setOrderRefund(
              refundItem.orderId,
              formatFunctions.REFUND_STATUS_REFUND_SUCCESS,
              getNowTime()
            )
          } else
            this.props.actions.setOrderRefund(
              js.Data.StoreOrderNo,
              js.Header.ServiceType,
              getNowTime()
            )
          this.showRefundResult(false, des)
          break

        default:
          this.showRefundResult(false, des)
          break
      }
      return
    } catch (e) {
      consoleForDebug(`parserRefundResponse Error Catch:\n${e}`)
    }

    this.showRefundResult(false, '未知錯誤，請再嘗試一次')
  }

  error(errMsg) {
    this.setState({ isShowLoadingAlert: false })
    this.showRefundResult(false, errMsg)
    // this.refs.toast.show(errMsg)
  }

  renderChooseData() {
    return (
      <View style={styles.contentWrapper}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            paddingBottom: 10
          }}
        >
          <Image
            style={styles.calendarImg}
            source={{ uri: 'icon_record_calendar' }}
          />
          <Text style={styles.chooseDateTitle}>請選擇查詢日期</Text>
        </View>

        <TouchableHighlight
          onPress={() =>
            this.setState({
              isShowDatePicker: true,
              datePickerType: datePickerTypes.startDate
            })
          }
          underlayColor={commonColor.btnClickColor}
        >
          <View style={styles.row}>
            <Text style={styles.chooseDateKey}>{`開始日期`}</Text>
            <Text style={styles.chooseDateValue}>{`${showDateTime(
              this.state.startDate
            )}`}</Text>
          </View>
        </TouchableHighlight>
        <LinearGradient
          style={styles.underLine}
          start={{ x: 0.0, y: 0.5 }}
          end={{ x: 1, y: 0.5 }}
          colors={commonColor.createGradient}
        />

        <TouchableHighlight
          onPress={() =>
            this.setState({
              isShowDatePicker: true,
              datePickerType: datePickerTypes.endDate
            })
          }
          underlayColor={commonColor.btnClickColor}
        >
          <View style={styles.row}>
            <Text style={styles.chooseDateKey}>{`結束日期`}</Text>
            <Text style={styles.chooseDateValue}>{`${showDateTime(
              this.state.endDate
            )}`}</Text>
          </View>
        </TouchableHighlight>
        <LinearGradient
          style={styles.underLine}
          start={{ x: 0.0, y: 0.5 }}
          end={{ x: 1, y: 0.5 }}
          colors={commonColor.createGradient}
        />

        <View style={{ height: 30 }} />
        <MainButton
          style={styles.btn}
          btnColors={commonColor.createGradient}
          title={'查 詢'}
          onPress={() => this.fetchRecord()}
        />
      </View>
    )
  }

  renderDatePicker() {
    return (
      <View
        style={[
          commonStyles.alertBackgroundView,
          { alignItems: 'center', justifyContent: 'center' }
        ]}
      >
        {this.state.datePickerType === datePickerTypes.startDate ? (
          <DatePicker
            certain={newDate =>
              this.setState({
                startDate: dateFormat(newDate),
                isShowDatePicker: false
              })
            }
            cancel={() => this.setState({ isShowDatePicker: false })}
            date={this.state.startDate}
            maxDate={this.state.endDate}
          />
        ) : (
          <DatePicker
            certain={newDate =>
              this.setState({
                endDate: dateFormat(newDate),
                isShowDatePicker: false
              })
            }
            cancel={() => this.setState({ isShowDatePicker: false })}
            date={this.state.endDate}
            minDate={this.state.startDate}
          />
        )}
      </View>
    )
  }

  renderRecordList() {
    if (this.props.record.noData) {
      return (
        <View style={styles.secondViewShow}>
          <Text>查無任何交易紀錄</Text>
        </View>
      )
    } else {
      return (
        <View style={styles.secondViewShow}>
          <RecordList
            data={
              this.state.isAdvancedSearch
                ? this.state.nowListRecords
                : this.props.record.record
            }
            isEnd={this.props.record.isEnd}
            isAdvancedSearch={this.state.isAdvancedSearch}
            fecthMore={() => this.fetchRecord()}
            onPress={index => this.onItemPress(index)}
          />
        </View>
      )
    }
  }

  onItemPress(index) {
    this.setState({
      toolbarTitle: '交易紀錄明細',
      [views.recordDetail]: true,
      detailItemIndex: index
    })
  }

  renderRecordDetail() {
    return (
      <View style={[styles.secondViewShow, { backgroundColor: 'whitesmoke' }]}>
        <RecordDetailView
          item={
            this.state.isAdvancedSearch
              ? this.state.nowListRecords[this.state.detailItemIndex]
              : this.props.record.record[this.state.detailItemIndex]
          }
          onPress={item => this.showRefundAlert(item)}
        />
      </View>
    )
  }

  showRefundAlert() {
    this.setState({ isShowRefundAlert: true, isShowLoadingAlert: false })
  }

  showRefundResult(isSuccess, msg) {
    this.setState({
      isShowRefundAlert: false,
      isShowLoadingAlert: false,
      isShowTradeResultAlert: true,
      tradeResultMsg: msg,
      tradeResultImg: isSuccess
        ? TRADE_RESULT_TYPE.SUCCESS
        : TRADE_RESULT_TYPE.FAIL
    })
  }

  renderRefundAlert() {
    var item = this.props.record.record[this.state.detailItemIndex]
    return (
      <RefundAlert
        onRefundPress={refundKey => this.pressRefundBtn(refundKey)}
        onCancelPress={() => this.closeRefundAlert()}
        systemOrderId={formatFunctions.formatSystemOrderId(
          getValueByKeyFromJsonObject(item, 'systemOrderId')
        )}
        orderId={formatFunctions.formatSystemOrderId(
          getValueByKeyFromJsonObject(item, 'orderId')
        )}
      />
    )
  }

  renderTradeResultAlert() {
    var tradeResultImg = this.state.tradeResultImg
    var imgPath
    var title
    if (tradeResultImg === TRADE_RESULT_TYPE.SUCCESS) {
      imgPath = 'icon_receipt_result_success'
      title = '退款成功'
    } else if (tradeResultImg === TRADE_RESULT_TYPE.ALERT) {
      imgPath = 'icon_receipt_result_alert'
      title = '退款失敗'
    } else {
      imgPath = 'icon_receipt_result_fail'
      title = '退款失敗'
    }
    return (
      <TradeResult
        imgPath={imgPath}
        title={title}
        msg={
          tradeResultImg === TRADE_RESULT_TYPE.SUCCESS
            ? ''
            : this.state.tradeResultMsg
        }
        onPress={() => this.setState({ isShowTradeResultAlert: false })}
      />
    )
  }

  renderErrorAlert() {
    return (
      <TradeResult
        imgPath={this.state.errorImg}
        title={'失敗'}
        msg={ this.state.errorMsg }
        onPress={() => this.setState({ isShowErrorAlert: false })}
      />
    )
  }

  showWitchView(whichView) {
    this.setState({
      toolbarTitle: '交易紀錄',
      [views.chooseDate]: false,
      [views.recordList]: false,
      [views.recordDetail]: false,
      [whichView]: true,
      detailItemIndex: -1
    })
  }

  pressBackBtn() {
    if (this.state[views.recordDetail]) {
      this.showWitchView(views.recordList)
      return
    } else if (this.state[views.recordList]) {
      if (this.state.isAdvancedSearch) {
        this.cancelAdvancedSearch()
        return
      }
      this.showWitchView(views.chooseDate)
      this.props.actions.resetRecord()
      return
    } else {
      this.props.navigation.goBack()
      return
    }
  }

  closeRefundAlert() {
    this.setState({
      isShowRefundAlert: false,
      isShowLoadingAlert: false,
      alertTitle: ''
    })
  }

  pressRefundBtn(refundKey) {
    this.setState({
      isShowRefundAlert: false,
      isShowLoadingAlert: true,
      alertTitle: '退款中'
    })
    this.refund(refundKey)
  }

  cancelAdvancedSearch() {
    this.setState({
      nowListRecords: [],
      isShowSearchBar: false,
      isAdvancedSearch: false,
      advancedSearchContent: ''
    })
    this.OrderSearchInput.blur()
  }
}

export default connect(
  state => ({
    merchId: state.settings.merchantId,
    tradeKey: state.settings.tradeKey,
    record: state.record,
    fetchResponse: state.record.fetchResponse
  }),
  dispatch => ({
    actions: bindActionCreators(recordActions, dispatch)
  })
)(RecordPage)

const wid = (windowW * 2) / 3
const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 30 : 0,
    backgroundColor: 'white'
  },
  contentWrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  viewWrapper: {
    flex: 1,
    width: windowW
  },
  row: {
    flexDirection: 'row',
    width: wid,
    paddingTop: 25,
    paddingBottom: 2
  },
  btn: {
    marginTop: 10
  },
  btnText: {
    color: 'white',
    fontSize: 25
  },
  secondViewShow: {
    position: 'absolute',
    top: toolbarHeight,
    right: 0,
    left: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  calendarImg: {
    resizeMode: 'contain',
    height: 50,
    width: 50
  },
  chooseDateTitle: {
    fontSize: 22 * fontSizeScaler,
    paddingLeft: 5,
    color: commonColor.darkGrayColor
  },
  chooseDateKey: {
    flex: 1,
    fontSize: 17 * fontSizeScaler,
    textAlign: 'left',
    color: commonColor.darkGrayColor
  },
  chooseDateValue: {
    flex: 1.2,
    fontSize: 17 * fontSizeScaler,
    textAlign: 'right',
    color: '#999799'
  },
  underLine: {
    height: 1,
    width: wid
  }
})
