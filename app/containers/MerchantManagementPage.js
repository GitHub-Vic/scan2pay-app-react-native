import React, { Component } from 'react'
import {
  StyleSheet,
  Platform,
  View,
  Text,
  Alert,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as merchantActions from '../actions/merchantActions'
import * as settingsActions from '../actions/settingsActions'
import { Icon } from 'react-native-elements'

import {
  goHome,
  mapToJsonString,
  httpsPost,
} from '../lib/functions'
import {
  commonColor,
  windowW,
  toolbarHeight,
  fontSizeScaler
} from '../commonStyles'
import {
  forIphoneXStyleByOrientation,
} from '../lib/functions'
import Toast from '../components/Toast'
import Toolbar from '../components/Toolbar'
import InsertDialog from '../components/merchantManagementPage/InsertDialog'
import LoadingAlert from '../components/alert/TitleWithOneBtn'
import MerchantList from '../components/merchantManagementPage/MerchantList'
import TradeResult from '../components/alert/TradeResult'
import {URL_INTELLA_DOMAIN, URL_LOGIN, APP_ID, PLATFORM, URL_LOGOUT } from '../config'
import appActions from '../actions'
import store from '../store'
import mqtt from '../lib/mqtt'
import apnsToken from '../lib/apnsToken'

const views = {
  chooseDate: 'isShowChooseDate',
  merchantList: 'isShowMerchantList',
  merchantDetail: 'isShowMerchantDetail'
}

class MerchantPage extends Component {
  constructor() {
    super()
    this.state = {
      iphoneXstyle: forIphoneXStyleByOrientation(),
      toolbarTitle: '特店管理',
      isShowInsertDialog: false,
      showResult: false,
      isShowLoadingAlert: false
    }
    this.login = this.login.bind(this)
  }

  componentWillReceiveProps (nextProps) {
    if (nextProps.merchants !== this.props.merchants && nextProps.merchants.length === 0) {
      this.props.actions.login('', '', '','')
      mqtt().disConnectForLogout()
      store.dispatch(appActions.easycardActions.setEasycardDevice(''))
      this.logout()
    }
  }

  componentDidMount() {
    this.fetchMerchant()
  }

  render() {
    return (
      <View style={[styles.wrapper, this.state.iphoneXstyle]}>
        {this.renderToolBar()}
        {this.renderMerchantList()}
        {
          this.state.showResult ?
          <TradeResult
            imgPath={'icon_receipt_result_fail'}
            title={this.state.resultTitle}
            msg={
              this.state.resultMsg
            }
            onPress={() => this.setState({ showResult: false })}
            style={{zIndex: 100}}
          />
          : null
        }
        {this.props.isFetching ? (
          <LoadingAlert title={this.props.isFetchingTitle} />
        ) : null}
        {this.state.isShowInsertDialog ? (
          <InsertDialog 
            title={'this.state.alertTitle'}
            closeInsertDialog={() => {this.setState({isShowInsertDialog: false})}} 
            insertMerchant={(data) => this.insertMerchant(data)}/>
        ) : null}
        <Toast ref="toast" />
        <View style={styles.bottom}>
          <Icon
            name='plus-circle'
            type='font-awesome'
            size={100}
            color='#80B34F'
            onPress={() => this.setState({isShowInsertDialog: true})} />
        </View>
        {this.state.isShowLoadingAlert ? <LoadingAlert title={'登入中'} /> : null}
      </View>
    )
  }

  renderToolBar() {
    return (
      <Toolbar
        title={this.state.toolbarTitle}
        backgroundImg={'bg_toolbar_create'}
        backOnPress={() => this.pressBackBtn()}
        homeOnPress={() => goHome(this.props.navigation)}
      />
    )
  }

  deleteMerchant (index) {
    Alert.alert(
      '確定要刪除？',
      '',
      [
        {
          text: '取消',
          onPress: () => {},
          style: 'cancel',
        },
        {text: '確定', onPress: () => this.doDeleteUserMerchant(index)},
      ],
      {cancelable: false},
    )
  }
  
  updateDefaultMerchant (index) {
    Alert.alert(
      '確定要設為預設？',
      '',
      [
        {
          text: '取消',
          onPress: () => {},
          style: 'cancel',
        },
        {text: '確定', onPress: () => this.doUpdateUserMerchant(index)},
      ],
      {cancelable: false},
    )
  }

  doUpdateUserMerchant (index) {
    var requestData = new Map()
    const {userId, merchantSeqId}  = this.props.merchants[index]
    requestData.set('userId', userId)
    requestData.set('merchantSeqId', merchantSeqId)
    requestData.set('isDefault', '1')
    this.props.merchActions.updateUserMerchant(`${URL_INTELLA_DOMAIN}userMerchant/update` ,requestData, {successFunction: (accountId) => this.login(accountId), failFunction: (obj) => this.showResult(obj)})
  }

  doDeleteUserMerchant (index) {
    var requestData = new Map()
    const {userId, merchantSeqId}  = this.props.merchants[index]
    console.log(userId, merchantSeqId, index)
    requestData.set('userId', userId)
    requestData.set('merchantSeqId', merchantSeqId)
    this.props.merchActions.deleteUserMerchant(`${URL_INTELLA_DOMAIN}userMerchant/delete` ,requestData, {successFunction: (accountId) => this.login(accountId), failFunction: (obj) => this.showResult(obj)})
  }

  insertMerchant (data) {
    var requestData = new Map()
    requestData.set('userId', this.props.userId)
    requestData.set('accountId', data.accountId)
    requestData.set('pin', data.password)
    this.props.merchActions.insertUserMerchant(`${URL_INTELLA_DOMAIN}userMerchant/insert` ,requestData, {successFunction: (acccountId) => {
      this.setState({ isShowInsertDialog: false })
      this.login(acccountId)
    }, failFunction: (obj) => this.showResult(obj)})
  }

  showResult (obj) {
    const {resultTitle = '錯誤', resultMsg} = obj
    this.setState({
      resultTitle,
      resultMsg,
      showResult: true
    })
  }

  fetchMerchant() {
    var requestData = new Map()
    requestData.set('userId', this.props.userId)
    this.props.merchActions.getUserMerchantByUserId(`${URL_INTELLA_DOMAIN}userMerchant/userId` ,requestData, {successFunction: (accountId) => this.login(accountId), failFunction: (obj) => this.showResult(obj)})
  }

  renderMerchantList() {
    if (this.props.merchants.length === 0) {
      return (
        <View style={styles.secondViewShow}>
          <Text>查無任何特店</Text>
        </View>
      )
    } else {
      return (
        <View style={styles.secondViewShow}>
          <MerchantList
            data={
              this.props.merchants
            }
            onPress={index => this.deleteMerchant(index)}
            onPressDefault={index => this.updateDefaultMerchant(index)}
          />
        </View>
      )
    }
  }

  onItemPress(index) {
    this.setState({
      toolbarTitle: '新增特店',
      [views.merchantDetail]: true,
      detailItemIndex: index
    })
  }

  pressBackBtn() {
    this.props.navigation.goBack()
  }

  async logout () {
    this.props.actions.logout()
    var requestData = new Map()
    requestData.set("tokenId", this.props.tokenId)
    requestData.set("userId", this.props.userId)
    requestData.set("accountId", this.props.merchantId)
    requestData.set("serialId", this.props.udid)
    requestData.set("appId", APP_ID)
    requestData.set("platform", PLATFORM)
    let msg =  await httpsPost(URL_LOGOUT, mapToJsonString(requestData))
    try {
      var response = JSON.parse(msg)
    } catch (e) {
      console.log(e)
    }
  }

  async login(accountId) {
    var requestData = new Map()
    requestData.set("userId", this.props.userId)
    requestData.set("accountId", accountId)
    requestData.set('tokenId', this.props.tokenId)
    requestData.set("serialId", this.props.udid)
    requestData.set("appId", APP_ID)
    requestData.set("platform", PLATFORM)
    console.log('requestData merchant' , requestData)
    this.setState({
      isShowLoadingAlert: true,
    })
    let msg =  await httpsPost(URL_LOGIN, mapToJsonString(requestData))
    this.setState({
      isShowLoadingAlert: false,
    })
    var response = JSON.parse(msg)
    console.log(response)
    if(response.loggedIn) {
      this.loginSuccess(response.username, response.storeName, response.tradeKey,response.randomid)
    }else {
      this.setState({status: response.message})
    }
  }

  loginSuccess(merchId, storeName, tradeKey,randomid) {
    this.props.actions.login(merchId, storeName, tradeKey,randomid)
    store.dispatch(appActions.easycardActions.setEasycardDevice(''))
    mqtt().disConnectForLogout()
    mqtt().connectForLogin()
    Platform.OS === 'ios' ? apnsToken().register() : null
  }

}

export default connect(
  state => ({
    merchant: state.merchant,
    merchants: state.merchant.merchants,
    fetchResponse: state.merchant.fetchResponse,
    userId: state.merchant.userId,
    isFetching: state.merchant.isFetching,
    isFetchingTitle: state.merchant.isFetchingTitle,
    tokenId: state.merchant.tokenId,
    merchantId: state.settings.merchantId,
    account: state.settings.account,
    udid: state.settings.udid,
  }),
  dispatch => ({
    actions: bindActionCreators(settingsActions, dispatch),
    merchActions: bindActionCreators(merchantActions, dispatch)
  })
)(MerchantPage)

const wid = (windowW * 2) / 3
const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 30 : 0,
    backgroundColor: 'white'
  },
  contentWrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  viewWrapper: {
    flex: 1,
    width: windowW
  },
  row: {
    flexDirection: 'row',
    width: wid,
    paddingTop: 25,
    paddingBottom: 2
  },
  btn: {
    marginTop: 10
  },
  btnText: {
    color: 'white',
    fontSize: 25
  },
  secondViewShow: {
    position: 'absolute',
    top: toolbarHeight,
    right: 0,
    left: 0,
    bottom: 0,
    alignItems: 'center',
    justifyContent: 'center'
  },
  calendarImg: {
    resizeMode: 'contain',
    height: 50,
    width: 50
  },
  chooseDateTitle: {
    fontSize: 22 * fontSizeScaler,
    paddingLeft: 5,
    color: commonColor.darkGrayColor
  },
  chooseDateKey: {
    flex: 1,
    fontSize: 17 * fontSizeScaler,
    textAlign: 'left',
    color: commonColor.darkGrayColor
  },
  chooseDateValue: {
    flex: 1.2,
    fontSize: 17 * fontSizeScaler,
    textAlign: 'right',
    color: '#999799'
  },
  underLine: {
    height: 1,
    width: wid
  },
  bottom: {
    position: 'absolute',
    bottom: 30,
    right: 30
  }
})

