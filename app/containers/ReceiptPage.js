import React, { Component } from 'react'
import { StyleSheet, Platform, View, Text} from 'react-native'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

import { URL_INTELLA_GENERAL } from '../config'
import { goHome, getNowTime, mapToJsonString, forIphoneXStyleByOrientation, consoleForDebug } from '../lib/functions'
import { RECEIPT_MODE, SERVICE_TYPE, TRADE_RESULT_TYPE } from '../constants/actionTypes'
import * as receiptActions from '../actions/receiptActions'

import { commonColor, windowH, windowW, toggleModeBtnHeight, toolbarHeight, fontSizeScaler} from '../commonStyles'

import Toolbar from '../components/Toolbar'
import CustomerScan from '../components/receiptPage/CustomerScan'
import CustomerBeScanned from '../components/receiptPage/CustomerBeScanned'
import LoadingAlert from '../components/alert/TitleWithOneBtn'
import TradeResult from '../components/alert/TradeResult'
import DoubleCheckAlert from '../components/alert/TitleWithTwoBtn'

const singleOrderQueryDelaySec = 5;
const singleOrderQueryIntervalSec = 5;
const backOrGoHome = ['back', 'home'];

class ReceiptPage extends Component {

  constructor() {
    super();
    this.state = {
      iphoneXstyle: forIphoneXStyleByOrientation(),
      urlToken: '',
      customerScanNowStatus: '消費者尚未掃碼',
      isShowLoadingAlert: false,
      isShowTradeResultAlert: false,
      isShowDoubleCheckAlert: false,
      isStartCheckOrder: false,
      isTradeEnd: false,
      isBackOrGoHome: backOrGoHome[0],
      timerId: '',
      intervals: [],
    }
  }

  componentWillUnmount() {
    this.props.actions.resetTradeData();
    this.props.actions.resetTradeMoney();
    this.closeBackgroundThing();
  }

  componentWillReceiveProps (nextProps) {
    var nextReceipt = nextProps.receipt;
    
    if(this.state.isTradeEnd)
      return;

    try{
      if(nextReceipt.tradeResult.tradeStatus && nextReceipt.tradeResult.tradeStatus !== TRADE_RESULT_TYPE.ALERT){
        this.endTrade(nextReceipt.tradeResult.tradeStatus, nextReceipt.tradeResult.title, nextReceipt.tradeResult.msg);
        return;
      }
    }catch(e){
    }

    this.parserServerResponse(nextProps);

    // if (nextReceipt.orderId !== this.props.orderId) {
    //   if(nextReceipt.mode === RECEIPT_MODE.CUSTOMER_SCAN)
    //     this.generateQRCodeContent();
    // }

    try{
      if(nextReceipt[SERVICE_TYPE.CUSTOMER_SCAN].Data.urlToken != this.state.urlToken)
        this.setState({urlToken: nextReceipt[SERVICE_TYPE.CUSTOMER_SCAN].Data.urlToken});
    }catch(e){
      this.setState({urlToken: ''});
    }

  }

  render() {
    const title = isCustomerScanMode ? '收錢碼' : '掃碼收款' ;
    const isCustomerScanMode = this.props.mode === RECEIPT_MODE.CUSTOMER_SCAN;
    const majorColor = isCustomerScanMode ? commonColor.customerScan : commonColor.customerBeScanned ;
    const toolbarBackImg = this.props.mode === RECEIPT_MODE.CUSTOMER_SCAN ?
    'bg_toolbar_create' : 'bg_toolbar_scan';
    return (
      <View style={[styles.wrapper, this.state.iphoneXstyle]}>
        <Toolbar 
          title = { title }
          backgroundImg = { toolbarBackImg }
          backOnPress = {()=> this.toolbarBackOnPress()}
          homeOnPress = {()=> this.toolbarGoHomeOnPress()} />
        <View style={styles.moneyView} > 
          <View style={styles.storeNameSpace}>
            <Text style={styles.storeName} numberOfLines={1} >{this.props.storeName}</Text>
          </View>
          <Text style={[styles.storeNameUnderLine,{backgroundColor:majorColor}]} />
          <View style={{flex: 1, alignItems: 'center', justifyContent: 'flex-end'}}>
            <Text style={{fontSize: 23 *fontSizeScaler, color: majorColor}}>{`收款金額  NT$ ${this.props.money}`}</Text>
          </View>
        </View>
        <View style={styles.contentView} >
          {this.renderUiAccordingReceiptMode(isCustomerScanMode)}
        </View>
        {this.state.isShowLoadingAlert ? this.renderLoadingAlert() : null}
        {this.state.isShowDoubleCheckAlert ? this.renderDoublceCheckAlert() : null}
        {this.state.isShowTradeResultAlert ? this.renderTradeResultDialog() : null}
      </View>
    );
  }

  toolbarBackOnPress() {
    if(this.props.mode === RECEIPT_MODE.CUSTOMER_SCAN)
      this.tradeResultIsUnknow();
    else
      this.goBack();
  }

  toolbarGoHomeOnPress() {
    if(this.props.mode === RECEIPT_MODE.CUSTOMER_SCAN) {
      this.setState({isBackOrGoHome:backOrGoHome[1]});
      this.tradeResultIsUnknow();
    }else {
      goHome(this.props.navigation);
    }
  }

  goBack() {
    this.endTrade();
    try {
      this.props.navigation.state.params.inputTextReFoucs();
    } catch(e){
      consoleForDebug(`receiptPage inputTextReFoucs() Error:\n${e}`)
    }
    this.setState({
      isShowTradeResultAlert: false,
    });
    this.props.navigation.goBack();
  }

  // not use
  // toggleReceiptMode(isCustomerScanMode) {
  //   var toggleMode = isCustomerScanMode ? RECEIPT_MODE.CUSTOMER_BE_SCANNED : RECEIPT_MODE.CUSTOMER_SCAN;
  //   this.closeBackgroundThing();
  //   this.props.actions.resetTradeData();
  //   this.resetState();
  //   this.props.actions.toggleReceiptMode(toggleMode);
  // }

  renderUiAccordingReceiptMode(isCustomerScanMode) {
    if(isCustomerScanMode){
      return <CustomerScan 
        startLoading={()=> this.setState({isShowLoadingAlert: true}) }
        customerScanNowStatus={this.state.customerScanNowStatus}
        text={this.state.urlToken}
        {...this.props} />;
    }else{
      return <CustomerBeScanned 
        startLoading={()=> this.setState({isShowLoadingAlert: true}) }
        {...this.props} />;
    }
  }

  singleOrderQuery() {
    var requestData = new Map()
    requestData.set("Method", "00000")
    requestData.set("MchId", this.props.merchId)
    requestData.set("CreateTime", getNowTime())
    requestData.set("ServiceType", SERVICE_TYPE.SINGLE_ORDER_QUERY)
    requestData.set("StoreOrderNo", this.props.orderId)
    requestData.set("TradeKey", this.props.tradeKey)
    var json = mapToJsonString(requestData);
    this.props.actions.fetchIntellaApi(SERVICE_TYPE.SINGLE_ORDER_QUERY, URL_INTELLA_GENERAL, json);
  }

  renderLoadingAlert() {
    if(this.props.mode === RECEIPT_MODE.CUSTOMER_SCAN)
      return <LoadingAlert title={'產生條碼中'} />
    else
      return <LoadingAlert title={'交易等待中'} haveBtn={true} 
        onPress={()=> this.setState({isShowDoubleCheckAlert: true}) } 
        btnText={'關閉視窗'} />
  }

  renderDoublceCheckAlert() {
    return (
      <DoubleCheckAlert 
        title='是否關閉視窗？'
        cancelOnPress={()=> this.setState({isShowDoubleCheckAlert: false}) } 
        certainOnPress={()=> this.tradeResultIsUnknow() }
        defaultBtn={'cancel'}/>
    );
  }

  renderTradeResultDialog() {
    var imgPath;
    var msg;
    if(this.props.tradeResult.tradeStatus === TRADE_RESULT_TYPE.SUCCESS){
      imgPath = 'icon_receipt_result_success';
      msg = '';
    }else if(this.props.tradeResult.tradeStatus === TRADE_RESULT_TYPE.ALERT){
      imgPath = 'icon_receipt_result_alert';
      msg = this.props.tradeResult.msg;
    }else {
      imgPath = 'icon_receipt_result_fail';
      msg = this.props.tradeResult.msg;
    }
    return <TradeResult 
      imgPath={imgPath} 
      title={this.props.tradeResult.title} 
      msg={msg}
      onPress={()=> this.state.isBackOrGoHome == backOrGoHome[0] ? this.goBack() : goHome(this.props.navigation) }/>
  }

  tradeResultIsUnknow() {
    this.closeBackgroundThing();
    this.props.actions.setTradeResult(TRADE_RESULT_TYPE.ALERT, '交易結果未知', '請半小時後至交易紀錄中查看');
    this.setState({isShowTradeResultAlert: true,isShowDoubleCheckAlert:false,isShowLoadingAlert:false}); 
  }

  parserServerResponse(nextProps) {
    //parser OLPay|Micropay if singleOrderQuery is undefined
    var statusCode = '';
    var orderStatus = '';
    try { 
      statusCode = nextProps.receipt[SERVICE_TYPE.SINGLE_ORDER_QUERY].Header.StatusCode;
      orderStatus = nextProps.receipt[SERVICE_TYPE.SINGLE_ORDER_QUERY].Data.OrderStatus;
      this.parserSingleOrderQuery(statusCode, orderStatus);
    }catch(e) {
      this.parserOthersResponse(nextProps);
    }
  }

  parserSingleOrderQuery(statusCode, orderStatus) {
    switch(statusCode) {
      case '0000':
        if(orderStatus === '0') {
          this.setState({
            isShowLoadingAlert: this.props.mode === RECEIPT_MODE.CUSTOMER_SCAN ? false : true,
            customerScanNowStatus: '交易中',
          });
        }else if(orderStatus === '1') {
          this.endTrade(TRADE_RESULT_TYPE.SUCCESS, '交易成功', ``);
        }else {  
          this.endTrade(TRADE_RESULT_TYPE.FAIL, '交易失敗', `錯誤代碼-${statusCode}`);
        }
        break;

      case '7002':
        break;

      default:
        this.endTrade(TRADE_RESULT_TYPE.FAIL, '交易失敗', `錯誤代碼-${statusCode}`);
        break;
    }
  }

  //CustomerScan & CustomerBeScanned
  parserOthersResponse(nextProps) {
    var isCustomerScanMode = nextProps.mode === RECEIPT_MODE.CUSTOMER_SCAN;
    let js = isCustomerScanMode ? nextProps.receipt[SERVICE_TYPE.CUSTOMER_SCAN] : nextProps.receipt[SERVICE_TYPE.CUSTOMER_BE_SCANNED];
    try {
      switch (js.Header.StatusCode) {
        case '0000':
          if(isCustomerScanMode){
            this.setState({isShowLoadingAlert: false});
            if(!this.state.isStartCheckOrder)
              this.startCheckOrder();
            return;
          }else{
            this.endTrade(TRADE_RESULT_TYPE.SUCCESS, '交易成功', nextProps.orderId);
          }
          break;
        case '7002':
          this.setState({
            isShowLoadingAlert: false,
            customerScanNowStatus: '消費者尚未掃碼',
          });
          break;
        case '9997':
          if(!this.state.isStartCheckOrder)
            this.startCheckOrder();
          break;
        default:   
          this.endTrade(TRADE_RESULT_TYPE.FAIL, '交易失敗', `錯誤代碼-${js.Header.StatusCode}`);
          break;
      }
    } catch(e) {
    }
  }

  endTrade(tradeResultType, title, msg) { 
    this.closeBackgroundThing();
    this.setState({
      isShowLoadingAlert: false,
      isShowTradeResultAlert: typeof tradeResultType === 'number' ? true : false,
      isShowDoubleCheckAlert: false,
      customerScanNowStatus: '',
      isTradeEnd: true,
    });
    this.props.actions.setTradeResult(tradeResultType, title, msg);
  }

  startCheckOrder() {
    if(!this.state.isStartCheckOrder)
      this.setState({isStartCheckOrder: true});

    var array = this.state.intervals.slice();
    var tId = setTimeout(() => {
      var intervalId = setInterval(()=>{this.singleOrderQuery()}, singleOrderQueryIntervalSec *1000);
      array.push(intervalId);
    }, singleOrderQueryDelaySec * 1000);

    this.setState({
      timerId: tId,
      intervals: array,
    });
  }

  resetState() {
    this.setState({
      urlToken: '',
      customerScanNowStatus: '消費者尚未掃碼',
      isShowLoadingAlert: false,
      isShowTradeResultAlert: false,
      isStartCheckOrder: false,
      isTradeEnd: false,
      timerId: '',
      intervals: [],
    });
  }

  closeBackgroundThing() {
    this.state.timerId && clearTimeout(this.state.timerId);
    // this.state.interValId && clearInterval(this.state.interValId);
    var intervals = this.state.intervals.slice();
    while(intervals.length){
      var interval = intervals.pop();
      interval && clearInterval(interval);
    }
    this.setState({isStartCheckOrder: false});
  }
}

export default connect(
  state => ({
    merchId: state.settings.merchantId,
    tradeKey: state.settings.tradeKey,
    mode: state.receipt.mode,
    money: state.receipt.money,
    orderId: state.receipt.orderId,
    tradeResult: state.receipt.tradeResult,
    receipt: state.receipt,
    storeName: state.settings.storeName,
  }),
  (dispatch) => ({
    actions: bindActionCreators(receiptActions, dispatch)
  })
)(ReceiptPage);

const styles = StyleSheet.create({
  wrapper:{
    flex: 1,
    marginTop: Platform.OS === 'ios'? 30 : 0,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  moneyView: {
    height: (windowH - toolbarHeight - toggleModeBtnHeight - 30) * 1/4.5 ,
    width: windowW,
    alignItems: 'center',
    justifyContent: 'center',
  },
  contentView: {
    height: (windowH - toolbarHeight - 30) * 3.5/4.5,
    width: windowW,
    alignItems: 'center',
    justifyContent: 'flex-end',
    marginTop: 20,
  },
  storeNameSpace: {
    flex: 1, 
    alignItems:'center', 
    backgroundColor: '#ededed',
    justifyContent: 'center',
    width: windowW, 
  },
  storeNameUnderLine: {
    height: 1,
    width: windowW, 
  },
  storeName: {
    color: commonColor.darkGrayColor,
    fontSize: 20 *fontSizeScaler,
    paddingBottom: 1,
  }
});
    