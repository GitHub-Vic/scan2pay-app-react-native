import React, {Component} from 'react'
import { StyleSheet, Platform, View, Text, Image, FlatList, TouchableHighlight } from 'react-native'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { forIphoneXStyleByOrientation, getNowTime } from '../lib/functions'
import { METHOD, SERVICE_TYPE } from '../constants/actionTypes'
import { commonColor, fontSizeScaler, borderRadius, windowW, } from '../commonStyles'

import * as easycardActions from '../actions/easycardActions'
import Toolbar from '../components/ezcardSetting/Toolbar'
import LinearGradient from 'react-native-linear-gradient'
import LoadingAlert from '../components/alert/TitleWithOneBtn'

class EZCardSetting extends Component {

    constructor() {
        super()
        this.state = {
            iphoneXstyle: forIphoneXStyleByOrientation(),
            devices: [],
            selectDevice: '',
        }
    }
    
    componentDidMount() {
        this.setState({selectDevice: this.props.easycardDevice})
        this.fetchDevices()
    }
    
    componentWillReceiveProps (nextProps) {
        if(nextProps.fetchResponse !== undefined)
                this.parserResponse(nextProps.fetchResponse)
    }

    fetchDevices() {
        var requestData = new Map()
        requestData.set("Method", METHOD.EASYCARD)
        requestData.set("ServiceType", SERVICE_TYPE.EASYCARD_DEVICE_QUERY)
        requestData.set("MchId", this.props.merchId)
        requestData.set("CreateTime", getNowTime())
        requestData.set("DeviceId", '00000000')
        requestData.set("TradeKey", this.props.tradeKey)
        requestData.set("Retry", "0")
        this.props.actions.fetchIntellaApi(requestData);
        
    }

    parserResponse(msg) {
        if(msg.length == 0)
            return

        var json = JSON.parse(msg)
        var statusCode = json.Header.StatusCode
        if(statusCode === '0000')
            if(json.Header.ServiceType === SERVICE_TYPE.EASYCARD_DEVICE_QUERY)
                this.parserDevices(json)
    }

    parserDevices(json) {
        try{
            this.setState({devices: json.Data.DeviceList})
        }catch(e){
            this.setState({devices: []})
        }
    }
    // 儲存(將ownDeviceID 存入 redux 使悠遊卡頁面重新變更)後返回
    storeAndBack(selectDevice) {
        this.props.actions.setEasycardDevice(selectDevice)
        this.props.navigation.goBack()
    }

    render() {
        return (
            <View style={[styles.wrapper, this.state.iphoneXstyle]}>
            {this.renderToolBar()}
            {this.renderDevicesList()}
            {this.props.isFetching && this.props.serviceType === SERVICE_TYPE.EASYCARD_DEVICE_QUERY ? <LoadingAlert title={'搜尋中'} /> : null}
            </View>
        )
    }

    renderToolBar() {
        return (
          <Toolbar 
            title = {'機台設定'}
            backgroundImg={'bg_toolbar_create'}
            backOnPress={() => this.props.navigation.goBack()}
            storePress={()=> this.storeAndBack(this.state.selectDevice)} />
        )
    }

    renderDevicesList() {
        return (
            <FlatList style={{flex:1, paddingTop: 5}}
                data={this.state.devices}
                keyExtractor={(item, index) => index}
                renderItem={({item, index}) => this.renderDeviceListItem(item)}
                ItemSeparatorComponent={() => this.separator()} />
        )
    }

    renderDeviceListItem(item) {
        return (
            <TouchableHighlight style={styles.itemTouchWrapper}
                onPress={() => {
                    this.setState({selectDevice: item.ownerDeviceId})
                    this.storeAndBack(item.ownerDeviceId)
                }} 
                underlayColor = 'transparent' >
                <View style={styles.itemWrapper}>
                    <Text style={styles.itemText} >{`機台名稱：${item.ownerDeviceId}`}</Text>
                    <View style={styles.itemStateView}>
                        {item.ownerDeviceId == this.state.selectDevice ? this.renderSelectView() : this.renderNotSelectView() }
                    </View>
                </View>
          </TouchableHighlight>
            
        )
    }

    renderNotSelectView() {
        return (
            <LinearGradient style={styles.itemNotSelectWrapper}
                start={{x: 0.0, y: 0.5}} end={{x: 1, y: 0.5}}
                colors={commonColor.createGradient} >
                <View style={styles.itemNotSelectView}>
                    <Text style={styles.itemNotSelectText}>選擇</Text> 
                </View>
            </LinearGradient>   
        )
    }

    renderSelectView() {
        return (
            <LinearGradient style={styles.itemSelectWrapper}
                start={{x: 0.0, y: 0.5}} end={{x: 1, y: 0.5}}
                colors={commonColor.createGradient}>
                <View style={styles.itemSelectView}>
                    <Image style={styles.itemSelectImg} source={{uri: 'icon_easycard_device_select'}} />
                    <Text style={styles.itemSelectText}>使用中</Text>
                </View>
            </LinearGradient> 
        )
    }
    
    separator() {
        return <View style={styles.separator} />
    }
    
}

export default connect(
    state => ({
      merchId: state.settings.merchantId,
      tradeKey: state.settings.tradeKey,
      isFetching: state.easycard.isFetching,
      serviceType: state.easycard.serviceType,
      fetchResponse: state.easycard.fetchResponse,
      easycardDevice: state.easycard.easycardDevice,
    }),
    (dispatch) => ({
      actions: bindActionCreators(easycardActions, dispatch)
    })
)(EZCardSetting)



const defaultFontSize = 12 *fontSizeScaler
const deviceStateWidth = windowW/5
const deviceStateHeigh = 26 *fontSizeScaler

const styles = StyleSheet.create({
    wrapper: {
        flex: 1,
        marginTop: Platform.OS === 'ios'? 30 : 0,
        backgroundColor: 'white'
    },
    itemTouchWrapper: {
        flex: 1,
    },
    itemWrapper: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 20,
        marginHorizontal: 15,
    },
    itemText: {
        flex: 2,
        fontSize: defaultFontSize,
    },
    itemStateView: {
        flex: 1,
        alignItems: 'flex-end',
        justifyContent: 'center',
    },
    itemNotSelectWrapper: {
        width: deviceStateWidth,
        height: deviceStateHeigh,
        borderRadius: borderRadius,
        justifyContent: 'center',
        alignItems: 'center',
    },
    itemNotSelectView: {
        width: deviceStateWidth -2,
        height: deviceStateHeigh -2,
        borderRadius: borderRadius,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
    },
    itemNotSelectText: {
        fontSize: defaultFontSize,
        color: commonColor.customerScan,
    },
    itemSelectWrapper: {
        width: deviceStateWidth *1.3,
        height: deviceStateHeigh,
        borderRadius: borderRadius,
    },
    itemSelectView: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    itemSelectText: {
        fontSize: defaultFontSize,
        backgroundColor: 'transparent',
        color: 'white',
    },
    itemSelectImg: {
        height: 15 *fontSizeScaler, 
        width: 15 *fontSizeScaler,
        marginRight: 10,
    },
    separator: {
      height: 1,
      backgroundColor: commonColor.listViewDividerColor,
    },
})

