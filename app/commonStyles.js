'use strict'
/**
 * @class StylesCommon
 * @desc 通用样式
 * */
import { StyleSheet, Platform, Dimensions, PixelRatio } from 'react-native'

export var windowH = Dimensions.get('window').height
export var windowW = Dimensions.get('window').width

export const fontSizeScaler = Platform.select({
  android: PixelRatio.getFontScale(), //PixelRatio.get()/PixelRatio.getFontScale()
  ios: calculateIOSSize()
})
function calculateIOSSize() {
  const pixelRatio = PixelRatio.get()
  if (pixelRatio === 2) {
    // iphone 5s and older Androids
    if (windowW < 360) {
      return 0.95
    }
    // iphone 5
    if (windowH < 667) {
      return 1
      // iphone 6-6s
    } else if (windowH >= 667 && windowH <= 735) {
      return 1.15
    }
    // older phablets
    return 1.25
  }
  if (pixelRatio === 3) {
    // catch larger devices
    // ie iphone 6s plus / 7 plus / mi note 等等
    return 1.27
  }
  return 1
}

export var elementHeigh = (windowH * 3.5) / 40 // 3.5/40
export var elementWitdh = (windowW * 2) / 3
export var mainBtnHeigh = (windowH * 1) / 15
export var mainBtnWidth = (windowW * 1.4) / 3
export var borderRadius = 45
export var dialogBorderRadius = 5
export var toolbarHeight = 50
export var toggleModeBtnHeight = (windowH * 2.5) / 19
export var alertTitleTextSize = 22 * fontSizeScaler
export var alertBtnTextSize = 17 * fontSizeScaler
export var commonColor = {
  defaultTextColor: '#434343',
  hintTextColor: '#999799',
  customerScan: '#85BC20',
  customerBeScanned: '#35B597',
  defaultRedColor: '#E94B24',
  defaultButtonColor: '#7DC2BE',
  createGradient: ['#B7D44B', '#88C142', '#89BC41'],
  scanGradient: ['#89C122', '#35B597'],
  success: '#99CC33',
  warning: '#E54B4B',
  disableGradient: ['#999', '#ccc'],
  btnClickColor: 'rgba(125, 192, 190, 0.5)',
  listViewDividerColor: '#DDDDDD',
  btnEnableFalseColor: '#999699',
  timeTextColor: '#9F9F9F',
  messageNotReadBackgroundColor: 'rgba(133, 188, 32, 0.2)',
  darkGrayColor: '#727171',
  ezcardPageDividingLineColor: '#969696'
}

export var commonStyles = StyleSheet.create({
  navigationTitle: {
    paddingLeft: Platform.OS === 'ios' ? 0 : windowW / 3,
    color: 'white',
    alignItems: 'center',
    fontSize: 20
  },
  navigationCustomerScan: {
    backgroundColor: commonColor.customerScan
  },
  navigationCustomerBeScanned: {
    backgroundColor: commonColor.customerBeScanned
  },
  alertBackgroundView: {
    height: windowH,
    width: windowW,
    backgroundColor: 'rgba(0,0,0,0.7)',
    position: 'absolute',
    top: 0,
    left: 0
  }
})
