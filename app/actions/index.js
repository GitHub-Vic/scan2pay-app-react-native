import * as messageActions from './messageActions'
import * as receiptActions from './receiptActions'
import * as recordActions from './recordActions'
import * as settingsActions from './settingsActions'
import * as easycardActions from './easycardActions'

export default appActions = {
  messageActions,
  receiptActions,
  recordActions,
  settingsActions,
  easycardActions,
}