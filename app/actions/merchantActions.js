import * as types from '../constants/actionTypes'
import {httpsPost, getNowTime, nativeEncryptJson, nativeDecryptResponse, mapToJsonString } from '../lib/functions'
import store from '../store'
import {URL_INTELLA_DOMAIN } from '../config'
import {login} from './settingsActions'

export function endLoadSSO() {
  return {
    type: types.END_LOAD_SSO,
  }
}

export function loading(isFetchingTitle) {
  return {
    type: types.LOADING,
    isFetchingTitle
  }
}

export function parseInsertUserMerchant(data) {
  return {
      type: types.PARSE_INSERT_USER_MERCHANT,
      data,
  }
}

export function parseDeleteUserMerchant(data) {
  return {
      type: types.PARSE_DELETE_USER_MERCHANT,
      data,
  }
}

export function parseUpdateUserMerchant(data) {
  return {
      type: types.PARSE_UPDATE_USER_MERCHANT,
      data,
  }
}

export function parseToken(data) {
  return {
      type: types.PARSE_TOKEN,
      data,
      time: getNowTime(),
  }
}

export function parseUserMerchant(data, successFunction = () => {}) {
  return {
      type: types.PARSE_USER_MERCHANT,
      data,
      successFunction,
  }
}

export function setChoosedMerchant(data) {
  return {
    type: types.SET_CHOOSED_MERCHANT,
    data: data,
  }
}

export function resetChoosedMerchant(data) {
  return {
    type: types.RESET_CHOOSED_MERCHANT,
    data: data,
  }
}

export function setMerchants(merchantsData) {
  return {
    type: types.SET_MERCHANTS,
    merchantsData: merchantsData,
  }
}

export function getUserMerchantByUserId(url, requestData, callBackFunction = {}) {
  return dispatch => {
    generalCallAPI (dispatch ,url, requestData, callBackFunction, (msg)=>{
      const msgJson = JSON.parse(msg)
      console.log('msgJson', msgJson)
      if (msgJson.msgCode === '9995' || msgJson.msgCode === '9996') {
        callBackFunction.failFunction && callBackFunction.logoutFunction({resultMsg: msgJson.msg})
        dispatch(endLoadSSO())
      } else if (msgJson.msgCode !== '0000' && msgJson.msgCode !== '0001') {
        callBackFunction.failFunction && callBackFunction.failFunction({resultMsg: msgJson.msg})
        dispatch(endLoadSSO())
      } else if (msgJson.msgCode === '0000' || msgJson.msgCode === '0001') {
        dispatch(parseUserMerchant(msgJson, callBackFunction.successFunction))
      } else {
        dispatch(endLoadSSO())
      }
      
    }, undefined)
  }
}

export function insertUserMerchant(url, requestData, callBackFunction) {
  return dispatch => {
    generalCallAPI (dispatch ,url, requestData, callBackFunction, (msg)=>{
      const msgJson = JSON.parse(msg)
      dispatch(parseInsertUserMerchant(msgJson))
      if (msgJson.msgCode === '0000') {
        var requestData = new Map()
        requestData.set('userId', store.getState().merchant.userId)
        dispatch(getUserMerchantByUserId(`${URL_INTELLA_DOMAIN}userMerchant/userId`, requestData, callBackFunction))
      } else {
        callBackFunction.failFunction && callBackFunction.failFunction({resultMsg: msgJson.msg})
      }
    }, '新增中')
  }
}

export function deleteUserMerchant(url, requestData, callBackFunction) {
  return dispatch => {
    generalCallAPI (dispatch ,url, requestData, callBackFunction, (msg)=>{
      const msgJson = JSON.parse(msg)
      dispatch(parseDeleteUserMerchant(msg))
      if (msgJson.msgCode === '0000') {
        var requestData = new Map()
        requestData.set('userId', store.getState().merchant.userId)
        dispatch(getUserMerchantByUserId(`${URL_INTELLA_DOMAIN}userMerchant/userId`, requestData, callBackFunction))
      } else {
        callBackFunction.failFunction && callBackFunction.failFunction({resultMsg: msgJson.msg})
      }
      
    }, '刪除中')
  }
}

export function updateUserMerchant(url, requestData, callBackFunction) {
  let requestDataOrigin = requestData
  return dispatch => {
    generalCallAPI (dispatch ,url, requestData, callBackFunction, (msg)=>{
      const msgJson = JSON.parse(msg)
      let choosedMerchant = {}
      requestDataOrigin.forEach((value, key) => {
        choosedMerchant[key] = value
      })
      dispatch(setChoosedMerchant(choosedMerchant))
      dispatch(parseUpdateUserMerchant(msgJson))
      if (msgJson.msgCode === '0000') {
        var requestData = new Map()
        requestData.set('userId', store.getState().merchant.userId)
        dispatch(getUserMerchantByUserId(`${URL_INTELLA_DOMAIN}userMerchant/userId`, requestData, callBackFunction))
      } else {
        callBackFunction.failFunction && callBackFunction.failFunction({resultMsg: msgJson.msg})
      }
    }, '修改中')
  }
}

export function getToken(url, requestData, callBackFunction) {
  return dispatch => {
    generalCallAPI (dispatch, url, requestData, callBackFunction, (msg)=>{
      const msgJson = JSON.parse(msg)
      console.log('msgJson', msgJson)
      if (msgJson.msgCode === '0000') {
        dispatch(parseToken(msgJson))
        dispatch(login('', '', '',''))
        callBackFunction.successFunction && callBackFunction.successFunction()
      } else {
        callBackFunction.failFunction && callBackFunction.failFunction({resultMsg: msgJson.msg})
        dispatch(endLoadSSO())
      }
    }, undefined)
  }
}

function generalCallAPI (dispatch, url, requestData, callBackFunction, customFunction, loadingTitle) {
  console.log('requestData', requestData)
  console.log('url', url)
  dispatch(loading(loadingTitle))
  requestData.set('tokenId', store.getState().merchant.tokenId)
  const json = mapToJsonString(requestData)
  nativeEncryptJson(json, url,
    async(successMsg, secretKey)=>{
      let msg =  await httpsPost(url, successMsg)
      if(msg.includes('Network request failed')){
        callBackFunction.failFunction && callBackFunction.failFunction({msg: '請確認網路狀態'})
        return
      }
      nativeDecryptResponse(secretKey, msg,
        customFunction,
        (errMsg)=>{
          callBackFunction.failFunction && callBackFunction.failFunction({resultMsg: `解密失敗-\n${errMsg}`})
        })
    },
    (errMsg)=>{
      callBackFunction.failFunction && callBackFunction.failFunction({resultMsg: `加密異常-\n${errMsg}`})
    }
  )
}