import * as types from '../constants/actionTypes' 

//string boolean
export function reset() {
  return {
    type: types.RESET_SETTINGS,
  }
}

//string boolean
export function login(merchId, storeName, tradeKey,randomid) {
  return {
    type: types.LOGIN,
    merchantId: merchId,
    storeName: storeName,
    tradeKey: tradeKey,
    randomid:randomid
  }
}

export function logout() {
  return {
    type: types.LOGOUT,
  }
}

//string
export function setUdid(udid) {
  return {
    type: types.SET_UDID,
    udid: udid,
  }
}

//string
export function setAPNsToken(token) {
  return {
    type: types.SET_APNS_TOKEN,
    token: token,
  }
}

export function alreadyRegisterAPNsToken() {
  return {
    type: types.ALREADY_REGISTER_APNSTOKEN
  }
}

export function alreadyUnregisterAPNsToken() {
  return {
    type: types.ALREADY_UNREGISTER_APNSTOKEN
  }
}

//boolean
export function setIsCleanMqttSession(isCleanMqttSession) {
  return {
    type: types.SET_IS_CLEANMQTTSESSION,
    isCleanMqttSession: isCleanMqttSession,
  }
}

export function alreadyShownLogoPage() {
  return {
    type: types.ALREADY_SHOWN_LOGOPAGE,
  }
}

export function setIsStoreAccount(isStoreAccount) {
  return {
    type: types.SET_IS_STORE_ACCOUNT,
    isStoreAccount: isStoreAccount,
  }
}

