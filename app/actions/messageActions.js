import * as types from '../constants/actionTypes';

//string of jsonArray
export function setAllMessage(messages) {
  return {
    type: types.SET_ALL_MESSAGE,
    message: messages,
  }
}

//string of jsonObject
export function addMessage(message) {
  return {
    type: types.ADD_MESSAGE,
    message: message,
  }
}

//string
export function setRead(orderId) {
  return {
    type: types.SET_READ,
    orderId: orderId,
  }
}

export function setAllRead() {
  return {
    type: types.SET_ALL_READ,
  }
}

export function toggleDeleteMode() {
  return {
    type: types.TOGGLE_DELETE_MODE,
  }
}

//string
export function toggleIsCheck(orderId) {
  return {
    type: types.TOGGLE_IS_CHECK,
    orderId: orderId,
  }
}

//boolean, int
export function setIsCheckByCount(isCheck, forwardCount) {
  return {
    type: types.SET_ISCHECK_BY_COUNT,
    isCheck: isCheck,
    forwardCount: forwardCount,
  }
}

export function deleteCheckMessage() {
  return {
    type: types.DELETE_CHECK_MESSAGE,
  }
}


