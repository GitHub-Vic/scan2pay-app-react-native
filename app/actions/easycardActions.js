import * as types from '../constants/actionTypes'
import {SERVICE_TYPE, TRADE_RESULT_TYPE} from '../constants/actionTypes'
import {URL_INTELLA_GENERAL } from '../config'
import {httpsPost, nativeEncryptJson, nativeDecryptResponse, mapToJsonString, consoleForDebug } from '../lib/functions'


export function startFetch(serviceType) {
  return {
    type: types.FETCH_EZC,
    serviceType: serviceType,
  }
}

export function endFetch(msg) {
  return {
    type: types.FETCH_EZC_END,
    fetchResponse: msg,
  }
}

export function fetchIntellaApi(requestData) {
  consoleForDebug(`${requestData.get("ServiceType")} - ${URL_INTELLA_GENERAL}\n${requestData}`)
  return dispatch => {
    dispatch(startFetch(requestData.get("ServiceType")))
    const json = mapToJsonString(requestData)
    nativeEncryptJson(json, URL_INTELLA_GENERAL,
      async(successMsg, secretKey)=>{
        httpsPost(URL_INTELLA_GENERAL, successMsg).then(function(msg) {
          //處理 response
          if(msg.includes('Network request failed')){
            dispatch(endFetch('請確認網路狀態'))
            return
          }
          
          nativeDecryptResponse(secretKey, msg,
            (msg)=>{
              dispatch(endFetch(msg))
            },
            (errMsg)=>{
              dispatch(endFetch(`${requestData.get("ServiceType")} 解密失敗-\n${errMsg}`))
            })  
          }).catch(function(err) {
            dispatch(endFetch(`${requestData.get("ServiceType")} 加密異常-\n${errMsg}`))
          })
      }
    )
  }
}

export function setEasycardDevice(device) {
  return {
    type: types.SET_EASYCARD_DEVICE,
    easycardDevice: device
  }
}

export function logoutEasycard() {
  return {
    type: types.CLEAN_EASYCARD_DEVICE,   
  }
}