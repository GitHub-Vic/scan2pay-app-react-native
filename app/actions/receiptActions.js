import * as types from '../constants/actionTypes';
import { httpsPost, nativeEncryptJson, nativeDecryptResponse, consoleForDebug } from '../lib/functions'
import {URL_INTELLA_ORDER} from '../config'

export function toggleReceiptMode(mode) {
  return {
    type: types.TOGGLE_RECEIPT_MODE,
    mode: mode,
  }
}

export function setReceiptMoney(money) {
  return {
    type: types.SET_RECEIPT_MONEY,
    money: money,
  }
}

export function doGenerateOrderId(mchId) {
  return async dispatch => {
    const response = await fetch(`${URL_INTELLA_ORDER}/createAppOrderNo/${mchId}`);
    responseJson = await response.json()
    if (!responseJson.msg) {
      dispatch(generateOrderId(responseJson.orderNo));
    }
  }
}

export function generateOrderId(data) {
  return {
    type: types.GENERATE_ORDER_ID,
    data
  }
}

export function resetTradeData() {
  return {
    type: types.RESET_TRADE_DATA
  }
}

export function resetTradeMoney() {
  return {
    type: types.RESET_TRADE_MONEY
  }
}

export function setTradeResult(tradeStatus, title, msg) {
  return {
    type: types.SET_TRADE_RESULT,
    tradeStatus,
    title,
    msg,
  }
}

export function fetchIntellaApi(serviceType, url, json) {  
  consoleForDebug(`${serviceType} - ${url}\n${json}`);
  return dispatch => {
    dispatch(requestIntellaApi(serviceType));
    if(isNeedEncrypt(serviceType)) {
      encryptJson(dispatch, serviceType, url, json);
    }
    // else{
    //   return fetch(`https://www.reddit.com/r/frontend.json`)
    //   .then(response => response.json())
    //   .then(json => dispatch(receiveIntellaApi(serviceType, json)))
    // }
  }
}

function requestIntellaApi(serviceType) {
  return {
    type: types.REQUEST_INTELLA_API,
    serviceType,
  }
}

function receiveIntellaApi(serviceType, json) {
  return {
    type: types.RECEIVE_INTELLA_API,
    serviceType,
    data: json,
  }
}

function isNeedEncrypt(serviceType) {
  if(serviceType === types.SERVICE_TYPE.CUSTOMER_BE_SCANNED)
    return true;
  if(serviceType === types.SERVICE_TYPE.CUSTOMER_SCAN)
    return true;
  if(serviceType === types.SERVICE_TYPE.SINGLE_ORDER_QUERY)
    return true;
}

function encryptJson(dispatch, serviceType, url, json){
  nativeEncryptJson(json, url,
    async(successMsg, secretKey)=>{
      var msg =  await httpsPost(url, successMsg);
      if(msg.includes('Network request failed')){
        dispatch(setTradeResult(types.TRADE_RESULT_TYPE.FAIL, '請確認網路狀態', msg));
        return;
      }

      nativeDecryptResponse(secretKey, msg,
        (msg)=>{
          dispatch(receiveIntellaApi(serviceType, JSON.parse(msg)));
        },
        (errMsg)=>{
          dispatch(setTradeResult(types.TRADE_RESULT_TYPE.FAIL, '解密失敗', `${serviceType}-${errMsg}`));
        });
    },
    (errMsg)=>{
        dispatch(setTradeResult(types.TRADE_RESULT_TYPE.FAIL, '功能異常',`${serviceType}-加密異常\n${errMsg}`));
    }
  );
}

// export function fetchIntellaApi(serviceType) {
//   return dispatch => {
//     dispatch(requestIntellaApi(serviceType))
//     return fetch(`https://www.reddit.com/r/frontend.json`)
//     .then(response => response.json())
//     .then(json => dispatch(receiveIntellaApi(serviceType, json)))
//   }
// }