import * as types from '../constants/actionTypes'
import {SERVICE_TYPE, TRADE_RESULT_TYPE} from '../constants/actionTypes'
import {URL_INTELLA_GENERAL } from '../config'
import {httpsPost, getNowTime, nativeEncryptJson, nativeDecryptResponse, mapToJsonString, consoleForDebug } from '../lib/functions'

export function fetchRecord() {
  return {
    type: types.FETCH_RECORD,
  }
}

export function endFetching(response) {
  return {
      type: types.END_FETCHING,
      response: response,
      time: getNowTime(),
  }
}

export function resetRecord() {
  return {
    type: types.RESET_RECORD,
  }
}

/**
 * 
 * @param {*} nowPage int
 * @param {*} recordData JsonArray
 */
export function setRecord(nowPage, recordData) {
  return {
    type: types.SET_RECORD,
    nowPage: nowPage,
    recordData: recordData,
  }
}

export function setRecordNoData() {
  return {
    type: types.SET_RECORD_NO_DATA,
  }
}

export function setRecordIsEnd() {
  return {
    type: types.SET_RECORD_IS_END,
  }
}

export function setOrderRefund(orderId, serviceType, createTime) {
  return {
    type: types.SET_ORDER_REFUND,
    orderId: orderId, 
    serviceType: serviceType,
    createTime: createTime,
  }
}

//for PageQuery and Refund(Cancel)
export function fetchAPI(requestData) {
  consoleForDebug(`${requestData.get("ServiceType")} - ${URL_INTELLA_GENERAL}\n${requestData}`)
  return dispatch => {
    dispatch(fetchRecord())
    const json = mapToJsonString(requestData)
    nativeEncryptJson(json, URL_INTELLA_GENERAL,
      async(successMsg, secretKey)=>{
        let msg =  await httpsPost(URL_INTELLA_GENERAL, successMsg)
        if(msg.includes('Network request failed')){
          dispatch(endFetching('請確認網路狀態'))
          return
        }

        nativeDecryptResponse(secretKey, msg,
          (msg)=>{
            dispatch(endFetching(msg))
          },
          (errMsg)=>{
            dispatch(endFetching(`${requestData.get("ServiceType")} 解密失敗-\n${errMsg}`))
          })
      },
      (errMsg)=>{
        dispatch(endFetching(`${requestData.get("ServiceType")} 加密異常-\n${errMsg}`))
      }
    )
  }
}

