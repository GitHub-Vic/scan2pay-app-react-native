/**
* @class RecordDetailView
* @desc 交易紀錄明細
* @prop
    item object 交易紀錄中的一個JsonObject
    onPress function 按了我要退款
*/

import React, { Component } from 'react'
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  TouchableHighlight
} from 'react-native'

import {
  commonColor,
  windowW,
  windowH,
  dialogBorderRadius,
  borderRadius,
  elementHeigh,
  alertBtnTextSize
} from '../../commonStyles'
import { getValueByKeyFromJsonObject } from '../../lib/functions'
import * as formatFunctions from '../../lib/formatFunctionsForShow'

export default class RecordDetailView extends Component {
  shouldComponentUpdate(nextProps) {
    if (nextProps.item === this.props.item) {
      return false
    }
    return true
  }

  render() {
    const item = this.props.item
    var {
      orderStatus,
      textColor,
      refundTime
    } = formatFunctions.formatTradeStatus(this.props.item)
    var { canRefund, showPrompt } = formatFunctions.analyticRefundBtnAndPrompt(
      this.props.item
    )
    var btnPressColor = canRefund
      ? commonColor.btnClickColor
      : commonColor.btnEnableFalseColor
    var refundBtnColor = canRefund
      ? { backgroundColor: commonColor.defaultRedColor }
      : { backgroundColor: commonColor.btnEnableFalseColor }
    let amount = getValueByKeyFromJsonObject(item, 'payment')
    let amountText = orderStatus === '退款成功' ? amount * -1 : amount
    return (
      <View style={styles.wrapper}>
        <View style={styles.detailWrapper}>
          <ScrollView>
            {this.renderOneLine(
              '消費金額',
              `NT$ ${formatFunctions.formatMoneyToCama(amountText)}`,
              textColor
            )}
            {this.renderOneLine(
              '交易日期',
              formatFunctions.formatTime(
                getValueByKeyFromJsonObject(item, 'createDate')
              )
            )}
            {this.renderOneLine(
              '交易單號',
              getValueByKeyFromJsonObject(item, 'systemOrderId')
            )}
            {this.renderOneLine(
              '付款類別',
              formatFunctions.formatMethod(this.props.item)
            )}
            {this.renderOneLine('訂單狀態', orderStatus, textColor)}
            {this.renderOneLine(
              '訂單編號',
              getValueByKeyFromJsonObject(item, 'orderId')
            )}
            {this.renderOneLine(
              '商品名稱',
              getValueByKeyFromJsonObject(item, 'description')
            )}
            {this.renderRefundTime(refundTime)}
          </ScrollView>
        </View>
        <View style={styles.elseWrapper}>
          {showPrompt ? this.renderCannotRefundPrompt() : null}
        </View>
        <View style={styles.elseWrapper}>
          {canRefund
            ? this.renderRefundBtn(btnPressColor, refundBtnColor)
            : null}
        </View>
      </View>
    )
  }

  renderOneLine(key, value, valueTextColor) {
    var textColor = valueTextColor ? { color: valueTextColor } : null
    return (
      <View>
        <View style={styles.oneLineWrapper}>
          <View style={styles.leftView}>
            <Text style={styles.text}>{key}</Text>
          </View>
          <View style={styles.rightView}>
            <Text style={[styles.text, textColor]}>{value}</Text>
          </View>
        </View>
        <View style={styles.separator} />
      </View>
    )
  }

  renderRefundTime(refundTime) {
    if (refundTime) {
      return this.renderOneLine(
        '退款時間',
        formatFunctions.formatTime(refundTime)
      )
    }
    return null
  }

  renderCannotRefundPrompt() {
    return (
      <Text style={{ color: commonColor.defaultRedColor }}>
        此筆交易尚未撥款
      </Text>
    )
  }

  renderRefundBtn(btnPressColor, refundBtnColor) {
    return (
      <TouchableHighlight
        style={styles.refundBtnWrapper}
        underlayColor={btnPressColor}
        onPress={() => this.props.onPress()}
      >
        <View style={[styles.refundBtn, refundBtnColor]}>
          <Text style={{ color: 'white', fontSize: alertBtnTextSize }}>
            我 要 退 款
          </Text>
        </View>
      </TouchableHighlight>
    )
  }
}

const padding = 20
const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    padding: padding
  },
  detailWrapper: {
    flex: 7,
    width: windowW - padding * 2,
    padding: 10,
    backgroundColor: 'white',
    borderRadius: dialogBorderRadius
  },
  elseWrapper: {
    flex: 1.5,
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  separator: {
    height: 1,
    width: windowW - padding * 2 - 20,
    marginTop: 5,
    marginBottom: 15,
    backgroundColor: commonColor.listViewDividerColor
  },
  oneLineWrapper: {
    flexDirection: 'row'
  },
  leftView: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center'
  },
  rightView: {
    flex: 3,
    alignItems: 'flex-end'
  },
  text: {
    color: commonColor.defaultTextColor
  },
  refundBtnWrapper: {
    width: (windowW * 1.5) / 3,
    height: (windowH * 1) / 13,
    borderRadius: borderRadius,
    alignItems: 'center',
    justifyContent: 'center'
  },
  refundBtn: {
    width: (windowW * 1.5) / 3,
    height: (windowH * 1) / 13,
    borderRadius: borderRadius,
    alignItems: 'center',
    justifyContent: 'center'
  }
})
