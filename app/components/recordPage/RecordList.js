/**
* @class RecordList
* @desc 交易紀錄List
* @prop
    data array 要顯示的資料們
    isEnd boolean 是否沒有更多資料。 true=沒有更多資料了。
    fecthMore function 獲取資料
    onPress function item's onPress
*/
import React, {Component} from 'react'
import {StyleSheet, View, FlatList, Text, Image} from 'react-native'
import * as Progress from 'react-native-progress'

import {commonColor, fontSizeScaler} from '../../commonStyles'
import RecordItemView from './RecordItemView'

import testData from './record.json';
export default class RecordList extends Component {

  shouldComponentUpdate(nextProps) {
    if(nextProps.data === this.props.data) {
      return false;
    }
    return true;
  }

  render() {
    // 測試資料 data={testData}
    return(
      <FlatList
        data={this.props.data}
        keyExtractor={(item, index) => item.orderId}
        renderItem={({item, index}) => <RecordItemView  item={item} onPress={()=> this.props.onPress(index)}/>}
        ItemSeparatorComponent={() => this.separator()} 
        onEndReachedThreshold = {0.5}
        onEndReached={()=> {this.props.isAdvancedSearch || this.props.isEnd ? null : this.props.fecthMore()} }
        ListFooterComponent={() => this.footerComponent()}
       />
    );
  }

  separator() {
    return <View style={styles.separator} />
  }

  footerComponent() {
    if(this.props.isAdvancedSearch || this.props.isEnd) {
      return (
        <View style={styles.footerView}>
          <Text>最後一筆</Text>
        </View>
      );
    }else {
      return (
        <View style={styles.footerView}>
          <Progress.CircleSnail size={30*fontSizeScaler} indeterminate={true} color={'black'} direction={'clockwise'}/>
          <Text style={{color:'black',marginHorizontal: 10}}>上拉 加載更多</Text>
        </View>
      );
    }
  }

}

const styles = StyleSheet.create({
  separator: {
    height: 1,
    backgroundColor: commonColor.listViewDividerColor,
  },
  footerView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});