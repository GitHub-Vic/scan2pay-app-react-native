/**
* @class RecordItemView
* @desc 交易紀錄List's item
* @prop
    item 交易紀錄中的一個JsonObject
    onPress function 是否沒有更多資料。 true=沒有更多資料了。
*/

import React, { PureComponent } from 'react'
import { StyleSheet, View, Text, Image, TouchableHighlight } from 'react-native'

import { commonColor, windowW } from '../../commonStyles'
import { getValueByKeyFromJsonObject } from '../../lib/functions'
import * as formatFunctions from '../../lib/formatFunctionsForShow'

export default class RecordItemView extends PureComponent {
  shouldComponentUpdate(nextProps) {
    if (nextProps.item === this.props.item) {
      return false
    }
    return true
  }

  render() {
    return (
      <TouchableHighlight
        onPress={() => this.props.onPress()}
        underlayColor="transparent"
      >
        {this.renderItem()}
      </TouchableHighlight>
    )
  }

  renderItem() {
    const item = this.props.item
    var { orderStatus, textColor } = formatFunctions.formatTradeStatus(item)
    let amountStyle = {
      fontFamily: 'Helvetica',
      textDecorationLine: orderStatus === '交易失敗' ? 'line-through' : 'none'
    }

    let amount = orderStatus === '退款成功' ? item.payment * -1 : item.payment
    let amoumtText = formatFunctions.formatMoneyToCama(amount)
    return (
      <View style={styles.wrapper}>
        <View style={styles.content}>
          <View style={styles.row}>
            <Image
              style={styles.timeImg}
              source={{ uri: 'icon_message_item_time' }}
            />
            <Text style={styles.valueText}>
              {formatFunctions.formatTime(item.createDate)}
            </Text>
          </View>

          <View style={styles.row}>
            <Text style={styles.keyText}>交易單號: </Text>
            <Text style={styles.valueText}>
              {formatFunctions.formatSystemOrderId(
                getValueByKeyFromJsonObject(item, 'systemOrderId')
              )}
            </Text>
          </View>

          <View style={styles.row}>
            <Text style={styles.keyText}>付款類別: </Text>
            <Text style={styles.valueText}>
              {formatFunctions.formatMethod(item)}
            </Text>
          </View>

          <View style={styles.row}>
            <Text style={styles.keyText}>訂單狀態: </Text>
            <Text style={[styles.valueText, { color: textColor }]}>
              {orderStatus}
            </Text>
          </View>

          <View style={styles.row}>
            <Text style={styles.keyText}>商品名稱: </Text>
            <Text style={styles.valueText}>{item.description}</Text>
          </View>
        </View>

        <View style={styles.money}>
          <Text style={{ color: textColor }}>
            NT$ <Text style={amountStyle}>{amoumtText}</Text>
          </Text>
        </View>
        <View style={styles.more}>
          <Image
            style={styles.timeImg}
            source={{ uri: 'icon_record_go_detail' }}
          />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    flexDirection: 'row',
    padding: 10
  },
  row: {
    flexDirection: 'row'
  },
  timeImg: {
    height: 20,
    width: 20
  },
  content: {
    width: (windowW * 5.5) / 10
  },
  money: {
    width: (windowW * 3) / 10,
    alignItems: 'flex-end',
    justifyContent: 'center'
  },
  more: {
    width: (windowW * 1.5) / 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  keyText: {
    color: commonColor.defaultTextColor
  },
  valueText: {
    width: (windowW * 5.5) / 10,
    color: commonColor.defaultTextColor
  }
})
