/**
* @function SearchBar
* @desc RecordPage - SearchBar
* @prop
    isShow boolean 是否顯示
    textInputRef function inputText's ref
    onTextChange function when InputText's content change
    value string 輸入框內要顯示的值
    cancelOnPress function
*/
import React from 'react'
import {StyleSheet, Platform, TouchableHighlight, View, Image, Text, TextInput} from 'react-native'
import {commonColor, windowW, toolbarHeight} from '../../commonStyles'

export default function SearchBar(props) {
  var visible = props.isShow ? null : {height:0,width:0};
  return (
    <View style={[styles.wrapper, visible]}>
    <Image source={{uri: "bg_toolbar_create"}} style={styles.backgroundImg}>
      <View style={styles.contentWrapper} >
        <TextInput
          style={styles.inputText}
          multiline={false}
          underlineColorAndroid='transparent'
          keyboardType={'numeric'}
          placeholder='請輸入交易單號末四碼'
          placeholderTextColor={commonColor.hintTextColor}
          onChangeText={(text) => props.onTextChange(text)}
          value={props.value}
          ref={props.textInputRef}
        />
        <TouchableHighlight onPress={() => props.cancelOnPress()} 
          style={styles.textView}
          underlayColor={'transparent'} >
          <View style={styles.textView}><Text style={styles.text}>取消</Text></View>
        </TouchableHighlight> 
      </View>
    </Image>
    </View>
  );
}
// autoFocus={true}


const inputBackgrounColor = 'rgba(0,0,0,0.2)';
const styles = StyleSheet.create({
  wrapper: {
    width: windowW,
    height: toolbarHeight,
    position: 'absolute',
    top: 0,
    left: 0,	
    backgroundColor: '#F5F5F5',
  },
  backgroundImg: {
    resizeMode: 'cover',
  },
  contentWrapper: {
    width: windowW,
    height: toolbarHeight,
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 15,
  },
  inputText: {
    flex: 8,
    borderRadius: Platform.OS === 'ios' ? 20 : 45,
    backgroundColor: inputBackgrounColor,
    marginVertical: 5,
    marginRight: 15,
    paddingLeft: 30,
  },
  textView: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  text: {
    color: 'white',
    fontSize: 20,
    backgroundColor: 'transparent',
  }


});
