/**
* @class TitleWithOneBtn 
* @desc progress * 1, string * 1, button * 1
* @prop
    title string Title
    onPress function 按鈕的onPress
    btnText string button's text
*/
import React, {Component} from 'react'
import { StyleSheet, View, Text, Button, TouchableHighlight, Image} from 'react-native'
import { commonColor, commonStyles, windowW, windowH, elementWitdh, elementHeigh, alertBtnTextSize, alertTitleTextSize, borderRadius, dialogBorderRadius, fontSizeScaler } from '../../commonStyles'
import LinearGradient from 'react-native-linear-gradient';

export default class TitleOneBtn extends Component{
  render(){
    return(
      <View style={commonStyles.alertBackgroundView}>
        <View style={styles.container}>
            
          <View style={styles.top}>
          </View>
          <View style={styles.content} >
            <Text style={styles.title} numberOfLines={1}>{this.props.title}</Text>
          </View>

          <TouchableHighlight style={styles.buttonWrapper} onPress={()=> this.props.onPress()} underlayColor={'transparent'} >
            <LinearGradient style={styles.button}
              start={{x: 0.0, y: 0.5}} end={{x: 1, y: 0.5}}
              colors={commonColor.createGradient} >
                <Text style={styles.buttonText}>{this.props.btnText}</Text>
            </LinearGradient> 
          </TouchableHighlight>
        </View>
      </View>
    );
  }

}

const viewH = windowH * 1/3;
const viewW = windowW * 6/8;

const styles = StyleSheet.create({
  container: {
    height: viewH,
    width: viewW,
    alignItems: 'center',
    position: 'absolute',
    top: (windowH - viewH) / 2 , 
    left: windowW / 8,
    borderRadius: dialogBorderRadius,
    backgroundColor: 'white',
  },
  top: {
    flex: 1,
    width: viewW,
    alignItems: 'center',
    justifyContent: 'center',
  },
  content: {
    flex: 1,
    width: viewW,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  title: {
    fontSize: alertTitleTextSize,
    color: commonColor.defaultTextColor,
  },
  buttonWrapper: {
    flex: 1,
    width: viewW,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    width: elementWitdh*2/3,
    height: elementHeigh*3/4,
    borderRadius: borderRadius,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontSize: alertBtnTextSize,
    color: 'white',
  },

});
