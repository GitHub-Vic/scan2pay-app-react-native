/**
* @class DateTimePicker for android 
* @desc DateTimePicker for android 
* @prop
    certain redux's action
    cancel fun
    date Date
    minDate Date
    maxDate Date
*/
'use strict';
import React, {Component} from 'react'
import { StyleSheet, DatePickerAndroid, View} from 'react-native';
import { dateFormat, stringToDate, consoleForDebug } from '../../../lib/functions'

export default class DatePicker extends Component {

  constructor() {
    super()
    this.state = {
      date: new Date(),
    }
  }

  componentDidMount() {
    if(this.props.date) this.setState({date: stringToDate(this.props.date)})
  }

  render() {
    this.showAndroidPicker()
    return null
  }

  async showAndroidPicker(stateKey) {
    try {
      const {action, year, month, day} = await DatePickerAndroid.open(
        {
          date: this.state.date, 
          minDate: this.props.minDate ? stringToDate(this.props.minDate) : new Date(2016, 0, 0),
          maxDate: this.props.maxDate ? stringToDate(this.props.maxDate) : new Date(2116, 0, 0),
          mode: this.props.mode ? this.props.mode : 'spinner',
        }
      )
      if (action === DatePickerAndroid.dismissedAction) {
        this.props.cancel ? this.props.cancel() : {}
      } else {
        var date = new Date(year, month, day);
        this.props.certain(date)
      }
    } catch ({code, message}) {
      consoleForDebug(`Error in DatePicker '${stateKey}': `, message);
      this.props.cancel ? this.props.cancel() : {}
    }
  }

}


const styles = StyleSheet.create({
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  button: {
    margin:5,
    backgroundColor: 'white',
    padding: 15,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#ffffff',
  },
});
 