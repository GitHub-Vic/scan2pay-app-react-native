/**
* @class DateTimePicker for ios 
* @desc DateTimePicker for ios 
* @prop
    certain redux's action
    cancel fun
    date Date
    minDate Date
    maxDate Date
*/
'use strict';
import React, {Component} from 'react'
import { StyleSheet, DatePickerIOS, View, TouchableHighlight, Text} from 'react-native';
import {commonStyles, windowH, dialogBorderRadius} from '../../../commonStyles'
import { dateFormat, stringToDate } from '../../../lib/functions'

export default class DatePicker extends Component {

    constructor() {
      super()
      this.state = {
        isShow: true,
        date: new Date(),
      }
    }

    componentWillMount() {
        if(this.props.date) this.setState({date: stringToDate(this.props.date)})
    }
  
    render() {
      if(!this.state.isShow) return null
      return (
        <View style={styles.container}>
          <Text style={styles.timeText}>{this.showDate()}</Text>
          <DatePickerIOS
            style={styles.iOSPicker}
            date={this.state.date}
            maximumDate={this.props.maxDate ? stringToDate(this.props.maxDate) : null}
            minimumDate={this.props.minDate ? stringToDate(this.props.minDate) : null}
            onDateChange={(date)=> this.setState({date: date})}
            mode='date' />
          <View style={styles.btnView}>
            <TouchableHighlight style={{flex: 1, paddingRight: 5}}
              underlayColor="#a5a5a5"
              onPress={()=> this.cancel()}>
              <Text style={styles.btnText}>取消</Text>
            </TouchableHighlight>
            <TouchableHighlight style={{flex: 1, paddingLeft: 5}}
              underlayColor="#a5a5a5"
              onPress={()=> this.certain()}>
              <Text style={styles.btnText}>確定</Text>
            </TouchableHighlight>
          </View>
        </View>
      )
    }

    showDate() {
      var dateTime = dateFormat(this.state.date)
      return `${dateTime.substring(0,4)}年${dateTime.substring(4,6)}月${dateTime.substring(6,8)}日`
    }

    certain() {
      try {
        this.setState({isShow: false})
        this.props.certain ? this.props.certain(this.state.date) : {}
      }catch(e) {
      }
    }

    cancel() {
        this.setState({isShow: false})
        this.props.cancel ? this.props.cancel() : {}
    }

}

let width = 300
let hei = windowH/3.2
const styles = StyleSheet.create({
    container: {
      padding: 10,
      backgroundColor: 'white',
      borderRadius: dialogBorderRadius,
    },
    timeText: {
      width: width,
      fontSize: 25,
      textAlign: 'center',
    },
    iOSPicker: {
      height: hei,
      width: width,
      marginTop: 10,
      borderTopColor: '#ccc',
      borderTopWidth: 1
    },
    btnView: {
      flexDirection: 'row',
      width: width,
      padding: 10,
    },
    btnText: {
      fontSize: 20,
      textAlign: 'center',
      padding: 10,
    }
  });
   