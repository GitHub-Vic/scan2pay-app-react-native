/**
* @class TitleWithOneBtn 
* @desc progress * 1, string * 1, button * 1
* @prop
    title string Title
    onPress function 按鈕的onPress
    btnText string button's text
    haveBtn boolean 要不要按鈕
*/
import React, {Component} from 'react'
import { StyleSheet, View, Text, Button, TouchableHighlight, Image} from 'react-native'
// import * as Progress from 'react-native-progress'
import { commonColor, commonStyles, windowW, windowH, mainBtnHeigh, mainBtnWidth, alertBtnTextSize, alertTitleTextSize, borderRadius, dialogBorderRadius, fontSizeScaler } from '../../commonStyles'
import MainButton from  '../MainButton'

export default class TitleWithOneBtn extends Component{
  render(){
    return(
      <View style={[commonStyles.alertBackgroundView, {zIndex: 3}]}>
        <View style={styles.container}>
            
          <View style={styles.top}>
            <Image style={{height:80*fontSizeScaler, width:80*fontSizeScaler}}
              source={require('../../images/loading.gif')} />
            {/* <Progress.CircleSnail
              size={50*fontSizeScaler} 
              indeterminate={true} 
              color={commonColor.defaultTextColor} 
              direction={'clockwise'}/> */}
          </View>
          <View style={styles.content} >
            <Text style={styles.title} numberOfLines={1}>{this.props.title}</Text>
          </View>
          {this.renderBtn()}
         
        </View>
      </View>
    );
  }

  renderBtn() {
    if(this.props.haveBtn){
      return(
        <View style={styles.buttonWrapper}>
          <MainButton style={styles.button}
            btnColors={commonColor.createGradient}
            title={this.props.btnText}
            onPress={()=> this.props.onPress()} />
        </View>
      );
    }else{
      return <View style={styles.spaceWrapper} />
    }

  }
}

const viewH = windowH * 1/3;
const viewW = windowW * 6/8;

const styles = StyleSheet.create({
  container: {
    height: viewH,
    width: viewW,
    alignItems: 'center',
    position: 'absolute',
    top: (windowH - viewH) / 2 , 
    left: windowW / 8,
    borderRadius: dialogBorderRadius,
    backgroundColor: 'white',
  },
  top: {
    flex: 1,
    width: viewW,
    alignItems: 'center',
    justifyContent: 'center',
  },
  content: {
    flex: 1,
    width: viewW,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  title: {
    fontSize: alertTitleTextSize,
    color: commonColor.defaultTextColor,
  },
  buttonWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    width: mainBtnWidth,
    height: mainBtnHeigh,
    borderRadius: borderRadius,
    backgroundColor: '#2E586A',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontSize: alertBtnTextSize,
    color: 'white',
  },
  spaceWrapper: {
    flex: 1,
  }

});
