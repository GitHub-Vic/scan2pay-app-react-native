/**
* @class TradeResult 
* @desc progress * 1, string * 1, button * 1
* @prop
    onPress function 按下後的動作
    imgPath string 圖片名稱。記得將圖檔丟入ios/android(drawable) 專案內
    title string Title
    msg string Msg
*/
import React, {Component} from 'react'
import {StyleSheet, View, Text, TouchableHighlight, Image, } from 'react-native'
import {commonColor, commonStyles, windowW, windowH, mainBtnHeigh, mainBtnWidth, alertBtnTextSize, alertTitleTextSize, dialogBorderRadius, borderRadius, fontSizeScaler } from '../../commonStyles'

import LinearGradient from 'react-native-linear-gradient';

export default class TradeResult extends Component{
  render(){
    return(
      <View style={[commonStyles.alertBackgroundView, this.props.style]}>
        <View style={styles.container}>
          <View style={styles.top} />

          <View style={styles.content}>
            <Text style={styles.title} numberOfLines={1}>{this.props.title}</Text>
            <Text style={styles.msg} numberOfLines={1}>{this.props.msg}</Text>
            <TouchableHighlight style={styles.buttonWrapper} onPress={()=> this.props.onPress()} underlayColor={'transparent'}>
              <LinearGradient style={styles.button}
              start={{ x: 0.0, y: 0.5 }} end={{ x: 1, y: 0.5 }}
              colors={commonColor.createGradient} >
                <Text style={styles.buttonText}>確 定</Text>
                </LinearGradient>
            </TouchableHighlight>
          </View>
          
          <Image style={styles.menuItems}  source={{uri: this.props.imgPath}}>
            <TouchableHighlight style={{flex:1}} onPress={()=> this.props.onPress()} underlayColor={'transparent'} ><View /></TouchableHighlight>
          </Image>

        </View>
      </View>
    );
  }
}

var viewH = windowH / 2.5;
var viewW = windowW * 6/8;
var topH = viewH * 1/6;

const styles = StyleSheet.create({
  container: {
    height: viewH,
    width: viewW,
    alignItems: 'center',
    position: 'absolute',
    top: (windowH - viewH) / 2, 
    left: windowW / 8,
  },
  top: {
    height: topH,
  },
  content: {
    height: viewH * 4/5,
    width: viewW,
    borderRadius: dialogBorderRadius,
    paddingTop: viewW / 3/1.8 ,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  title: {
    flex: 1,
    fontSize: alertTitleTextSize,
    color: commonColor.defaultTextColor,
  },
  msg: {
    flex: 0.5,
    fontSize: 13 *fontSizeScaler,
    color: commonColor.defaultTextColor,
    paddingHorizontal: 10,
  },
  button: {
    width: mainBtnWidth,
    height: mainBtnHeigh,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: borderRadius,
  },
  buttonWrapper: {
    flex: 1.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontSize: alertBtnTextSize,
    color: 'white',
    fontWeight: 'bold',
  },
  menuItems: {
    height: topH * 2,
    width: topH * 2,
    position: 'absolute',
    top: 0,
    left: (viewW - topH * 2) / 2,
  },

});
