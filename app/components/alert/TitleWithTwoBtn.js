/**
* @class TitleWithOneBtn 
* @desc progress * 1, string * 1, button * 1
* @prop
    title string Title
    cancelOnPress function cancel按鈕的onPress
    certainOnPress function certain按鈕的onPress
    defaultBtn string 希望使用者點選的按鈕。 cancel | certain
*/
import React, {Component} from 'react'
import { StyleSheet, View, Text, Button, TouchableHighlight} from 'react-native'
import { commonColor, commonStyles, windowW, windowH, elementWitdh, mainBtnHeigh, alertBtnTextSize, alertTitleTextSize, borderRadius, dialogBorderRadius } from '../../commonStyles'
import LinearGradient from 'react-native-linear-gradient';

export default class TitleWithTwoBtn extends Component{
  render(){
    return(
      <View style={commonStyles.alertBackgroundView}>
        <View style={styles.container}>
          
          <View style={styles.content} >
            <Text style={styles.title} numberOfLines={2}>{this.props.title}</Text>
          </View>
          <View style={{flex:1, flexDirection:'row'}}>
            <TouchableHighlight style={styles.buttonWrapper} onPress={()=> this.props.cancelOnPress()} underlayColor={'transparent'} >
              {this.renderBtn(this.props.defaultBtn == 'cancel', '取消')}
            </TouchableHighlight>

            <TouchableHighlight style={styles.buttonWrapper} onPress={()=> this.props.certainOnPress()} underlayColor={'transparent'} >
                {this.renderBtn(this.props.defaultBtn == 'certain', '確認')}
            </TouchableHighlight>
          </View>

        </View>
      </View>
    );
  }

  renderBtn(isDefault, title) {
    if(isDefault) {
      return (
          <LinearGradient style={styles.defaultButton}
          start={{x: 0.0, y: 0.5}} end={{x: 1, y: 0.5}}
          colors={commonColor.createGradient} >
            <Text style={styles.defaultButtonText}>{title}</Text>
          </LinearGradient> 
        );
    } else {
      return (
          <LinearGradient style={styles.defaultButton}
          start={{x: 0.0, y: 0.5}} end={{x: 1, y: 0.5}}
          colors={commonColor.createGradient} >
            <View style={styles.button}>
              <Text style={styles.buttonText}>{title}</Text> 
            </View>
          </LinearGradient>   
        );
    }
   }

}

const viewH = windowH * 1/3;
const viewW = windowW * 6/8;

const styles = StyleSheet.create({
  container: {
    height: viewH,
    width: viewW,
    alignItems: 'center',
    position: 'absolute', 
    top: (windowH - viewH) / 2 , 
    left: windowW / 8,
    borderRadius: dialogBorderRadius,
    backgroundColor: 'white',
  },
  top: {
    flex: 1,
    width: viewW,
    alignItems: 'center',
  },
  content: {
    flex: 1,
    width: viewW,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: dialogBorderRadius,
  },
  title: {
    fontSize: alertTitleTextSize,
    color: commonColor.defaultTextColor,
    textAlign: 'center',
  },
  buttonWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    width: elementWitdh / 2.1 -2,
    height: mainBtnHeigh -2,
    borderRadius: borderRadius,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  defaultButton: {
    width: elementWitdh / 2.1,
    height: mainBtnHeigh,
    borderRadius: borderRadius,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontSize: alertBtnTextSize,
    color: commonColor.customerScan,
  },
  defaultButtonText: {
    fontSize: alertBtnTextSize,
    color: 'white',
  },
  spaceWrapper: {
    flex: 1,
  }

});
