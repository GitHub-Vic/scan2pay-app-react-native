/**
* @function Toolbar
* @desc Toolbar
* @prop
    title string 內容
    backOnPress function 按下返回的動作
    storePress function 按下返回的動作
    backgroundImg string 背景圖
*/
import React from 'react'
import { StyleSheet, TouchableHighlight, View, Image, Text, } from 'react-native'
import { windowW, toolbarHeight, fontSizeScaler } from '../../commonStyles'

export default function Toolbar(props) {
  return (
    <Image source={{ uri: props.backgroundImg }} style={styles.backgroundImg}>
      <View style={styles.wrapper} >
        <Text style={styles.title}> {props.title} </Text>
        <TouchableHighlight onPress={() => props.backOnPress()} style={styles.back} underlayColor={'transparent'} >
          <View><Image source={{uri: 'icon_toolbar_back'}} style={styles.img} /></View>
        </TouchableHighlight>
        <TouchableHighlight onPress={() => props.storePress()} style={styles.home} underlayColor={'transparent'} >
          <Text style={styles.storeText}>儲存</Text>
        </TouchableHighlight>
      </View>
    </Image>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    width: windowW,
    height: toolbarHeight,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  backgroundImg: {
    width: windowW,
    height: toolbarHeight,
  },
  title: {
    fontSize: 23 *fontSizeScaler,
    color: 'white',
    backgroundColor: 'transparent',
  },
  back: {
    position: 'absolute',
    left: 10,
  },
  home: {
    position: 'absolute',
    right: 10,
  },
  img: {
    resizeMode: 'contain',
    height: 30,
    width: 30,
  },
  storeText: {
    color: 'white',
    fontSize: 16 *fontSizeScaler,
  },

});
