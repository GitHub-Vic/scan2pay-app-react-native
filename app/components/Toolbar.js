/**
* @function Toolbar
* @desc Toolbar
* @prop
    title string 內容
    backOnPress function 按下返回的動作
    homeOnPress function 按下Home的動作
    backgroundImg string 背景圖
    
    actionImgPath string action的圖片
    actionOnPress function 按下Action的動作
*/
import React from 'react'
import { StyleSheet, TouchableHighlight, View, Image, Text, } from 'react-native'
import { windowW, toolbarHeight, fontSizeScaler } from '../commonStyles'

export default function Toolbar(props) {
  return (
    <Image source={{ uri: props.backgroundImg }} style={styles.backgroundImg}>
      <View style={styles.wrapper} >
        <Text style={styles.title}> {props.title} </Text>
        <TouchableHighlight onPress={() => props.backOnPress()} style={styles.back} underlayColor={'transparent'} >
          <View><Image source={{uri: 'icon_toolbar_back'}} style={styles.img} /></View>
        </TouchableHighlight>
        {props.actionOnPress ? renderActionBtn(props.actionImgPath, props.actionOnPress) : null}
        {
          !props.hideHome ? 
          <TouchableHighlight onPress={() => props.homeOnPress()} style={styles.home} underlayColor={'transparent'} >
            <View><Image source={{uri: 'icon_toolbar_home'}} style={styles.img} /></View>
          </TouchableHighlight>
          : null
        }
      </View>
    </Image>
  );
}

function renderActionBtn(imgPath, onPress) {
  if(null === imgPath) {
    return null
  }else {
    return (
      <TouchableHighlight onPress={() => onPress()} style={styles.action} underlayColor={'transparent'} >
        <View><Image source={{uri: imgPath}} style={styles.img} /></View>
      </TouchableHighlight>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    width: windowW,
    height: toolbarHeight,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  backgroundImg: {
    width: windowW,
    height: toolbarHeight,
  },
  title: {
    fontSize: 23 *fontSizeScaler,
    color: 'white',
    backgroundColor: 'transparent',
  },
  back: {
    position: 'absolute',
    left: 10,
  },
  home: {
    position: 'absolute',
    right: 10,
  },
  img: {
    resizeMode: 'contain',
    height: 30,
    width: 30,
  },
  action: {
    position: 'absolute',
    right: 60,
  },

});
