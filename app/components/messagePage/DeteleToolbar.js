/**
* @function DeleteToolbar
* @desc DeleteToolbar
* @prop
    backgroundImg string 背景圖
    options [string] 
    value string
    onChange function
    cancelOnPress function
*/
import React from 'react'
import { StyleSheet, TouchableHighlight, View, Image, Text, } from 'react-native'
import { windowW, toolbarHeight } from '../../commonStyles'

import Toggle from './Toggle'

export default function DeleteToolbar(props) {
  return (
    <Image source={{ uri: props.backgroundImg }} style={styles.backgroundImg}>
      <View style={styles.wrapper} >
        <Toggle
          value={props.value}
          options={props.options}
          onChange={(option) => props.onChange(option)} />
        <TouchableHighlight style={styles.cancel} 
          onPress={() => props.cancelOnPress()}
          underlayColor={'transparent'} >
          <View><Image source={{uri: 'icon_message_delete_cancel'}} style={styles.img} /></View>
        </TouchableHighlight>

      </View>
    </Image>
  );
}


const styles = StyleSheet.create({
  wrapper: {
    width: windowW,
    height: toolbarHeight,
    alignItems: 'center',
    flexDirection: 'row',
  },
  backgroundImg: {
    width: windowW,
    height: toolbarHeight,
  },
  allReadOutLayer: {
    position: 'absolute',
    left: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5A32D',
    borderColor: 'white',
    borderWidth: 2,
    borderRadius: 20,
    padding: 8,
  },
  allReadText: {
    color: 'white',
    fontSize: 15,
  },
  cancel: {
    width: windowW * 0.8/5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  img: {
    resizeMode: 'contain',
    height: 30,
    width: 30,
  },

});
