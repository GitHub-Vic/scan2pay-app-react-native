/**
* @class MessageList
* @desc 訊息通知 List
* @prop
    data array 要顯示的資料們
    onPress function item's onPress（回傳orderId)
    isDeleteMode boolean 是否再刪除模式
    listRef function
*/
import React, {Component} from 'react'
import {StyleSheet, View, FlatList} from 'react-native'

import {commonColor} from '../../commonStyles'
import MessageItemView from './MessageItemView'

export default class MessageList extends Component {
  shouldComponentUpdate(nextProps) {
    if(nextProps.data === this.props.data) {
      return false;
    }
    return true;
  }
  
  render() {
    return(
      <FlatList
        ref={(ref)=> this.props.listRef(ref)}
        data={this.props.data}
        keyExtractor={(item, index) => index}
        renderItem={({item, index}) => <MessageItemView item={item} 
                                         onPress={()=> this.props.onPress(item.orderId)}
                                         isDeleteMode={this.props.isDeleteMode} />}
        ItemSeparatorComponent={() => this.separator()} 
        getItemLayout={(data, index) => (
          {length: 131, offset: 131 * index, index}
        )}
       />
    );
  }

  separator() {
    return <View style={styles.separator} />
  }

}

const styles = StyleSheet.create({
  separator: {
    height: 1,
    backgroundColor: commonColor.listViewDividerColor,
  },
  footerView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});