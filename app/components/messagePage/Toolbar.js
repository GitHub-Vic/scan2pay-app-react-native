/**
* @function Toolbar
* @desc Toolbar For Message
* @prop
    backgroundImg string 背景圖
    allReadOnPress function
!not yet    dongleOnPress function 按下的動作
    deteleMode function 按下的動作
    homeOnPress function 按下的動作
*/
import React from 'react'
import { StyleSheet, TouchableHighlight, View, Image, Text, } from 'react-native'
import { windowW, toolbarHeight } from '../../commonStyles'

export default function Toolbar(props) {
  return (
    <Image source={{ uri: props.backgroundImg }} style={styles.backgroundImg}>
      <View style={styles.wrapper} >
        <Text style={styles.title}>訊息通知</Text>
        <TouchableHighlight onPress={() => props.allReadOnPress()} style={styles.allReadOutLayer} underlayColor={'transparent'} >
          <View>
            <Text style={styles.allReadText}>全部已讀</Text>
          </View>
        </TouchableHighlight>

        <View  style={styles.dongle}/>

        <TouchableHighlight onPress={() => props.deteleMode()} style={styles.detele} underlayColor={'transparent'} >
          <View><Image source={{uri: 'icon_toolbar_delete'}} style={styles.img} /></View>
        </TouchableHighlight>

        <TouchableHighlight onPress={() => props.homeOnPress()} style={styles.home} underlayColor={'transparent'} >
          <View><Image source={{uri: 'icon_toolbar_home'}} style={styles.img} /></View>
        </TouchableHighlight>

      </View>
    </Image>
  );
}


const styles = StyleSheet.create({
  wrapper: {
    width: windowW,
    height: toolbarHeight,
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  backgroundImg: {
    width: windowW,
    height: toolbarHeight,
  },
  title: {
    fontSize: 25,
    color: 'white',
    backgroundColor: 'transparent',
  },
  allReadOutLayer: {
    position: 'absolute',
    left: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5A32D',
    borderColor: 'white',
    borderWidth: 2,
    borderRadius: 20,
    padding: 8,
  },
  allReadText: {
    color: 'white',
    fontSize: 15,
  },
  home: {
    position: 'absolute',
    right: 10,
  },
  img: {
    resizeMode: 'contain',
    height: 30,
    width: 30,
  },
  detele: {
    position: 'absolute',
    right: 50,
  },
  dongle: {
    position: 'absolute',
    right: 90,
  },

});
