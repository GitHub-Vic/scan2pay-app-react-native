import React from 'react'
import { StyleSheet, View, Text, } from 'react-native'
import { windowW, borderRadius, fontSizeScaler } from '../../commonStyles'

export default function Badge(props) {
  return (
    <View style={{flex:1}}>
      {props.menuBtn}
        {
          props.count == 0 ? null
            : <View style={styles.back}>
                <Text style={styles.text}>{props.count > 99 ? '99+' : props.count}</Text>
              </View>
        }
    </View>
  )
}

const positionLeft = windowW/3/1.8
const styles = StyleSheet.create({
  back: {
    position: 'absolute',
    left: positionLeft,
    top: 0,
    paddingVertical: 3,
    paddingHorizontal: 6,
    borderRadius: borderRadius,
    backgroundColor: 'red',
  },
  text: {
    color: 'white',
    fontSize: 13 *fontSizeScaler,
  }
})