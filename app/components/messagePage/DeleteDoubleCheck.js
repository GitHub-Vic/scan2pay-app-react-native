import React, {Component} from 'react'
import { StyleSheet, View, Text, TouchableHighlight} from 'react-native'
import { commonColor, commonStyles, windowW, windowH, elementWitdh, mainBtnHeigh, alertBtnTextSize, alertTitleTextSize, borderRadius, dialogBorderRadius } from '../../commonStyles'

export default class DeleteDoubleCheck extends Component{
  render(){
    return(
      <View style={commonStyles.alertBackgroundView}>
        <View style={styles.container}>
          
          <View style={styles.content} >
            <Text style={styles.title} numberOfLines={2}>是否刪除？</Text>
          </View>
          <View style={{flex:1, flexDirection:'row'}}>
            <TouchableHighlight style={styles.buttonWrapper} onPress={()=> this.props.cancelOnPress()} underlayColor={'transparent'} >
              <View style={styles.button} >
                <Text style={styles.buttonText}>取消</Text> 
              </View>
            </TouchableHighlight>

            <TouchableHighlight style={styles.buttonWrapper} onPress={()=> this.props.certainOnPress()} underlayColor={'transparent'} >
              <View style={styles.defaultButton}>
                <Text style={styles.defaultButtonText}>確定</Text>
              </View> 
            </TouchableHighlight>
          </View>

        </View>
      </View>
    );
  }

}

const viewH = windowH * 1/3;
const viewW = windowW * 6/8;

const styles = StyleSheet.create({
  container: {
    height: viewH,
    width: viewW,
    alignItems: 'center',
    position: 'absolute', 
    top: (windowH - viewH) / 2 , 
    left: windowW / 8,
    borderRadius: dialogBorderRadius,
    backgroundColor: 'white',
  },
  top: {
    flex: 1,
    width: viewW,
    alignItems: 'center',
  },
  content: {
    flex: 1,
    width: viewW,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: dialogBorderRadius,
  },
  title: {
    fontSize: alertTitleTextSize,
    color: commonColor.defaultTextColor,
  },
  buttonWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    width: elementWitdh / 2.1 -2,
    height: mainBtnHeigh -2,
    borderRadius: borderRadius,
    borderColor: commonColor.defaultRedColor,
    borderWidth: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  defaultButton: {
    width: elementWitdh / 2.1,
    height: mainBtnHeigh,
    borderRadius: borderRadius,
    backgroundColor: commonColor.defaultRedColor,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontSize: alertBtnTextSize,
    color: commonColor.defaultRedColor,
  },
  defaultButtonText: {
    fontSize: alertBtnTextSize,
    color: 'white',
  },
  spaceWrapper: {
    flex: 1,
  }

});
