/**
* @class MessageItemView
* @desc 訊息通知List's item
* @prop
    item 交易紀錄中的一個JsonObject
    onPress function 點擊item （回傳orderId)
    isDeleteMode boolean 是否再刪除模式
*/

import React, {PureComponent} from 'react'
import {StyleSheet, View, Text, Image, TouchableHighlight} from 'react-native'

import {commonColor, windowW,} from '../../commonStyles'
import {getValueByKeyFromJsonObject } from '../../lib/functions'
import * as formatFunctions from '../../lib/formatFunctionsForShow'

var fontWeight = 'bold';
var backColor = commonColor.messageNotReadBackgroundColor;
export default class MessageItemView extends PureComponent {
  
  shouldComponentUpdate(nextProps) {
    if(nextProps.item === this.props.item) {
      return false;
    }
    return true;
  }

  render() {
    return(
      <TouchableHighlight style={{height: 130, backgroundColor: 'white'}}
        onPress={() => {this.props.onPress()}} 
        underlayColor = 'transparent' >
        {this.renderItem()}
      </TouchableHighlight>
    );
  }
  
  renderItem() {
    const item = this.props.item;
    var index = this.props.index;
    var checkboxUri = item.isCheck ? 'icon_message_checkbox_select' : 'icon_message_checkbox_notselect';
    var checkboxStyle = this.props.isDeleteMode ? {height: 25, width: 25} : {height: 0, width: 0};
    if(item.isRead) {
      fontWeight = 'normal';
      backColor = 'white';
    }else {
      fontWeight = 'bold';
      backColor = commonColor.messageNotReadBackgroundColor;
    }

    
    return(
      <View style={[styles.wrapper, {backgroundColor: backColor}]}>
        <View style={styles.indexContent}>
          <Text style={[styles.valueText,{fontWeight: fontWeight}]}>{item.index}</Text>
        </View>

        <View style={styles.content}>
          <View style={styles.row}>
            <Image style={styles.timeImg} source={{uri: 'icon_message_item_time'}} />
            <Text style={[styles.timeText,{fontWeight: fontWeight}]}>{formatFunctions.formatTime(item.date)}</Text>
          </View>
          
          {this.renderRow(`商品名稱: `, item.description)}
          {this.renderRow(`交易單號: `, formatFunctions.formatSystemOrderId(getValueByKeyFromJsonObject(item, 'systemOrderId')))}
          {this.renderRow(`訂單編號: `, formatFunctions.formatSystemOrderId(item.orderId))}
          {this.renderRow(`付款類別: `, item.zhifuType)}
          
        </View>

        <View style={styles.moneyContent}>
          <Text style={[styles.money,{fontWeight: fontWeight}]}>{`NT$ ${formatFunctions.formatMoneyToCama(item.money)}`}</Text>
          <Image style={checkboxStyle} source={{uri: checkboxUri}} />
        </View>
      </View>
    );
  }

  renderRow(key, value) {
    return (
      <View style={styles.row}>
        <Text style={[styles.keyText,{fontWeight: fontWeight}]}>{key}</Text>
        <Text style={[styles.valueText,{fontWeight: fontWeight}]}>{value}</Text>
      </View>
    );
  }

}


var totlaW = windowW - 20;
const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    flexDirection: 'row',
    padding: 10,
  },
  row: {
    flexDirection: 'row',
  },
  timeImg: {
    height: 20,
    width: 20,
  },
  timeText: {
    color: commonColor.timeTextColor,
  },
  indexContent: {
    width: totlaW * 1.5/10,
    justifyContent: 'center',
  },
  index: {
    width: totlaW * 1.5/10,
    color: commonColor.defaultTextColor,
    textAlign: 'center',
  },
  content: {
    width: totlaW * 5.5/10,
    paddingLeft: 5,
    justifyContent: 'center'
  },
  moneyContent: {
    flexDirection: 'row',
    width: totlaW * 3/10,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  money: {
    color:commonColor.customerScan,
    marginRight: 5,
  },
  keyText: {
    color: commonColor.defaultTextColor,
  },
  valueText: {
    width: totlaW * 5.5/10 * 2/3 - 5,
    color: commonColor.defaultTextColor,
  },
});



