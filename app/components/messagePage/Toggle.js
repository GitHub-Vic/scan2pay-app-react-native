/**
* @function Toggle
* @desc Toggle For Message
* @prop
    options [string] 選項們
    value string 現在選到的選項
    onChange
*/
import React, {Component} from 'react'
import {StyleSheet, View, Text, TouchableOpacity} from 'react-native'
import {windowW} from '../../commonStyles'

export default class Toggle extends Component {

  renderOption = (option) => {
    const {value} = this.props
    return (
      <TouchableOpacity key={option}
        style={[styles.option, option === value && styles.activeOption]}
        onPress={()=> this.props.onChange(option)} >
        <Text style={styles.text}>
            {option}
        </Text>
      </TouchableOpacity>
    )
  }

  render() {
    const {options} = this.props
    return (
      <View style={styles.optionsContainer}>
        {options.map(this.renderOption)}
      </View>
    );
  }
}

const totalWidth = windowW * 4.2/5
const eachWidth = totalWidth * 1/4
const styles = StyleSheet.create({
  optionsContainer: {
    width: totalWidth,
    flexDirection: 'row',
    marginLeft: 10,
  },
  option: {
    width: eachWidth,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  activeOption: {
    backgroundColor: '#F1C11B',
    borderColor: 'white',
    borderWidth: 2,
    borderRadius: 20,
    padding: 5,
  },
  text: {
    fontSize: 17,
    color: 'white',
  }
});