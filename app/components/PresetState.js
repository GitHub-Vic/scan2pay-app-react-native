import React, { Component } from 'react'
import { StyleSheet, Platform, View, Image,} from 'react-native'
import { commonColor, windowH, windowW, } from '../commonStyles'
import * as Progress from 'react-native-progress'

export default class PresetState extends Component {

  render() {
    return (
     <Image source={require('../images/presetStateBackground.jpg')} style={styles.backgroundImg}>
      <View style={styles.view}>
        <View style={styles.load}>
          <Image source={require('../images/presetStateLogo.png')} style={styles.logo}/>
          <Progress.CircleSnail size={circleProgressSize} indeterminate={true} color={'#65B32C'} direction={'clockwise'}/>
          <Image source={require('../images/presetStateText.png')} style={styles.text}/>
        </View>
        
        <Image source={require('../images/presetStateAppName.png')} style={styles.appName}/>
      </View>
     </Image>
    )
  }
  
}

const logoHW = windowW/4
const loadviewTop = windowH/2 - logoHW
const circleProgressSize = logoHW *1.5

const styles = StyleSheet.create({
  backgroundImg:{ 
    height: Platform.OS === 'ios'? windowH-30 : windowH,
    marginTop: Platform.OS === 'ios'? 30 : 0,
    width: windowW,
  },
  view: {
    flex: 1,
    alignItems: 'center',
  },
  load: {
    width: windowW,
    position: 'absolute',
    top: Platform.OS === 'ios'? loadviewTop - 30 : loadviewTop ,
    alignItems: 'center',
  },
  logo: {
    position: 'absolute',
    top: logoHW*0.5/2,
    left: windowW/2 - logoHW/2,
    height: logoHW,
    width: logoHW,
    resizeMode: 'contain',
  },
  text: {
    marginTop: 10,
    width: windowW/3,
    resizeMode: 'contain',
  },
  appName: {
    height: windowH /10,
    width: windowW * 1.5/3,
    position: 'absolute',
    bottom: windowH / 10,
    resizeMode: 'contain',
  },
  circleProgress: {
    position: 'absolute',
    top: 5,
    left: windowW/2 - circleProgressSize/2,
  },
  
});
  