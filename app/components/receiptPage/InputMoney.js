// !!!!!!!!!要限制只能輸入數字 http://blog.csdn.net/fox_wei_hlz/article/details/70146831 !!!!
// !!!!!!!!!好像限制是數字了.. 但開頭不能為0.. 還有標點符號..
/**
* @class InputMoney
* @desc 收款頁面 - 輸數消費金額
* @prop
    onTextChange function 輸入框內的值變動
    onSubmitEditing function 當按下鍵盤的enter時
    id function reference此TextInput
    unitColor string 顏色
    value string 輸入框內要顯示的值
    underLineColor string 顏色
*/
import React from 'react'
import{StyleSheet, TextInput, View, Text, } from 'react-native'

import {commonColor, elementWitdh, elementHeigh, windowH, fontSizeScaler } from '../../commonStyles'
import LinearGradient from 'react-native-linear-gradient'

export default function InputMoney(props){
  return(
      <View style={styles.wrapper}>
        <Text style={[styles.unit, {color: props.unitColor}]}>NT$</Text>
        <TextInput
          style={styles.inputText}
          multiline={false}
          maxLength={6}
          underlineColorAndroid='transparent'
          autoFocus={true}
          keyboardType={'numeric'}
          placeholder= {props.placeholder ? props.placeholder : '收款金額'}
          placeholderTextColor={commonColor.hintTextColor}
          onChangeText={(text) => props.onTextChange(text)}
          value={props.value}
          ref={props.id}
        />
        <LinearGradient style={styles.underLine}
          start={{x: 0.0, y: 0.5}} end={{x: 1, y: 0.5}}
          colors={props.underLineColor} />
      </View>
  );

  // onSubmitEditing={props.onSubmitEditing}
}


const styles = StyleSheet.create({
  wrapper: {
    height: windowH * 1/8,
    width: elementWitdh,
  },
  unit: {
    position: 'absolute',
    bottom: 15,
    left: 10,
    fontSize: 25 *fontSizeScaler,
  },
  inputText: {
    height: windowH * 1/8,
    paddingLeft: 80,
    paddingTop: 20,
    paddingRight: 0,
    fontWeight: 'bold',
    fontSize: 27 *fontSizeScaler,
    color: commonColor.defaultTextColor,
  },
  underLine: {
    height: 2,
  }
});
