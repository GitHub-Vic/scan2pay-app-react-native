/**
* @class customerScan 
* @desc 收款頁面 - 顯示QRCode
* @prop
    props 
    text string qrcode's content
    customerScanNowStatus string 交易狀態
    startLoading function 掃碼後的loading提示框
*/
import React, { Component } from 'react'
import { StyleSheet, View, Text} from 'react-native'
import * as Progress from 'react-native-progress'

import { URL_INTELLA_GENERAL } from '../../config'
import { getNowTime, mapToJsonString } from '../../lib/functions'
import { SERVICE_TYPE } from '../../constants/actionTypes'
import { commonColor, windowW, windowH, fontSizeScaler } from '../../commonStyles'
import QRCode from 'react-native-qrcode'

export default class CustomerScan extends Component {

  constructor() {
    super();
    this.state = {
      fetchQRCodeContent: true,
      orderId: '',
    }
  }

  componentDidMount() {
    //generateOrderId here and waiting for 
    // new orderId in receiptPage's componentWillReceiveProps (nextProps) function
    this.props.actions.doGenerateOrderId(this.props.merchId);
  }
  
  componentWillReceiveProps (nextProps) {
    try {
      if(nextProps.orderId && this.state.fetchQRCodeContent)
        if(nextProps.orderId !== this.state.orderId)
          this.generateQRCodeContent(nextProps.orderId);
    }catch(e) {
    }
  }

  render() {
    var qrcodeText = this.props.text ? this.props.text : '' ;
    var qrcodeShowSize = windowW * 2/3 > windowH * 3.5/4 * 1.5/3.3 ? windowH * 3.5/4 * 1.5/3.3 : windowW * 2/3;
    var qrcodeSize = qrcodeText.length > 5 ? qrcodeShowSize : 0;
      return(
        <View styel={styles.wrapper}>
          <View style={styles.qrcode}>
            <QRCode      
              value={qrcodeText}
              size={qrcodeSize}
              bgColor='black'
              fgColor='white' />
          </View>
          <View style={styles.hintContent}>
            <Text style={{color: commonColor.hintTextColor, fontSize: 13*fontSizeScaler}}>讓消費者掃描螢幕上的QRcode條碼</Text>
          </View>
          <View style={styles.tradeStatusWrapper}>
            <Progress.CircleSnail
              size={this.props.customerScanNowStatus.length > 0 ? 30*fontSizeScaler : 0} 
              indeterminate={true} 
              color={commonColor.defaultRedColor} 
              direction={'clockwise'}/>
            <Text style={styles.tradeStatusText}
              ref={(ref)=> this.tradeStatusText = ref }>
              {this.props.customerScanNowStatus}
            </Text>
          </View>
        </View>
      )
  }

  generateQRCodeContent(nextOrderId) {
    this.props.startLoading();
    this.setState({orderId: nextOrderId});
    var requestData = new Map();
    requestData.set("Method", "00000");
    requestData.set("MchId", this.props.merchId);
    requestData.set("CreateTime", getNowTime()); 
    requestData.set("StoreOrderNo", nextOrderId);
    requestData.set("DeviceInfo", "skb0001");
    requestData.set("Body", "RN Item");
    requestData.set("TotalFee", this.props.money);
    requestData.set("TradeKey", this.props.tradeKey)
    requestData.set("ServiceType", SERVICE_TYPE.CUSTOMER_SCAN);
    requestData.set("Cashier", 'cashier');
    var json = mapToJsonString(requestData);
    this.props.actions.fetchIntellaApi(SERVICE_TYPE.CUSTOMER_SCAN, URL_INTELLA_GENERAL, json);
  }


}



const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    width: windowW * 2/3,
  },
  qrcode: {
    flex: 3, 
    alignItems: 'center',
    justifyContent: 'center',
  },
  hintContent: {
    flex: 1,
    paddingTop: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  tradeStatusWrapper: {
    flex: 1,
    flexDirection: 'row',
    marginLeft: 20,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  tradeStatusText: {
    color: commonColor.defaultRedColor, 
    fontSize: 18 *fontSizeScaler, 
    textAlign: 'center',
  },
  
});