/**
* @class customerScan 
* @desc 收款頁面 - 顯示QRCode
* @prop
    props 
    startLoading function 掃碼後的loading提示框
*/
import React, { Component } from 'react'
import { StyleSheet, View, Text} from 'react-native'

import { URL_INTELLA_GENERAL, URL_INTELLA_ORDER } from '../../config'
import { getNowTime, mapToJsonString } from '../../lib/functions'
import { SERVICE_TYPE } from '../../constants/actionTypes'
import { commonColor, windowW, fontSizeScaler } from '../../commonStyles'
import Camera from 'react-native-camera'

export default class CustomerBeScanned extends Component {

  constructor() {
    super();
    this.state = {
      scanBool: true,
    }
  }

  render() {
      return(
        <View styel={styles.wrapper}>
          <Camera style={styles.camera} 
            onBarCodeRead={(result)=> this.onBarCodeRead(result)} />
          <View style={styles.hintContent}>
            <Text style={{color: commonColor.hintTextColor, fontSize: 13*fontSizeScaler}}>螢幕會自動偵測行動條碼</Text>
          </View>
        </View>
      )
  }

  async onBarCodeRead(result) {
    if(!this.state.scanBool)
      return;

    this.props.startLoading()
    this.setState({scanBool: false})
    const response = await fetch(`${URL_INTELLA_ORDER}/createAppOrderNo/${this.props.merchId}`);
    responseJson = await response.json()
    if (!responseJson.msg) {
      this.props.actions.generateOrderId(responseJson.orderNo)
      var requestData = new Map()
      requestData.set("Method", "00000")
      requestData.set("MchId", this.props.merchId)
      requestData.set("CreateTime", getNowTime());
      requestData.set("StoreOrderNo", this.props.orderId)
      requestData.set("DeviceInfo", "skb0001")
      requestData.set("Body", "RN Item")
      requestData.set("TotalFee", this.props.money)
      requestData.set("ServiceType", SERVICE_TYPE.CUSTOMER_BE_SCANNED)
      requestData.set("TradeKey", this.props.tradeKey)
      requestData.set("AuthCode", result.data)
      requestData.set('Cashier', this.props.merchId)
      var json = mapToJsonString(requestData)
      this.props.actions.fetchIntellaApi(SERVICE_TYPE.CUSTOMER_BE_SCANNED, URL_INTELLA_GENERAL, json)
    }
  }
}


const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  camera: {
    flex: 4, 
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'black',
    width: windowW,
  },
  hintContent: {
    flex: 1,
    paddingTop: 15,
    alignItems: 'center'
  },
  
});
  
  