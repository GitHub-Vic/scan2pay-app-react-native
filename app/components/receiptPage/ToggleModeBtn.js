/**
* @class ToggleModeBtn
* @desc 收款 切換收款模式
* @prop
    wrapperStyle style
    onPress function 按下後的動作
    textColor string backgroundColor 
    imgPath string 圖片名稱。記得將圖檔丟入ios/android(drawable) 專案內
    text string 內容
*/
import React from 'react'
import { StyleSheet, TouchableHighlight, View, Image, Text, } from 'react-native'

import { commonColor, windowH, fontSizeScaler} from '../../commonStyles'

export default function ToggleModeBtn(props){
  return(
    <TouchableHighlight 
      style={[styles.wrapper, props.wrapperStyle]} 
      underlayColor={'transparent'} 
      onPress={() => props.onPress()} >
      <View 
        style={[styles.wrapper, props.style]} >
        { createImage(props.imgPath) }
        <Text style={[styles.text,{color: props.textColor}]}> {props.text} </Text>
      </View>
    </TouchableHighlight>
  );
 }

function createImage(imgPath){
   if(imgPath)
     return <Image style={styles.menuItems} source={{uri: imgPath}} />
   else
     return null;
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#E5E5E4'
  },
  menuItems: {
    resizeMode: 'contain',
    height: 40,
    width: 40,
  },
  text: {
    fontSize: 14 *fontSizeScaler,
    textAlign: 'center',
  },
});
  