import React, {Component} from 'react'
import { StyleSheet, View, Image, TouchableOpacity, TouchableWithoutFeedback , Keyboard} from 'react-native'
import {  commonStyles, windowW, windowH, dialogBorderRadius } from '../../commonStyles'
import { consoleForDebug } from '../../lib/functions';



export default class PureWhiteAlert extends Component{
    static defaultProps = {
      hasKeyborad: true,
      containerStyle:{},
      topStyle: {},
      topView: null,
      viewH: windowH * 1/2,
      contentStyle: {},
      contentView: null,
      bottomStyle: {},
      bottomView: null,
      cancelBtnStyle: {},
      onImgPress: ()=>consoleForDebug(`onImgPress`),
      imgPath: 'icon_refund_cancel',
      imgSize: windowW * 1.3/10,
      imgStyle: {},
    }

    render(){
      cancelSize = this.props.imgSize
      viewH = this.props.viewH
      var container = this.props.hasKeyborad ? {top: cancelSize} : {top: (windowH - viewH + cancelSize/2) / 2}
      return(
        <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss()}>
          <View style={commonStyles.alertBackgroundView}>
            <View style={[styles.container, container,{height: viewH},this.props.containerStyle]}>
              <View style={[styles.top, this.props.topStyle]}>
                {this.props.topView}
              </View>
              <View style={[styles.content, this.props.contentStyle]} >
                {this.props.contentView}
              </View>
              <View style={[styles.bottom, this.props.bottomStyle]} >
                {this.props.bottomView}
              </View>
            </View>
            {this.renderCancel()}
          </View>
        </TouchableWithoutFeedback>
      );
    }

    renderCancel() {
      var cancelWithKeyborad = this.props.hasKeyborad ? {top: cancelSize/2} : {}
      return (
        <TouchableOpacity style={[styles.cancel, cancelWithKeyborad, this.props.imgStyle]} 
          onPress={() => this.props.onImgPress()} 
          activeOpacity={0.9} >
          <View >
              <Image style={{height:cancelSize,width:cancelSize}} 
              source={{uri: this.props.imgPath}}/> 
          </View>
        </TouchableOpacity>
      );
    }
  
  }



var viewH = windowH * 1/2
var posTop = (windowH - (windowH * 1/2)) / 2
const viewW = windowW * 6/8
var cancelSize = windowW * 1.3/10

const styles = StyleSheet.create({
  container: {
    height: viewH,
    width: viewW,
    alignItems: 'center',
    position: 'absolute',
    top: posTop, 
    left: windowW / 8,
    borderRadius: dialogBorderRadius,
    backgroundColor: 'white',
  },
  top: {
    flex: 1,
    width: viewW,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: cancelSize/2,
  },
  content: {
    flex: 2,
    width: viewW,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: cancelSize/2,
  },
  bottom: {
    flex: 1,
    width: viewW,
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingBottom: 10,
    paddingHorizontal: cancelSize/2,
  },
  cancel: {
    position: 'absolute',
    top: (windowH - viewH) / 2 - cancelSize/2.5, 
    left: windowW / 8 - cancelSize/2.5,
    width: cancelSize,
    height: cancelSize,
  },

});