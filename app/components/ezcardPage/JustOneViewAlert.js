import React, {Component} from 'react'
import { StyleSheet, View, Text, Image} from 'react-native'
import { commonColor, commonStyles, windowW, windowH, elementWitdh, elementHeigh, alertBtnTextSize, alertTitleTextSize, borderRadius, dialogBorderRadius, fontSizeScaler } from '../../commonStyles'

import { consoleForDebug } from '../../lib/functions'
import PureWhiteAlert from './PureWhiteAlert'
import MainButton from  '../MainButton'

export default class JustOneViewAlert extends Component{

    static defaultProps = {
        onPress: ()=>{},
        imgPath: null,
        text: '',
    }

    render(){
        return(
            <PureWhiteAlert 
                onImgPress = {()=>this.props.onPress()}
                contentView = {this.renderContentView()}
                bottomView = {this.renderBtn()}
                hasKeyborad = {false} />
        )
    }
    
    renderContentView() {
        return (
            <View style={{flexDirection:'row', alignItems: 'center'}}>
                {this.props.imgPath ?
                    <Image style={styles.img} source={{uri: this.props.imgPath}}/> 
                    : null
                }
                <Text style={styles.text}>{this.props.text}</Text>
            </View>
        )
    }

    renderBtn() {
        return (
            <MainButton 
                btnColors={commonColor.createGradient}
                title={'確 定'}
                onPress={()=> this.props.onPress()} />
        )
    }


}

const styles = StyleSheet.create({
    text: {
        fontSize: 18* fontSizeScaler,
        paddingLeft: 5,
    },
    img: {
        height: 18* fontSizeScaler,
        width: 18* fontSizeScaler
    },
})
