import React, {Component} from 'react'
import { StyleSheet, View, Text, Button, TouchableHighlight, Image} from 'react-native'
import { commonColor, commonStyles, windowW, windowH, elementWitdh, elementHeigh, alertBtnTextSize, alertTitleTextSize, borderRadius, dialogBorderRadius, fontSizeScaler } from '../../commonStyles'

import { consoleForDebug } from '../../lib/functions'
import PureWhiteAlert from './PureWhiteAlert'
import InputMoney from  '../receiptPage/InputMoney'
import MainButton from  '../MainButton'

export default class InputMoneyAlert extends Component{

    static defaultProps = {
        title: '???',
        onTextChange: (text) => {consoleForDebug(text)},
        inputValue: '',
        onSubmitEditing: ()=>{},
        onImgPress: ()=>{},
        onPressCertenBtn: ()=>{},
    }

    render(){
        return(
            <PureWhiteAlert 
                onImgPress = {()=>this.props.onImgPress()}
                topView = {this.renderTitle()}
                contentView = {this.renderInput()}
                bottomView = {this.renderBtn()} />
        )
    }

    renderTitle() {
        return (
            <Text style={styles.titleText} numberOfLines={2}>請輸入
                <Text style={[styles.titleText, {color:'red'}]}>{this.props.title}</Text>
            金額</Text>
        )
    }

    renderInput() {
        return (
            <InputMoney 
                onTextChange={(input) => this.props.onTextChange(input)}
                onSubmitEditing={this.props.onSubmitEditing()}
                id={(c) => this.input = c}
                unitColor={commonColor.customerScan}
                underLineColor={commonColor.createGradient}
                value={this.props.inputValue}
                placeholder={`${this.props.title}金額`}
            />
        )
    }

    renderBtn() {
        return (
            <MainButton 
                btnColors={commonColor.createGradient}
                title={'確 定'}
                onPress={()=> this.props.onPressCertenBtn()} />
        )
    }


}

const styles = StyleSheet.create({
    titleText: {
        fontSize: 20* fontSizeScaler,
    },
})
