import React, {Component} from 'react'
import { StyleSheet, View, Text, Button, TouchableHighlight, Image} from 'react-native'
import { commonColor, commonStyles, windowW, windowH, elementWitdh, elementHeigh, alertBtnTextSize, alertTitleTextSize, borderRadius, dialogBorderRadius, fontSizeScaler } from '../../commonStyles'

import { consoleForDebug } from '../../lib/functions'
import PureWhiteAlert from './PureWhiteAlert'
import InputIconPassword from  '../InputIconPassword'
import MainButton from  '../MainButton'

export default class RefundPasswordAlert extends Component{

    static defaultProps = {
        title: '???',
        onTextChange: (text) => {consoleForDebug(text)},
        inputValue: '',
        onImgPress: ()=>{},
        onPressCertenBtn: ()=>{},
    }

    render(){
        return(
            <PureWhiteAlert 
                onImgPress = {()=>this.props.onImgPress()}
                topView = {this.renderTitle()}
                contentView = {this.renderInput()}
                bottomView = {this.renderBtn()} />
        )
    }

    renderTitle() {
        return (
            <Text style={styles.titleText} numberOfLines={2}>請輸入
                <Text style={[styles.titleText, {color:'red'}]}>{this.props.title}</Text>
            {'\n'}<Text style={styles.msgText}>退款金額將自動存入悠遊卡</Text>
            </Text>
        )
    }

    renderInput() {
        return (
            <InputIconPassword 
                textInputRef={()=> {}}
                password={true} 
                icon={'icon_inputtext_password'}
                onTextChange={(text) => this.props.onTextChange(text)}
                inputValue={this.props.inputValue}
                hintText={'退款密碼'} />
        )
    }

    renderBtn() {
        return (
            <MainButton 
                btnColors={commonColor.createGradient}
                title={'確 定'}
                onPress={()=> this.props.onPressCertenBtn()} />
        )
    }


}

const styles = StyleSheet.create({
    titleText: {
        fontSize: 20* fontSizeScaler,
        textAlign: 'center',
    },
    msgText: {
        fontSize: 14* fontSizeScaler,
    },
})
