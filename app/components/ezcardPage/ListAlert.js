import React, {Component} from 'react'
import { StyleSheet, View, Text, Button, TouchableHighlight, Image} from 'react-native'
import PureWhiteAlert from './PureWhiteAlert'
import { windowH, fontSizeScaler, commonColor, dialogBorderRadius, windowW } from '../../commonStyles'

import LinearGradient from 'react-native-linear-gradient'
import OrderQueryList from './OrderQueryList'

export default class ListAlert extends Component{

    static defaultProps = {
        recordDatas:[],
        renderTitle: null,
        onCancelPress: ()=>{},
        onClickRecoedItem:()=>{},
        isSelected: false,
        serviceType:''
    }

    render(){
        var bottomStyle = this.props.onPressCertenBtn ? {flex:0, height: btnH, paddingBottom:0,} : {flex:0, height: 0} 
        return(
            <PureWhiteAlert 
                viewH = {windowH*5/6}
                onImgPress = {()=>this.props.onCancelPress()}
                topView = {this.props.renderTitle ? this.props.renderTitle() : null}
                contentStyle = {{flex: 4, paddingHorizontal: 10}}
                contentView = {this.renderInput()}
                bottomStyle = {bottomStyle}
                bottomView = {this.props.onPressCertenBtn ? this.renderBtn() : null} />
        )
    }

    renderInput() {
        return (
            <OrderQueryList 
                onClickRecoedItem ={(recordRefundMoney,recordRefundOrderNo)=>this.props.onClickRecoedItem(recordRefundMoney,recordRefundOrderNo)}
                recordDatas = {this.props.recordDatas}
                serviceType = {this.props.serviceType}
            />
        )
    }

    renderBtn() {
        return (
            <LinearGradient style={styles.certainBtnGradientWrapper}
                start={{ x: 0.0, y: 0.5 }} end={{ x: 1, y: 0.5 }}
                colors={this.props.isSelected ? commonColor.createGradient :commonColor.disableGradient } >
                
                <TouchableHighlight style={{flex: 1}}
                    underlayColor={commonColor.btnClickColor}
                    onPress={() => this.props.onPressCertenBtn()} 
                    disabled={!this.props.isSelected}
                    >
                    <View style={{justifyContent: 'center', flex: 1}}>
                        <Text style={styles.certainText}>{'確 定'}</Text>
                    </View>
                </TouchableHighlight>
            </LinearGradient>
        )
    }


}

const btnH = 40* fontSizeScaler
const diaW = windowW * 6/8
const styles = StyleSheet.create({
    certainBtnGradientWrapper: {
        flex:0,
        height: btnH,
        width: diaW,
        borderBottomLeftRadius: dialogBorderRadius,
        borderBottomRightRadius: dialogBorderRadius,
    },
    certainText: {
      color: 'white',
      fontSize: 18* fontSizeScaler,
      textAlign: 'center'
    },
})