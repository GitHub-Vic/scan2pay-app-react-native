import React, {Component} from 'react'
import { StyleSheet, View, Text, Image} from 'react-native'
import { commonColor, commonStyles, windowW, windowH, elementWitdh, elementHeigh, alertBtnTextSize, alertTitleTextSize, borderRadius, dialogBorderRadius, fontSizeScaler } from '../../commonStyles'

import { EASYCARD_KEY_ACTIONTYPE_CANCEL_RESERVE , EASYCARD_KEY_ACTIONTYPE_REFUND_TRADE } from '../../constants/actionTypes'
import PureWhiteAlert from './PureWhiteAlert'
import MainButton from  '../MainButton'

export default class CancelQueryAlert extends Component{

    static defaultProps = {
        onPress: ()=>{},
        recordDatas:{},
        onPressCertenBtn:()=>{}
    }

    render(){
        return(
            <PureWhiteAlert 
                onImgPress = {()=>this.props.onPress()}
                contentView = {this.renderContentView()}
                bottomView = {this.renderBtn()}
                hasKeyborad = {false} />
        )
    }
    
    renderContentView() {
        return (
            <View style={styles.container}>
                 <View style={{width:'100%',alignItems:'center'}}>
                    <Text style={styles.title}>是否取消交易?</Text>
                </View>
                <Text style={styles.infoText}>{`時間：${this.props.recordDatas.createTime}`}</Text>
                <Text style={styles.infoText}>{`卡號：${this.props.recordDatas.ezCardID}`}</Text>
                <Text style={styles.infoText}>{`機台：${this.props.recordDatas.deviceId}`}</Text>
                <View style={styles.tradeBox}>
                    {this.props.recordDatas.actionType === EASYCARD_KEY_ACTIONTYPE_CANCEL_RESERVE ?
                        <Text  style={[styles.tradeInfo, {textAlign:'left'}]}>{`加值`}</Text> :
                        <Text  style={[styles.tradeInfo, {textAlign:'left'}]}>{`購貨`}</Text> }
                    <Text style={[styles.tradeInfo, {textAlign:'right' }]}>{`${this.props.recordDatas.amount}元`}</Text>
                </View>
            </View>
        )
    }

    renderBtn() {
        return (
            <MainButton 
                btnColors={commonColor.createGradient}
                title={'確 定'}
                onPress={()=> this.props.onPressCertenBtn()} />
        )
    }


}
const ItemBorderColor = '#b7db81'
const ItemBorderCheckColor = '#5e9907'
const styles = StyleSheet.create({
    container:{
        flexDirection:'row' ,
        flexWrap:'wrap' , 
        marginTop: -60
    },
    title:{  
        fontSize: 18* fontSizeScaler,
        fontWeight: '800',
        color:'#222',
        marginBottom:30
    },
    infoText: {
        width:'100%',
        fontSize: 14* fontSizeScaler,
        color:'gray',
        marginBottom: 3,
        paddingLeft: 5
    },
    tradeBox:{
        width:'100%',
        flexDirection:'row',
        borderRadius:5,
        borderWidth: 1,
        borderColor:ItemBorderColor,
        marginTop:15,
        marginLeft: -5,
        padding: 10
    },
    tradeInfo:{
        flex: 1,
        fontSize: 16* fontSizeScaler,
        color:ItemBorderCheckColor
    },

})
