import React, {Component} from 'react'
import {StyleSheet, View, Text } from 'react-native'
import {commonColor, windowW, alertTitleTextSize, fontSizeScaler,windowH } from '../../commonStyles'

import PureWhiteAlert from './PureWhiteAlert'
import MainButton from  '../MainButton'

const imgSize = windowW/4
export default class NormalTradeResultAlert extends Component{
    static defaultProps = {
        imgPath: 'icon_receipt_result_alert',
        onPress: ()=>{},
        title: '',
        map: new Map(),
        contentStyle:{},
        containerStyle:{},
        rowStyle:{},
        rowValueStyle:{},
        bottomStyle:{}
    }
    
    render(){
        return(
            <PureWhiteAlert 
                onImgPress = {()=>this.props.onPress()}
                imgPath = {this.props.imgPath}
                imgSize = {imgSize}
                imgStyle = {{left: (windowW - imgSize)/2,top:(windowH *1/3)/3}}
                hasKeyborad = {false}
                topView = {this.renderTitle()}
                topStyle = {{paddingTop: imgSize/2 +10}}
                contentStyle={this.props.contentStyle}
                containerStyle={this.props.containerStyle}
                contentView = {this.renderMap()}
                bottomView = {this.renderBtn()}
                bottomStyle = {this.props.bottomStyle}
            />
        )
    }

    renderTitle() {
        return <Text style={styles.title} numberOfLines={2}>{this.props.title}</Text>
    }

    renderMap() {
        var keys = this.props.map.keys();
        var values = this.props.map.values();
        var array =[]
        for(var i=0;i<this.props.map.size;i++)
            array.push (
                this.renderRow(i, keys.next().value, values.next().value)
            )
        return array.map((a)=> a)
    }

    renderRow(index, key, value) {
        return (
            <View style={styles.row} key={index}>
                <Text style={[styles.key ,this.props.rowStyle ]}>{`・ ${key}`}</Text>
                {index == this.props.map.size-1 ?
                    <Text style={[styles.value,this.props.rowValueStyle, {color:'red'}]}>{value}</Text>
                    :
                    <Text style={[styles.value , this.props.rowValueStyle]} >{value}</Text>
                }
            </View>
        )
    }

    renderBtn() {
        return (
            <MainButton 
                btnColors={commonColor.createGradient}
                title={'確 定'}
                onPress={()=> this.props.onPress()} />
        )
    }
  }
  
  const textSize = 16.5 * fontSizeScaler
  const styles = StyleSheet.create({
    title: {
      flex: 1,
      fontSize: alertTitleTextSize,
      color: commonColor.defaultTextColor,
    },
    row: {
        flexDirection:'row',
    },
    key: {
        fontSize: textSize,
        flex: 3,
        textAlign: 'left'
    },
    value: {
        fontSize: textSize,
        flex: 1,
        textAlign: 'right'
    },
  });
  