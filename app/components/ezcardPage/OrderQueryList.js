import React, {Component} from 'react'
import {StyleSheet, View, FlatList, Text, Image,TouchableHighlight} from 'react-native'
import {_formatDate} from '../../lib/functions'
import {commonColor, fontSizeScaler, dialogBorderRadius, windowW } from '../../commonStyles'
import { SERVICE_TYPE } from '../../constants/actionTypes'
import LinearGradient from 'react-native-linear-gradient'
export default class RecordList extends Component {
  static defaultProps = {
    recordDatas:[],
    serviceType:'',
    onClickRecoedItem:()=>{},
  }
  state = {
    currentId :false
  }
  shouldComponentUpdate(nextProps , nextState) {
    // if(nextProps.data === this.props.data) {
    //   return false
    // }
    return true
  }

  render() {
    // 測試資料 data={testData}
    // data={this.props.data}
    return(
      <FlatList
        data={this.props.recordDatas}
        extraData={this.state}
        keyExtractor={(item, index) => item.OrderId}
        renderItem={({item, index}) => this.renderItem(index, item) }
        // ItemSeparatorComponent={() => this.separator()} 
       />
    );
  }

  renderItem(index, item) {
    switch (this.props.serviceType){
     case SERVICE_TYPE.EASYCARD_ORDERQUERY:
       return this.renderOrderQuery(index,item);
     case SERVICE_TYPE.EASYCARD_RESERVEORDER_QUERY:
       return this.renderReserveOrderQuery(index,item);
     case SERVICE_TYPE.EASYCARD_TRANSACTION_DETAIL_QUERY:
       return this.renderTransactionDetailQuery(index,item);
    }
 }

 renderOrderQuery(index, item) {
      return (
          <TouchableHighlight
            style={this.state.currentId === index ?[styles.item ,{borderWidth:2 , borderColor:ItemBorderCheckColor}] : styles.item}
            underlayColor={ItemBorderColor}
            onPress={() => {
              this.setState({currentId : index})
              this.props.onClickRecoedItem(item.Payment , item.OrderId)
              }} >
            <View style={{flexWrap:'wrap'}}> 
                <View style={styles.row}>
                    <Text style={styles.index}>{index+1}</Text>
                    {this.state.currentId === index ? <Image style={styles.checkIcon} source={{uri: 'icon_easycard_record_select.png'}} />: null}
                </View>
                <View style={styles.row}>
                    <Text style={styles.time}>{_formatDate(item.CreateDate)}</Text>
                    <Text style={styles.money}>{`$ ${item.Payment}`}</Text>
                </View>
            </View>
            </TouchableHighlight>
        
      )
  }
  // 加值查詢
  renderReserveOrderQuery(index,item){
    return (
      <TouchableHighlight>
        <View  style={styles.item} >
          <View style={styles.listContainer}> 
              <View style={styles.row}>
                  <Text style={styles.index}>{index+1}</Text>
                  {this.renderReserveOrderTradeTotal(item)}
              </View>
              <View style={styles.row}>
                  <Text style={styles.time}>{_formatDate(item.CreateTime)}</Text>
              </View>
              <View style={styles.row}>
                  <Image style={styles.checkIcon} source={{uri: 'icon_easycard_reserve_item.png'}}/>
                  <Text style={styles.userID}>{item.UserId}</Text>
              </View>
              {this.renderReserveOrderCancelDate(item)}
          </View>
        </View>
      </TouchableHighlight>
    )
  }
  renderReserveOrderTradeTotal(item){
    const {CancelDate,Amount} = item
    let color = CancelDate? 'red' : commonColor.customerScan
    let displayAmount = CancelDate? `+${Amount} 已取消 ` : `+${Amount}  `
    return <Text style={[styles.money, {color}]}>{displayAmount}</Text>
  
  }
  renderReserveOrderCancelDate(item){
    const {CancelDate} = item
    return (
      CancelDate
      ?
      <View style={styles.row}>
        <Text style={styles.time}>{_formatDate(CancelDate)}</Text>
      </View>
      : null
    )
  }
   // 交易明細查詢
   renderTransactionDetailQuery(index,item){
    return (
      <TouchableHighlight>
        <View  style={styles.item} >
          <View style={styles.listContainer}> 
              <View style={styles.row}>
                  <Text style={styles.index}>{index+1}</Text>
                  <Text style={[styles.money,styles.moneyColor]}>{`${item.TXNAMT}`}</Text>
              </View>
              <View style={styles.row}>
                  <Text style={styles.time}>{_formatDate(item.Date + item.Time)}</Text>
              </View>
              <View style={styles.row}>
                  <Text style={styles.transactionDetailQueryItem}>{`交易序號:${item.TSQN}`}</Text>
                  <Text style={[styles.transactionDetailQueryItem,{textAlign:'right'}]}>{`扣款方式:${item.SubType}`}</Text>
              </View>
              <View style={styles.row}>
                  <Text style={styles.transactionDetailQueryItem}>{`交易後餘額:${item.EV}`}</Text>
                  <Text style={[styles.transactionDetailQueryItem,{textAlign:'right'}]}>{`序號(卡片):${item.Sequence}`}</Text>
              </View>
          </View>
        </View>
      </TouchableHighlight>
    )
  }
  
  separator() {
    return <View style={styles.separator} />
  }
}
const ItemBorderColor = '#b7db81'
const ItemBorderCheckColor = '#5e9907'
const fontSize = 16 * fontSizeScaler
const styles = StyleSheet.create({
  separator: {
    height: 1,
    backgroundColor: commonColor.listViewDividerColor,
  },
  item: {
    borderWidth: 1,
    borderColor: 'gray',
    borderRadius: dialogBorderRadius,
    padding: 10,
    marginTop: 8,
    backgroundColor:"#fff",
    shadowOffset:{height:1},
    shadowColor:'#333',
    shadowOpacity:0.5,
  },
  listContainer:{
    flexWrap:'wrap',
    flex:1,
  },
  row: {
      width: windowW *5/8,
      flexDirection: 'row',
      alignItems: 'center',
      marginBottom: 5
     
  },
  index: {
    flex: 1,
    textAlign: 'left',
    fontSize: fontSize,
    textDecorationLine: 'underline'
  },
  checkIcon: {
    width:20 * fontSizeScaler,
    height:20 * fontSizeScaler
  },
  order: {
    fontSize: fontSize,
    marginTop: 10,
    marginBottom: 5,
  },
  time: {
    flex: 1,
    textAlign: 'left',
    fontSize: fontSize,
    color:'#777'
  },
  userID:{
    flex: 1,
    marginLeft: 5,
    fontSize: fontSize,
  },
  money: {
    flex: 1,
    textAlign: 'right',
    fontSize: fontSize,
  },
  moneyColor:{
    color:commonColor.customerScan
  },
  transactionDetailQueryItem:{
    flex: 1,
    fontSize: 14*fontSizeScaler,
    color:'#777'
  }
});
