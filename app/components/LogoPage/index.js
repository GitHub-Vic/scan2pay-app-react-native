import React, {Component} from 'react'
import {View, Image} from 'react-native'
import {goHome} from '../../lib/functions'

const showSec = 2
export default class LogoPage extends Component {

  constructor() {
    super()
    this.state = {
      timerId: {},
    }
  }

  componentDidMount() {
    var tId = setTimeout(() => {
      goHome(this.props.navigation)
    }, showSec * 1000);

    this.setState({timerId: tId});
  }

  render() {
    return(
      <Image style={{flex:1, resizeMode:'cover'}} 
        source={{uri: 'bg_entrance'}}/>
    );
  }

  componentWillUnmount() {
    this.state.timerId && clearTimeout(this.state.timerId);
  }
}