/**
* @class MainButton
* @desc 主要漸層按鈕
* @prop
    style
    btnColors [string] 顏色
    title string 按鈕Title
    onPress function 
*/
import React,{Component} from 'react'
import {StyleSheet, TouchableHighlight, View, Text} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'
import {commonColor, mainBtnHeigh, mainBtnWidth, borderRadius as radius} from '../../commonStyles'

export default class InsertCancelButton extends Component {
  render() {
    return (
      <LinearGradient style={[styles.certainBtnGradientWrapper, this.props.style]}
        start={{ x: 0.0, y: 0.5 }} end={{ x: 1, y: 0.5 }}
        colors={this.props.btnColors} >
        <TouchableHighlight style={styles.certainBtn}
          underlayColor={commonColor.btnClickColor}
          onPress={() => this.props.onPress()} >
          <View><Text style={styles.certainText}>{this.props.title}</Text></View>
        </TouchableHighlight>
      </LinearGradient>
    )
  }
}


const styles = StyleSheet.create({
  certainBtnGradientWrapper: {
    height: mainBtnHeigh,
    width: 120,
    borderRadius: radius,
  },
  certainText: {
    color: 'white',
    fontSize: 20,
  },
  certainBtn: {
    height: mainBtnHeigh,
    width: 120,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: radius,
  },

  });
  