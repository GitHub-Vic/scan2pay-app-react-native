/**
* @class InputMoney
* @desc 輸數框+左側icon +是否是密碼輸入匡(default:沒有)
* @prop
    textInputRef function ref
    icon string icon圖檔名稱
    password boolean 是否是輸入匡（明碼or暗碼 切換)
    onTextChange function 輸入框內的值變動
    inputValue string 輸入框內要顯示的值
    hintText string 提示字
*/
import React, {Component} from 'react'
import {StyleSheet, View, Text, Image, TextInput, TouchableHighlight} from 'react-native'

import {commonColor, elementWitdh, elementHeigh, windowH, windowW, fontSizeScaler } from '../../commonStyles'
import LinearGradient from 'react-native-linear-gradient'

export default class UserInput extends Component {
  constructor() {
    super();
    this.state = {
      secureTextEntry: false,
    };
  }

  componentWillMount() {
    this.setState({secureTextEntry:this.props.password});
  }

  render() {
    var inputTextPaddingRight = this.props.password ? {paddingRight: imageHW + 5} : null;
    return (
      <View style={[styles.wrapper, this.props.style]}>
        <Text style={styles.textTitle}>{this.props.textTitle}</Text>
        {/* <Image style={styles.icon} source={{uri: this.props.icon}} /> */}
        <TextInput
          ref={this.props.textInputRef}
          style={[styles.inputText,inputTextPaddingRight, this.props.style]}
          multiline={false}
          underlineColorAndroid='transparent'
          autoFocus={this.props.autoFocus ? true : false}
          placeholder={this.props.hintText}
          placeholderTextColor={commonColor.hintTextColor}
          secureTextEntry={this.state.secureTextEntry} 
          onChangeText={(text) => this.props.onTextChange(text)} 
          value={this.props.inputValue} 
          onSubmitEditing={()=> this.props.onSubmitEditing ? this.props.onSubmitEditing() : {}}
          returnKeyType={this.props.returnKeyType ? this.props.returnKeyType : 'default'}/>
        {this.props.password ? this.renderEyeImg() : null}
        <LinearGradient style={[styles.underLine, this.props.style]}
          start={{x: 0.0, y: 0.5}} end={{x: 1, y: 0.5}}
          colors={commonColor.createGradient} />
      </View>
    );
  }

  renderEyeImg() {
    var imgPath = this.state.secureTextEntry ? 'icon_password_isopen' : 'icon_password_isclose';
    return (
      <TouchableHighlight style={styles.eyeView}
        underlayColor={'transparent'}
        onPress={() => this.changeSecureTextEntry()} >
        <View >
          <Image style={styles.eye} source={{uri: imgPath}} />
        </View>
      </TouchableHighlight>
    );
  }

  changeSecureTextEntry() {
    this.setState({secureTextEntry: !this.state.secureTextEntry});
  }

}

const imageHW = windowW * 1/14 
const styles = StyleSheet.create({
  wrapper: {
    height: windowH * 1/11,
    width: windowW * 2.5/3,
  },
  textTitle: {
    position: 'absolute',
    bottom: 0,
    left: 5,
    height: windowH * 1/15,
    width: windowW * 0.9/3,
    fontSize: 20 *fontSizeScaler,
  },
  inputText: {
    position: 'absolute',
    bottom: 0,
    height: windowH * 1/12,
    width: windowW * 2.3/3,
    paddingLeft: imageHW + 90,
    fontSize: 20 *fontSizeScaler,
    color: commonColor.defaultTextColor,
  },
  underLine: {
    height: 2,
    width: windowW * 2.3/3,
    position: 'absolute',
    bottom: 5,
  },
  eyeView: {
    position: 'absolute',
    bottom: 15,
    right: 5,
    height: imageHW,
    width: imageHW,
  },
  eye: {
    height: imageHW,
    width: imageHW,
  },

});