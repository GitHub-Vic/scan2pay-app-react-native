/**
* @class RefundAlert
* @desc 交易紀錄 退款Alert
* @prop
    systemOrderId string 交易單號（系統單號）
    orderId string 訂單編號
    onRefundPress function 按下按鈕後的動作
    onCancelPress function 按下按鈕後的動作
*/
import React, {Component} from 'react' 
import {StyleSheet, View, Text, Image, TouchableHighlight, TouchableOpacity, Keyboard} from 'react-native'
import { commonColor, commonStyles, windowW, windowH, dialogBorderRadius, alertBtnTextSize, borderRadius} from '../../commonStyles'

import InputIconPassword from  '../InputIconPassword'

export default class DeleteAlert extends Component {

  constructor() {
    super();
    this.state={
      refundPassword: '',
      isKeyboardShow: false,
    };
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', (e) => this.keyboardDidShow(e))
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => this.keyboardDidHide())
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  keyboardDidShow(e) {
    this.setState({
      isKeyboardShow: true,
      keyboardHeight: e.endCoordinates.height,
    });
  }

  keyboardDidHide() {
    this.setState({
      isKeyboardShow: false,
      keyboardHeight: 0,
    });
  }

  render() {
    const alertStyle = this.state.isKeyboardShow ? {top: cancelSize/1.5} : null
    const cancelBtnStyle = this.state.isKeyboardShow ? {top: cancelSize/2.7} : null
    return (
      <View style={commonStyles.alertBackgroundView}>
        <Image style={[styles.containerImg, alertStyle]} source={{uri: 'bg_dialog_refund_simple'}}>
          <View style={[styles.containerView]}>
            <Text style={styles.title}>請輸入退款密碼</Text>
            <Text style={styles.hint}>退款後金額自動匯入付款帳戶</Text>
            <View style={styles.space}/>
            <Text style={styles.contentText}>{`交易單號: ${this.props.systemOrderId}`}</Text>
            <Text style={styles.contentText}>{`訂單編號: ${this.props.orderId}`}</Text>
            <InputIconPassword textInputRef={(r)=> this.passwordTextInput = r}
              password={true} 
              icon={'icon_inputtext_password'}
              onTextChange={(text) => this.setRefundPasswrod(text)}
              inputValue={this.state.refundPassword}
              hintText={'退款密碼'} />
          </View>
          <View style={styles.btnView}>
            <TouchableHighlight style={styles.refundBtnWrapper}
              underlayColor={commonColor.btnClickColor}
              onPress={(refundKey) => this.props.onRefundPress(this.state.refundPassword)} >
              <View style={[styles.refundBtnWrapper,{backgroundColor: commonColor.defaultRedColor}]}>
                <Text style={{color: 'white', fontSize: alertBtnTextSize}}>確 定 退 款</Text>
                </View>
            </TouchableHighlight>
          </View>
        </Image>
        {this.renderCancel(cancelBtnStyle)}
      </View>
    );
  }

  renderCancel(cancelBtnStyle) {
    return (
      <TouchableOpacity style={[styles.cancel, cancelBtnStyle]} 
        onPress={() => this.props.onCancelPress()} 
        activeOpacity={0.9} >
        <View >
            <Image style={{height:cancelSize,width:cancelSize}} 
            source={{uri: 'icon_refund_cancel'}}/> 
        </View>
      </TouchableOpacity>
    );
  }

  setRefundPasswrod(password) {
    this.setState({refundPassword: password});
  }
}

const viewH = windowH * 1/2;
const viewW = windowW * 6/8;
const cancelSize = windowW * 1.3/10;

const styles = StyleSheet.create({
  containerImg: {
    height: viewH,
    width: viewW,
    alignItems: 'center',
    position: 'absolute',
    top: (windowH - viewH) / 2 , 
    left: windowW / 8,
    borderRadius: dialogBorderRadius,
  },
  containerView: {
    flex: 5,
    margin: 10,
    alignItems: 'center',
  },
  btnView: {
    flex: 1,
    margin: 10,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  title: {
    fontSize: alertBtnTextSize,
    color: commonColor.defaultTextColor,
  },
  hint: {
    fontSize: 15,
    color: commonColor.hintTextColor,
    marginTop: 10,
    textAlign: 'center'
  },
  space: {
    height: 15,
  },
  contentText: {
    fontSize: 15,
    color: commonColor.defaultRedColor,
    marginTop: 5,
  },
  refundBtnWrapper: {
    width: windowW * 1.5/3,
    height: windowH * 1/13,
    borderRadius: borderRadius,
    alignItems: 'center',
    justifyContent: 'center',
  },
  cancel: {
    position: 'absolute',
    top: (windowH - viewH) / 2 - cancelSize/2.5, 
    left: windowW / 8 - cancelSize/2.5,
    width: cancelSize,
    height: cancelSize,
  },
});
