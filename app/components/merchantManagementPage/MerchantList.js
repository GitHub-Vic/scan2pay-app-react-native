/**
* @class RecordList
* @desc 交易紀錄List
* @prop
    data array 要顯示的資料們
    isEnd boolean 是否沒有更多資料。 true=沒有更多資料了。
    fecthMore function 獲取資料
    onPress function item's onPress
*/
import React, {Component} from 'react'
import {StyleSheet, View, FlatList} from 'react-native'

import {commonColor} from '../../commonStyles'
import MerchantItemView from './MerchantItemView'

export default class MerchantList extends Component {

  shouldComponentUpdate(nextProps) {
    if(nextProps.data === this.props.data) {
      return false;
    }
    return true;
  }

  render() {
    return(
      <FlatList
        data={this.props.data}
        keyExtractor={(item, index) => index}
        renderItem={({item, index}) => <MerchantItemView  item={item} onPress={()=> this.props.onPress(index)} onPressDefault={()=> this.props.onPressDefault(index)}/>}
        ItemSeparatorComponent={() => this.separator()}
        onEndReachedThreshold = {0.5}
       />
    );
  }

  separator() {
    return <View style={styles.separator} />
  }

}

const styles = StyleSheet.create({
  separator: {
    height: 1,
    backgroundColor: commonColor.listViewDividerColor,
  },
  footerView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
});