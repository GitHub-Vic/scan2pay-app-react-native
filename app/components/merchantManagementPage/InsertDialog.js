import React, {Component} from 'react'
import { StyleSheet, View, Text, Keyboard} from 'react-native'
import { commonColor, commonStyles, windowW, windowH, dialogBorderRadius, fontSizeScaler } from '../../commonStyles'
import InsertButton from  './InsertButton'
import UserInput from './UserInput';

export default class InsertDialog extends Component{
  constructor(props) {
    super(props);
    this.state = {
      accountId: '',
      password: '',
      showPass: true,
      press: false,
    };
    this.showPass = this.showPass.bind(this);
  }

  showPass() {
    this.state.press === false
      ? this.setState({showPass: false, press: true})
      : this.setState({showPass: true, press: false});
  }

  render(){
    return(
      <View style={[commonStyles.alertBackgroundView, {zIndex: 2}]}>
        <View style={styles.containerDialog}>
          <Text style={styles.text}>新增特店</Text>
          <View style={styles.contentSpace}>
            <UserInput textInputRef={(r)=> this.accountTextInput = r}
              password={false} 
              textTitle={'特店帳號'}
              onTextChange={(text) => this.setState({accountId: text})}
              inputValue={this.state.accountId}
              hintText={'帳號'}
              returnKeyType={'next'}
              onSubmitEditing={()=> this.passwordTextInput ? this.passwordTextInput.focus() : {}} />

            <View style={{height:5}} />
            <UserInput textInputRef={(r)=> this.passwordTextInput = r}
              password={true} 
              textTitle={'特店密碼'}
              onTextChange={(text) => this.setState({password: text})}
              inputValue={this.state.password}
              hintText={'密碼'}
              returnKeyType={'go'} 
              onSubmitEditing={()=> this.props.insertMerchant({accountId: this.state.accountId, password: this.state.password})}/>
            <View style={{flexDirection: 'row'}}>
              <InsertButton style={styles.insertMerchantButton}
                btnColors={['#d6d6d4', '#c2c2c2', '#bdbdbd']}
                title={'取 消'}
                onPress={()=> this.props.closeInsertDialog()}/>
              <InsertButton style={styles.insertMerchantButton}
                btnColors={commonColor.createGradient}
                title={'新 增'}
                onPress={()=> {
                  Keyboard.dismiss()
                  this.props.insertMerchant({accountId: this.state.accountId, password: this.state.password})
                }}/>
            </View>
          </View>
        </View>
      </View>
    )
  }
}

const viewH = windowH * 1/2;
const viewW = windowW * 7/8;

const styles = StyleSheet.create({
  containerDialog: {
    height: viewH,
    width: viewW,
    alignItems: 'center',
    position: 'absolute',
    top: (windowH - viewH) / 4 , 
    left: windowW / 13,
    borderRadius: dialogBorderRadius,
    backgroundColor: 'white',
  },
  container: {
    flex: 1,
    alignItems: 'center'
  },
  insertMerchantButton: {
    marginLeft: 25,
    marginTop: 30,
  },
  text: {
    fontSize: 25 * fontSizeScaler,
    marginTop: 40,
    marginBottom: 20
  },
});
