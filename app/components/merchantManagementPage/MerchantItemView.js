/**
* @class RecordItemView
* @desc 交易紀錄List's item
* @prop
    item 交易紀錄中的一個JsonObject
    onPress function 是否沒有更多資料。 true=沒有更多資料了。
*/

import React, { PureComponent } from 'react'
import { StyleSheet, View, Text, TouchableHighlight } from 'react-native'
import { commonColor, windowW, borderRadius as radius, windowH, commonStyles } from '../../commonStyles'
import LinearGradient from 'react-native-linear-gradient'
import { Icon } from 'react-native-elements'

export default class MerchantItemView extends PureComponent {
  shouldComponentUpdate(nextProps) {
    if (nextProps.item === this.props.item) {
      return false
    }
    return true
  }

  render() {
    const item = this.props.item
    return (
      <View style={styles.wrapper}>
        <View style={styles.content}>
          <View style={styles.row}>
            <Text style={styles.keyText}>特店帳號: </Text>
            <Text style={styles.valueText}>{item.accountId}</Text>
          </View>
          <View style={styles.row}>
            <Text style={styles.keyText}>特店名稱: </Text>
            <Text style={styles.valueText}>{item.name}</Text>
          </View>
        </View>
        {
          item.isDefault === '1' ?
          <View style={styles.isDefault}>
            <Text style={{fontSize: 17}}>預設特店</Text>
          </View>
          :
          <View style={styles.isDefault}>
            <LinearGradient style={[styles.certainBtnGradientWrapper]}
              start={{ x: 0.0, y: 0.5 }} end={{ x: 1, y: 0.5 }}
              colors={commonColor.createGradient} >
              <TouchableHighlight style={styles.certainBtn}
                underlayColor={commonColor.btnClickColor}
                onPress={() => this.props.onPressDefault()} >
                <View><Text style={styles.certainText}>設為預設</Text></View>
              </TouchableHighlight>
            </LinearGradient>
          </View>
        }
        <View style={styles.more}>
          {/* <Image
            style={styles.timeImg}
            source={{ uri: 'icon_record_go_detail' }}
          /> */}
          <Icon
            name='trash'
            type='font-awesome'
            size={40}
            color='red'
            onPress={() => this.props.onPress()} />
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    flexDirection: 'row',
    padding: 10
  },
  row: {
    flexDirection: 'row'
  },
  timeImg: {
    height: 20,
    width: 20
  },
  isDefault: {
    width: (windowW * 1.9) / 10,
    flexDirection: 'column',
    justifyContent: 'center'
  },
  content: {
    width: (windowW * 5.6) / 10
  },
  money: {
    width: (windowW * 3) / 10,
    alignItems: 'flex-end',
    justifyContent: 'center'
  },
  more: {
    width: (windowW * 2) / 10,
    alignItems: 'center',
    justifyContent: 'center'
  },
  keyText: {
    color: commonColor.defaultTextColor
  },
  valueText: {
    width: (windowW * 5.5) / 10,
    color: commonColor.defaultTextColor
  },
  certainBtnGradientWrapper: {
    height: (windowH * 1) / 17,
    width: (windowW * 1) / 5,
    borderRadius: radius,
  },
  certainText: {
    color: 'white',
    fontSize: 15,
  },
  certainBtn: {
    height: (windowH * 1) / 17,
    width: (windowW * 1) / 5,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: radius,
  },
})
