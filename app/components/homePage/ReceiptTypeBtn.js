/**
* @function ReceiptTypeBtn
* @desc 主頁 收款方式 按鈕
* @prop
    onPress function 按下後的動作
    btnStyle 按鈕style
    imgStyle 圖片style
    imgPath 圖片名稱。記得將圖檔丟入ios/android(drawable) 專案內
    text 內容
*/
import React from 'react'
import{ StyleSheet, TouchableHighlight, View, Image, Text, } from 'react-native'
import { borderRadius as radius } from '../../commonStyles'

export default function ReceiptTypeBtn(props){
  return(
    <TouchableHighlight onPress={() => props.onPress()}>
      <View style={[styles.container, props.btnStyle]}>
        <Image source={{uri: props.imgPath}} style={styles.imgStyle} />
        <Text style={styles.textStyle}>{props.text}</Text>
      </View>
    </TouchableHighlight>
  );
}

const styles = StyleSheet.create({
  container:{
    flexDirection: 'row',
    borderRadius: radius,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: 'white',
    borderWidth: 1.5,
  },
  imgStyle:{
    resizeMode: 'contain',
    height: 50,
    width: 50,
  },
  textStyle:{
    marginLeft: 15,
    fontSize: 30,
    color: 'white',
  },

});
