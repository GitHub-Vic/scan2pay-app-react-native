/**
* @class MenuBtn
* @desc 首頁 主功能按鈕
* @prop
    onPress function 按下後的動作
    imgPath string 圖片名稱。記得將圖檔丟入ios/android(drawable) 專案內
    text string 內容
*/
import React from 'react'
import { StyleSheet, TouchableHighlight, View, Image, Text, } from 'react-native'

import { commonColor, fontSizeScaler, windowW} from '../../commonStyles'

export default function MenuBtn(props){
  return(
    <TouchableHighlight style={styles.wrapper} underlayColor={'transparent'} onPress={() => props.onPress()} >
      <View >
        { createImage(props.imgPath) }
        <Text style={styles.text}> {props.text} </Text>
      </View>
    </TouchableHighlight>
  );
 }

function createImage(imgPath){
  if(imgPath)
    return <Image style={styles.menuItems} source={{uri: imgPath}} />
  else
    return null;
 }

 
const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    justifyContent: 'center',
    flexDirection: 'row',
  },
  menuItems: {
    resizeMode: 'contain',
    height: 50 *fontSizeScaler,
    width: 80 *fontSizeScaler,
  },
  text: {
    fontSize: 16 *fontSizeScaler,
    textAlign: 'center',
    color: commonColor.defaultTextColor,
  },
});
