/**
* @class ActionBtn
* @desc ActionBtn 右下角的按鈕
* @prop
    onPress function 按下後的動作
    imgPath string 圖片名稱。記得將圖檔丟入ios/android(drawable) 專案內
*/
import React from 'react'
import { StyleSheet, TouchableHighlight, Image, View} from 'react-native'

import { commonColor, windowH, } from '../commonStyles'

export default function ActionBtn(props){
  return(
    <TouchableHighlight 
      style={styles.wrapper} 
      underlayColor={'#DCDCDC'} 
      onPress={() => props.onPress()} >
      <View>
        <Image style={styles.image} source={{uri: props.imgPath}} />
      </View>
    </TouchableHighlight>
  );
}

const styles = StyleSheet.create({
  wrapper: {
    alignItems: 'center',
    justifyContent: 'center',
    width: 60,
    height: 60,
    borderRadius: 30,
    backgroundColor: '#E6E6FA',
    position: 'absolute',
    bottom: 30,
    right: 30,
  },
  image: {
    resizeMode: 'contain',
    width: 30,
    height: 30,
  },
});
  