import React, {Component} from 'react';
import { StyleSheet } from 'react-native'
import Toast, {DURATION} from 'react-native-easy-toast'
import {commonColor} from '../commonStyles'

export default class CustomToast extends Component {

    show(text) {
        this.refs.toast.show(text, DURATION.LENGTH_LONG)
    }

    render() {
        return <Toast ref="toast" style={styles.toast} textStyle={styles.text}/>
    }
}

const styles = StyleSheet.create({
    toast: {
        padding: 12,
        borderRadius: 30,
        backgroundColor: 'white',
    },
    text: {
        color: commonColor.customerScan,
        fontSize: 20,
    }
})