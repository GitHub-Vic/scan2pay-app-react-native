export var ID_SERIAL = 'serialID';
export var ID_MERCHANT = 'merchantId';
export var NUMBER_ORDER = 'orderNumber';
export var IS_LOGIN = 'isLogin';
export var IS_STORE_ACCOUNT = 'isStoreAccount';
export var ACCOUNT = 'account';
export var IS_CLEAN_MQTT_SESSION = 'isCleanMqttSession';
