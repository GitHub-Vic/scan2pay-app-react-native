export function parserErrorCode(errorCode){
    if(errorCode == null)
        return "Error Code Is Null";
    if(errorCode.length != 6)
        return "Error Code Length Not Right";
    if(errorCode.startsWith("00")) {
        return parser00(errorCode);
    }else if(errorCode.startsWith("01")){
        return parser01(errorCode);
    }else if(errorCode.startsWith("02")){
        return parser02(errorCode);
    }else if(errorCode.startsWith("03")){
        return parser03(errorCode);
    }else if(errorCode.startsWith("04")){
        return parser04(errorCode);
    }else if(errorCode === "900000"){
        return "系統錯誤，請重新嘗試";
    }else if(errorCode === "900001"){
        return "Reader使用中";
    }else if(errorCode ==="900003"){
        return "Retry超過3次";
    }else{
        return errorCode;
    }
}
function parser00(errorCode){
    let endStr = errorCode.substring(4,6)
    switch (endStr){
        case "04":
            return "卡片沒收，Terminal進行鎖卡";
        case "05":
            return "卡片驗證碼錯誤";
        case "06":
            return "卡片被限制，請持卡人與悠遊卡公司聯絡";
        case "12":
            return "交易不合法(例如:重複開卡)";
        case "13":
            return "金額不合法";
        case "14":
            return "卡號不合法";
        case "15":
            return "無此發卡業者";
        case "19":
            return "交易重複";
        case "41":
            return "遺失卡";
        case "51":
            return "額度不足";
        case "54":
            return "卡片過期";
        case "57":
            return "交易不被允許";
        case "58":
            return "端末設備交易不被允許";
        case "61":
            return "超過金額上限";
        case "76":
            return "無法找到原始交易(例如:銷售退貨交易無法找到原始銷售交易紀錄)";
        case "77":
            return "找到原始交易，但是交易內容比對不一致";
        case "98":
            return "悠遊卡扣款失敗";
        }
        return errorCode;
    }

function parser01(errorCode){
    let endStr = errorCode.substring(3, 6);
    switch (endStr) {
        case "117":
            return "前日交易尚未結帳，須先結帳";
        case "123":
            return "餘額不足";
        case "125":
            return "須執行Retry";
        case "126":
            return "非Retry交易";
        case "128":
            return "Retry交易資料與實際卡片不符";
        case "130":
            return "票卡已退";
        case "131":
            return "卡別錯誤";
        case "132":
            return "黑名單卡";
        case "133":
            return "票卡已鎖";
        case "134":
            return "未開卡之票卡";
    }
    return errorCode;
}

function parser02(errorCode){
    let endStr = errorCode.substring(4, 6);
    switch (endStr) {
        case "12":
            return "不合法之交易";
        case "13":
            return "不合法之交易金額";
        case "19":
            return "交易重複";
        case "51":
            return "額度不足";
        case "54":
            return "卡片過期";
        case "57":
            return "持卡人交易不被允許";
        case "58":
            return "端末機交易不被允許";
        case "59":
            return "設備未啟用";
        case "61":
            return "超過金額上限";
        case "65":
            return "超過次數上限";
        case "76":
            return "無法找到原始交易";
        case "77":
            return "找到原始交易，但交易內容比對不一致";
        case "90":
        case "94":
            return "聯絡發卡銀行";
    }
    return errorCode;
}

function parser03(errorCode){
    let endStr = errorCode.substring(4, 6);
    switch (endStr) {
        case "12":
            return "不合法之交易";
        case "13":
            return "不合法之交易金額";
        case "19":
            return "交易重複";
        case "51":
            return "額度不足";
        case "54":
            return "卡片過期";
        case "57":
            return "持卡人交易不被允許";
        case "58":
            return "端末機交易不被允許";
        case "59":
            return "設備未啟用";
        case "61":
            return "超過金額上限";
        case "65":
            return "超過次數上限";
        case "90":
        case "94":
            return "聯絡發卡銀行";
    }
    return errorCode;
}

function parser04(errorCode){
    let midStr = errorCode.substring(2,4);
    let endStr = errorCode.substring(4, 6);
    switch (midStr) {
        case "60":
            if(endStr.startsWith("A"))
                return "指令呼叫錯誤，請重新操作";
            else
                return "格式錯誤，請重新操作";

        case "61":
            if(endStr === "01") {
                return "票卡不適用";
            }else if(endStr === "03") {
                return "卡片已失效";
            }else if(endStr === "04") {
                return "未開卡之票卡/票卡狀態不符";
            }else if(endStr === "08") {
                return "票卡過期";
            }else if(endStr === "09") {
                return "票卡已鎖卡";
            }else if(endStr === "0F") {
                return "卡片失效，請洽悠遊卡公司";
            }
            return errorCode+"-不正常卡，拒絕交易";

        case "62":
            if(endStr === "01") {
                return "找不到卡片，請重新感應";
            }else if(endStr === "02") {
                return "讀卡失敗，請重新感應";
            }else if(endStr === "03") {
                return "寫卡失敗";
            }else if(endStr === "04") {
                return "多張卡";
            }else if(endStr === "xx") {
                return "查詢卡號失敗";
            }
            return errorCode+"-交易中止，請重新操作";

        case "63":
            if(endStr === "04") {
                return "設備重新Sing On中，請稍等";
            }else if(endStr === "05") {
                return "設備重新Sing On中，請稍等";
            }
            return errorCode+"-交易中止，請重新開機";

        case "64":
            if(endStr === "01") {
                return "取消交易與上一筆交易不符";
            }else if(endStr === "02") {
                return "交易金額超過額度";
            }else if(endStr === "03") {
                return "餘額不足";
            }else if(endStr === "04") {
                return "卡號錯誤";
            }else if(endStr === "09") {
                return "自動加值未啟用";
            }else if(endStr === "0A") {
                return "票卡自動加值金額為零";
            }else if(endStr === "0C") {
                return "累計小額扣款金額超出日限額";
            }else if(endStr === "0D") {
                return "單次小額扣款金額超出次限額";
            }else if(endStr === "0E") {
                return "無法交易，卡片餘額異常";
            }else if(endStr === "0F") {
                return "Reader累計加值金額超出額度管控限制";
            }else if(endStr === "10") {
                return "票卡不適用(非普通卡)";
            }else if(endStr === "11") {
                return "票卡押金不符(押金非100)";
            }else if(endStr === "12") {
                return "超過通路退卡交易額度(餘額>1000)";
            }else if(endStr === "18") {
                return "票卡於此通路限制使用";
            }
            return errorCode+"-拒絕交易";

        case "65":
            return errorCode+"-安全錯誤，請報修";

    }

}
