
#project structure

/app
    /actions    <-- action 放置處
    /components    <-- presentation components 放置處
    /constants    <-- constants 放置處
    /containers    <-- container 放置處
    /images        <-- 放圖片
    /lib        <-- 放一些共用的 function
    /reducers    <-- 放 reducer
    index.js    <-- 整個專案的 entry point
    <!-- /__mocks__    <-- Jest 的 mock 檔 -->
    <!-- /api        <-- api定義處  -->
    <!-- /model        <-- 放 data model -->
    <!-- /styles        <-- 放整個專案的 style 定義 -->
    <!-- config.js    <-- 放設定檔 -->

#yarn add
$ yarn add react-redux
$ yarn add redux
$ yarn add redux-thunk
$ yarn add redux-logger
ps.redux-action ( not use.. )



#project structure
    /app
        index - entry
        store - redux's store
        
        /actions
            receiptActions - 收款部分的actions
            settingsActions - 設定部分的actions

        /components
            actionBtn - 卡在右下角的 回首頁按鈕
            toolbar - tool bar
            /alert 提示框們
                titleWithOneBtn - 只有Title ＆ 可顯示/不顯示的一個Button 
                tradeResult - 交易結果
            /homePage 首頁＆首頁's ui
                index 首頁
            /receiptPage 收款頁面's ui 
                customerBeScanned - 消費者被掃's ui ＆ 相機相關動作
                customerScan - 消費者主掃's ui & 產碼的動作
                toggleModeBtn - 切換模式的按鈕
            /recordPage 交易紀錄‘s ui
                index.js 交易紀錄 選擇日期畫面

        /constants
            actionTypes - redux action's types

        /containers
            receiptMoneyPage - 收款 輸入金額 頁面
            receiptPage - 收款（消費者主掃/被掃） 頁面
            settingsPage - 設定
        
        /lib
            functions - 共用的function們

        /reducers
            index - 將全部的reducers合併
            receiptReducers - 收款部分的reducers
            settingsReducers - 設定部分的reducers

    /



#need modify




#mqtt
/app/index.js use /app/lib/mqtt.js

componentDidMount():
    setUdid()

    connectActiveMQ() => isLoggin ? 
        true => isUdid.length > 0 ?
            true => initMQTT => connect(isCleanSession?) => subscribe topic
            :false => return
    :false => disconnect()

    start ConnectInterval(if interval is null)


componentWillUnmount():
    disconnect()
    stop ConnectInterval


disconnect():
    mqttClinet ? 
        true => isConnect ?
            true => unsubscribe topic => disconnect => null
           :false => null
       :false => return