import React, {Component} from 'react'
import {NativeModules, AsyncStorage, Text, NativeEventEmitter, Platform} from 'react-native'
import {persistStore, autoRehydrate} from 'redux-persist'
import {Provider} from 'react-redux'
import store from './store'
import {StackNavigator} from 'react-navigation'
import Orientation from 'react-native-orientation'
import SplashScreen from 'react-native-splash-screen'

import PresetState from './components/PresetState'
import HomePage from './containers/HomePage'
import ReceiptMoneyPage from './containers/ReceiptMoneyPage'
import ReceiptPage from './containers/ReceiptPage'
import SettingsPage from './containers/SettingsPage'
import RecordPage from './containers/RecordPage'
import MessagePage from './containers/MessagePage'
import MessageDonglePage from './containers/MessageDonglePage'
import LogoPage from './components/LogoPage'
import LoginPage from './containers/LoginPage'
import ForgetPasswordPage from './containers/ForgetPasswordPage'
import EZCardPage from './containers/EZCardPage'
import EZCardSetting from './containers/EZCardSetting'
import EZCardSettingConfig from './containers/EZCardSettingConfig'
import MerchantManagementPage from './containers/MerchantManagementPage'

const {AllPayPassMethod} = NativeModules
import appActions from './actions'
import mqtt from './lib/mqtt'
import apnsToken from './lib/apnsToken'
let opt = {
  key: 'root',
  storage: AsyncStorage,
}

Text.defaultProps.allowFontScaling = false
export default class App extends Component {
  constructor() {
    super()
    this.state = {isPersist: false, closePresetUi: false}
  }

  componentWillMount() {
    persistStore(store, opt, ()=>this.persistStoreSuccess())
    Orientation.lockToPortrait()
    this.countDownPresetUi()
  }

  componentDidMount() {
    SplashScreen.hide()
    Platform.OS === 'ios' ? this.listenAPNsToken() : null
    // AllPayPassMethod.emitterAPNsTokenTest(`RN emitterAPNsTokenTest`)
  }

  componentWillUnmount() {
    mqtt().stopConnectMqtt()
    store.dispatch(appActions.settingsActions.reset())
    store.dispatch(appActions.recordActions.resetRecord())
    this.unRegisterEventEimtter()
    Platform.OS === 'ios' ? apnsToken().stopCheckAPNsToken() : null
  }

  listenAPNsToken() {
    const {APNsTokenEmitter} = NativeModules
    this.eventEmitter = new NativeEventEmitter(NativeModules.APNsTokenEmitter)
    const subscribeAPNsToekn = this.eventEmitter.addListener('APNsToken', this.setAPNsToken)
  }

  persistStoreSuccess() {
    this.setState({isPersist: true})
    mqtt().startConnectMqtt()
    AllPayPassMethod.getPhoneOnlyNumber(this.setUdid)
    Platform.OS === 'ios' ? AllPayPassMethod.getAPNsToken(this.setAPNsToken) : null
    Platform.OS === 'ios' ? apnsToken().startCheckAPNsToken() : null
  }

  setUdid = (udid) => {
    store.dispatch(appActions.settingsActions.setUdid(udid))
  }

  setAPNsToken = (token) => {
    if(token.length > 0) {
      store.dispatch(appActions.settingsActions.setAPNsToken(token))
    }
  }

  render() {
    if(!this.state.closePresetUi || !this.state.isPersist) {
      return <PresetState />
    }else {
      return (
        <Provider store={store} >
          <AppNavigation />
        </Provider>
      )
    }
  }

  unRegisterEventEimtter() {
    try{
      subscribeAPNsToekn.remove()
    }catch(e) {
    }
  }

  countDownPresetUi() {
    setTimeout(() => {
      this.setState({closePresetUi: true})
    }, 2 * 1000);
  }

}

const AppNavigation = StackNavigator({
  LogoPage: { screen: LogoPage,
    navigationOptions: ({navigation}) => ({
      header: null })
  },
  LoginPage: { screen: LoginPage,
    navigationOptions: ({navigation}) => ({
      header: null })
  },
  HomePage: { screen: HomePage ,
    navigationOptions: ({navigation}) => ({
    header: null, // title: 'test',
    }),
  },
  ReceiptMoneyPage:{ screen: ReceiptMoneyPage ,
    navigationOptions: ({navigation}) => ({
    header: null })
  },
  ReceiptPage:{ screen: ReceiptPage ,
    navigationOptions: ({navigation}) => ({
    header: null })
  },
  SettingsPage:{ screen: SettingsPage ,
    navigationOptions: ({navigation}) => ({
    header: null })
  },
  RecordPage:{ screen: RecordPage ,
    navigationOptions: ({navigation}) => ({
    header: null })
  },
  MessagePage:{ screen: MessagePage ,
    navigationOptions: ({navigation}) => ({
    header: null })
  },
  MessageDonglePage:{ screen: MessageDonglePage ,
    navigationOptions: ({navigation}) => ({
    header: null })
  },
  ForgetPasswordPage:{ screen: ForgetPasswordPage ,
    navigationOptions: ({navigation}) => ({
    header: null })
  },
  EZCardPage:{ screen: EZCardPage ,
    navigationOptions: ({navigation}) => ({
    header: null })
  },
  EZCardSetting:{ screen: EZCardSetting ,
    navigationOptions: ({navigation}) => ({
    header: null })
  },
  EZCardSettingConfig:{ screen: EZCardSettingConfig ,
    navigationOptions: ({navigation}) => ({
    header: null })
  },
  MerchantManagementPage:{ screen: MerchantManagementPage ,
    navigationOptions: ({navigation}) => ({
    header: null })
  },
}, {
  initialRouteName: 'HomePage',
});