import { createStore, combineReducers, applyMiddleware} from 'redux'
import { createLogger } from 'redux-logger'
import thunk from 'redux-thunk'
import reducers from './reducers'
import {autoRehydrate} from 'redux-persist'
import {DEBUGGABLE} from './config'

const logger = createLogger()
const createStoreWithMiddleware = applyMiddlewareWithLoggerOrNot()(createStore);
const store = createStoreWithMiddlewareWithLoggerOrNot()

function applyMiddlewareWithLoggerOrNot() {
  if(DEBUGGABLE === 'true')
    return applyMiddleware(logger, thunk)
  else
    return applyMiddleware(thunk)
}

function createStoreWithMiddlewareWithLoggerOrNot() {
  if(DEBUGGABLE === 'true')
    return createStoreWithMiddleware(combineReducers(reducers), 
        window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
        autoRehydrate())
  else
    return createStoreWithMiddleware(combineReducers(reducers),
         autoRehydrate())
}

export default store;
