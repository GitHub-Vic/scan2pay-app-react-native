#### READ ./app/README.md
----
#### Xcode IDE
##### 
#### stageTarget: AllPayPassRN StageTarget (Scheme:AllPayPassRN stageTarget)
Xcode->AllPayPassRN(proj)->TARGETS/AllPayPassRN
->General->Identity

* Display name: Scan²Pay stage
* Bundle Identifier: co.intella.ScanToPayStage

->Info->URL Types

* item[0]
    * identifier: co.intella.ScanToPayStage
    * URL Schemes: scan2pay-app-ios-stage
    * Role: Editor

###### ios/AllPayPassRN/stageTarget Info.plist

#### prodTarget: AllPayPassRN (Scheme: AllPayPassRN)
Xcode->AllPayPassRN(proj)->TARGETS/AllPayPassRN
->General->Identity

* Display name: Scan²Pay
* Bundle Identifier: co.intella.ScanToPay

->Info->URL Types

* item[0]
    * identifier: co.intella.ScanToPay
    * URL Schemes: scan2pay-app-ios
    * Role: Editor

###### ios/AllPayPassRN/Info.plist
----